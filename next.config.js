const webpack = require("webpack");
const path = require("path");
const withOffline = require("next-offline");
const withSourceMaps = require("@zeit/next-source-maps");

const { nextI18NextRewrites } = require("next-i18next/rewrites");

const localeSubpaths = {
  de: "de/de",
  fr: "fr/fr",
  "en-GB": "gb/en",
  "en-AU": "au/en",
  "en-US": "us/en",
  es: "es/es",
  "zh-Hant-TW": "tw/zh",
  ja: "jp/ja",
};

const nextConfig = {
  env: {
    GRAPHQL_URL: process.env.GRAPHQL_URL,
    PUBLIC_URL: process.env.PUBLIC_URL,
    MAPBOX_TOKEN: process.env.MAPBOX_TOKEN,
    CLUBHOUSES_INDEX_ID: process.env.CLUBHOUSES_INDEX_ID,
    RCC_BENEFITS_ID: process.env.RCC_BENEFITS_ID,
    NAVIGATION_ID: process.env.NAVIGATION_ID,
    MOBILE_NAVIGATION_ID: process.env.MOBILE_NAVIGATION_ID,
  },
  publicRuntimeConfig: {
    localeSubpaths,
  },
  rewrites: async () => nextI18NextRewrites(localeSubpaths),
  webpack: (config) => {
    config.resolve.alias = {
      ...config.resolve.alias,
      "@Types": path.resolve(__dirname, "./global-types"),
      "@Components": path.resolve(__dirname, "./components"),
      "@Containers": path.resolve(__dirname, "./containers"),
      "@Hocs": path.resolve(__dirname, "./hocs"),
      "@Utils": path.resolve(__dirname, "./utils"),
      "@Server": path.resolve(__dirname, "./server"),
      "@Config": path.resolve(__dirname, "./config"),
      "@Queries": path.resolve(__dirname, "./queries"),
      "styled-components": path.resolve(
        __dirname,
        "node_modules",
        "styled-components"
      ),
    };

    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: "empty",
    };

    config.output = {
      ...config.output,
      globalObject: "this",
    };

    // // Returns environment variables as an object
    // const env = Object.keys(process.env).reduce((acc, curr) => {
    //   acc[`process.env.${curr}`] = JSON.stringify(process.env[curr]);
    //   return acc;
    // }, {});

    // /** Allows you to create global constants which can be configured
    //  * at compile time, which in our case is our environment variables
    //  */
    // config.plugins.push(new webpack.DefinePlugin(env));

    return config;
  },
};

module.exports = withSourceMaps(
  withOffline({
    generateInDevMode: false,
    ...nextConfig,
  })
);
