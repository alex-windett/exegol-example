export interface IClubhouse {
  name: string;
  slug: string;
  address: string;
  location: ICoordinates;
}

export interface ICityPartner {
  type?: string;
  address?: string;
  name: string;
  url: string;
  location: ICoordinates;
}

export interface IClubhouseCity {
  name: string;
  clubhouse: IClubhouse;
  bikeHire: boolean;
  membersGroup: boolean;
  rideLeaders: boolean;
  partnersCollection: {
    items: ICityPartner[];
  };
}

export interface IClubhouseRegion {
  name: string;
  citiesCollection: {
    items: IClubhouseCity[];
  };
}

export interface IMapLocation {
  coords: [number, number];
  marker: IMarker;
  popup?: {
    coords?: [number, number];
    html: () => JSXElement | ReactElement | Element | undefined;
  };
}

export interface IMapMarker {
  url: string;
  width?: string;
  height?: string;
}

export interface IClubhouseItem {
  slug: string;
  name: string;
}
export interface IClubhouseRegionList {
  clubhouseRegionCollection: {
    items: IClubhouseItem[];
  };
}
