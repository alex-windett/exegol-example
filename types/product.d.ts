import { IContentfulAsset } from "./productCategory";

export interface IProduct {
  sanitizedName: string;
  cmsComponents: any;
  colors: IProductCellColor[];
  name: string;
  code: string;
  reviews: {
    Results: any;
    Rating: number;
    TotalResults: string;
  };
  breadcrumbs: {
    category: Breadcrumb;
    subCategory: Breadcrumb;
  };
}

export interface IProductCMS {
  cms: {
    description: {
      json: object;
    };
    triptic: IProductTriptic;
    detailsAndMaterials: {
      details: string[];
    };
    careInstructions: {
      instructions: string[];
    };
    video: {
      autoPlay: boolean;
      entryTitle: string;
      muted: boolean;
      description: string;
      video: {
        contentType: string;
        title: string;
        url: string;
      };
      posterImage: {
        contentType: string;
        title: string;
        url: string;
      };
    };
    fitGuide: {
      description: string;
      name: string;
      modelFitDescription: string;
      modelFitImagesCollection: IContentfulAsset[];
      modelAttributes: {
        json: object;
      };
      primaryFit: IFitComparison;
      secondaryFit: IFitComparison;
    };
    features: IFeaturesCarousel;
    sizeGuide: {
      sizingTable: object;
      instructions: ISizeGuideInstructions;
      assetsCollection: { items: IContentfulAsset[] };
    };
  };
}

export interface ISizeGuideInstructions {
  json: object;
}

export interface IFeaturesCarousel {
  slidesCollection: {
    items: IFeaturesCarouselSlide[];
  };
}
export interface IFeaturesCarouselSlide {
  slideTitle: string;
  slideSummary: string;
  slideMedia: IContentfulAsset;
}

export interface IFitComparison {
  description: string;
  name: string;
  image: IContentfulAsset;
  sideImage: IContentfulAsset;
  rearImage: IContentfulAsset;
}

export interface IProductTriptic {
  assetsCollection: {
    items: IProductTripticItem[];
  };
}
export interface IProductTripticItem {
  contentType: string;
  url: string;
  title: string;
}
export interface IProductPrice {
  formattedValue: string;
  value: integer;
}

interface IMannequinImages {
  url: string;
  alt?: string;
  title: string;
}

export interface IProductSize {
  code: string;
  stockValue: string;
  name: string;
  sanitizedName: string;
  price: IProductPrice;
  wasPrice?: IProductPrice;
}
export interface IProductCellColor {
  code: string;
  colorName: string;
  price: IProductPrice;
  wasPrice: IProductPrice;
  sanitizedName: string;
  mannequins: IMannequinImages[];
  swatch: string;
  marketingMessage?: string;
  sizes: IProductSize[];
  cmsComponents: {
    items: any;
  };
}

export interface IColorCMS {
  manequinImagesCollection: {
    items: IContentfulAsset[];
  };
}

interface ShowcaseCollectionItem {
  url: string;
  title: string;
}
export interface ShowcaseCollection {
  showcaseCollection: { items: ShowcaseCollectionItem[] };
}

export interface IBreadcrumb {
  code: string;
  name: string;
}
