interface ICategory {
  title: string;
  slug: string;
}

export interface ICategoryCollection {
  items: ICategory[];
}

export interface ISubCategoryCollection {
  items: ICategory[];
}

export interface IStory {
  slug: string;
  title: string;
  subCategoryCollection: ISubCategoryCollection;
  categoryCollection: ICategoryCollection;
}
