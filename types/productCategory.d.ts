import { Document } from "@contentful/rich-text-types";

import { ContentfulEntry } from "./";

import { IProductCellColor, IProductPrice } from "./product";
import { ICategoryCollection, ISubCategoryCollection } from "./story";

export interface ICategoryPage {
  productCategoryCollection: IProductCategoryCMS;
  hrybisProductCategory: {
    items: IProductCell[];
  };
}

export interface IProductCategoryCMS extends ContentfulEntry {
  items: ICategoryCMSEntry[];
}

export interface ICategoryCMSEntry {
  title: string;
  metaTitle: string;
  metaImage: IContentfulAsset;
  hasHero: boolean;
  description: false | { json: any };
  contentSlots: strign;
  heroCarousel: {
    slidesCollection: {
      items: IHeroCarouselItem[];
    };
  };
  contentBlocksCollection: {
    items: IProductCategoryContentSlot[];
  };
}

export interface IHeroCarouselItem {
  __typename: string;
  image?: IContentfulAsset;
  video?: IContentfulAsset;
  posterImage?: IContentfulAsset;
  aspect?: string;
}

export interface IProductCategoryContentSlot {
  __typename: string;
  subHeading?: string;
  heroMedia?: IContentfulAsset;
  heading: string;
  body?: false | { json: any };
  asset?: IContentfulAsset;
  categoryCollection?: ICategoryCollection;
  subCategoryCollection?: ISubCategoryCollection;
  slug?: string;
}

export interface IProductCell {
  code: string;
  name: string;
  price: IProductPrice;
  colors: IProductCellColor[];
}

export interface IContentfulAsset {
  url: string;
  title: string;
  contentType?: string;
}
export interface ISlidesCollection {
  image: IContentfulAsset;
}
