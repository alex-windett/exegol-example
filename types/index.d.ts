import { IBasketWishlist } from "@Types/basketWishlist";

export interface ContentfulEntry {
  sys: { id: string };
}

export interface APIError {
  error?: any;
  success: Boolean;
}

export interface CurrencyInterface {
  active: boolean;
  isocode: String;
  name: String;
  symbol: String;
}

export interface AddressInterface {
  country: [Object];
  firstName: String;
  id: String;
  lastName: String;
  line1: String;
  line2?: String;
  postalCode: String;
  town: String;
}

export interface LanguageInterface {
  active: Boolean;
  isocode: String;
  name: String;
  nativeName: String;
}

export interface IUser {
  sub: string;
  email_verified: boolean;
  given_name?: string;
  family_name: string;
  email: string;
  telephone?: string;
}

export interface IAmplifyToken {
  jwtToken: string;
  payload: {
    sub: string;
    device_key: string;
    event_id: string;
    token_use: string;
    scope: string;
    auth_time: number;
    iss: string;
    exp: number;
    iat: number;
    jti: string;
    client_id: string;
    username: string;
  };
}

export interface AppContext {
  // false or basket object
  miniBasket: IBasketWishlist;
  // Basket object or error
  setMiniBasket: ({
    forceRefresh,
  }: {
    forceRefresh: boolean;
  }) => false | object;
  miniBasketOpen: boolean;
  setMiniBasketOpen: (isOpen: boolean) => void;
  globalErrors: GlobalError[];
  setGlobalErrors: (error: string) => void;
  basketID: string;
  setBasketID: (id: string) => void;
}

export interface GlobalError {
  triggeredAt: number;
  error: string;
}

export interface ICoordinates {
  lat: number;
  lon: number;
}
