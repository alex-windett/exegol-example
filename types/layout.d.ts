export interface INavigationItem {
  title: string;
  url: string;
  items?: INavLink[];
}

export interface INavigationContext {
  navigation: INavigationItem[];
  navOpen: boolean;
  setNavOpen: (open: boolean) => void;
}
