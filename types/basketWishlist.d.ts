import { IProductPrice, IProductCellColor, IProductSize } from "./product";

export interface IBasketWishlist {
  basket?: Basket;
  wishlist?: Wishlist;
  loading?: boolean;
  error?: any;
}

export interface IBasket {
  guid: string;
  count?: number;
  items?: BasketItem[];
  totalPrice: IProductPrice;
  subTotalPrice: IProductPrice;
  deliveryMode: DeliveryMode;
}

export interface IWishlist {
  guid: string;
  count?: number;
  items?: BasketItem[];
  subTotalPrice: IProductPrice;
}

export interface IBasketItem {
  code: string;
  name: string;
  count?: number;
  size: IProductSize;
  color: IProductCellColor;
  price: IProductPrice;
  wasPrice: IProductPrice;
  totalPrice: IProductPrice;
}

interface IDeliveryMode {
  code: string;
  name: string;
  price: IProductPrice;
}
