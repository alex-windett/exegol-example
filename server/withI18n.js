const path = require("path");
const NextI18Next = require("next-i18next").default;
const { localeSubpaths } = require("next/config").default().publicRuntimeConfig;

const defaultLanguage = "en-GB";

export const languages = {
  de: "de/de",
  fr: "fr/fr",
  "en-GB": "gb/en",
  "en-AU": "au/en",
  "en-US": "us/en",
  es: "es/es",
  "zh-Hant-TW": "tw/zh",
  ja: "jp/ja",
};

export const RTL_LANGS = ["ja", "zh-Hant-TW"];

const languagePrefixes = Object.keys(languages).map((l) => l);

const NextI18NextInstance = new NextI18Next({
  defaultLanguage,
  otherLanguages: languagePrefixes.filter(
    (language) => language !== defaultLanguage
  ),
  fallbackLng: defaultLanguage,
  allLanguages: languagePrefixes,
  defaultNS: ["common"],
  localeStructure: "{{lng}}/{{ns}}",
  localePath: path.resolve("./public/static/locales"),
  localeExtension: "json",
  localeSubpaths,
  shallowRender: false,
});

export default NextI18NextInstance;

/* Optionally, export class methods as named exports */
export const {
  appWithTranslation,
  withTranslation,
  Link,
  Router,
  i18n,
} = NextI18NextInstance;
