import express, { Request, Response } from "express";
import next from "next";
import cors from "cors";
import https from "https";
import fs from "fs";
import path from "path";
import cookieParser from "cookie-parser";
import url from "url";

const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

const HTTPS_PORT = 3001;

const printInfo = (url: string) => {
  const info = `
    ╭────────────────────────────────────────────────────╮
    │                                                    │
    │   Listing on : ${url}                              
    │                                                    │
    ╰────────────────────────────────────────────────────╯
  `;

  console.log(info);
};

(async () => {
  await app.prepare();
  const server = express();

  server.use(cors());
  server.use(cookieParser());

  server.get("*", (req: Request, res: Response) => handle(req, res));

  server.get("*", (req: Request, res: Response) => {
    const parsedUrl = url.parse(req.url, true);
    const { pathname } = parsedUrl;

    // handle GET request to /service-worker.js
    if (pathname === "/service-worker.js") {
      const filePath = path.join(__dirname, ".next", pathname);

      app.serveStatic(req, res, filePath);
    } else {
      handle(req, res, parsedUrl);
    }
  });

  const httpsServer = https.createServer(
    {
      key: fs.readFileSync(path.join(__dirname, "../certs/localhost-key.pem")),
      cert: fs.readFileSync(path.join(__dirname, "../certs/localhost.pem")),
    },
    server
  );
  httpsServer.listen(HTTPS_PORT, () => {
    printInfo(`https://localhost:${HTTPS_PORT}`);
  });
})();
