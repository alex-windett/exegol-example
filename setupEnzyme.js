const { configure } = require("enzyme");
const EnzymeAdapter = require("enzyme-adapter-react-16");

jest.setTimeout(30000);

configure({ adapter: new EnzymeAdapter() });
