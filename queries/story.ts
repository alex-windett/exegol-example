import gql from "fake-tag";

import {
  Node,
  imageQuery,
  linkQuery,
  RaphaProduct,
  PullQuote,
  Body,
  ImagePullquote,
  VideoPlayer,
  CrossHeadline,
  ImageSet,
  KeyDetails,
  AnchorItem,
  StoryFeature
} from "@Utils/graphqlSnippets";

const storyImageQuery = gql`
  image {
    ${imageQuery}
  }
  width
  aspect
`;

export const ProductMannequin = gql`
  sku
  description
  name
  hero {
    ${imageQuery}
  }
  link {
    ${linkQuery} 
  }
  product {
    ${RaphaProduct}
  }
`;

const StoryProfile = gql`
  ${Node}

  body {
    json
  }
  story {
  }
  image {
    url
    title
  }
`;

export const Block = gql`
  block(id: $id, locale: $locale) {
    blockItemsCollection {
      items {
        __typename
        
        ... on StoryFeature {
          ${StoryFeature}
        }
        
        ... on StoryProfile {
          ${StoryProfile}
        }
  
        ... on AnchorLink {
          ${AnchorItem}
        }
      }
    }
  }
`;

const StoryRows = gql`
  ... on Body {
    ${Node}
    ${Body}
  }
  
  ... on ImagePullquote {
    ${Node}
    ${ImagePullquote}
  }
  
  ... on Pullquote {
    ${Node}
    ${PullQuote}
  }
  
  ... on Image {
    ${Node}
    ${storyImageQuery}
  }

  ... on CarouselShowcase {
    ${Node}
  }
  
  ... on VideoPlayer {
    ${Node}
    ${VideoPlayer}
  }

  ... on ImageSet {
    ${Node}
    ${ImageSet}
  }

  ... on KeyDetails {
    ${Node}
    ${KeyDetails}
  }
  
  ... on CrossHeadline {
    ${Node}
    ${CrossHeadline}
  }

  ... on ProductMannequin {
    ${Node}
  }

  ... on Block {
    ${Node}
  }

  ... on CarouselFullWidth {
    ${Node}
  }
  
  ... on CarouselFeatures {
    ${Node}
  }

  ... on HybrisProduct {
    ${Node}
  }
`;

export default gql`
  query getStory($slug: String!, $limit: Int, $skip: Int, $preview: Boolean, $locale: String) {
    storyCollection(limit: 1, preview: $preview, locale: $locale, where: { slug: $slug }) {
      items {
        ${Node}
        heroMedia {
          ${imageQuery}
        }
        subCategoryCollection(limit: 1) {
          items {
            name
            slug
            parentCategory {
              name
              slug
            }
          }
        }
        slug
        publsihedDate
        heading
        subHeading
        credit1
        credit2
        credit3
        backgroundColor
        storyModulesCollection(skip: $skip, limit: $limit, preview: $preview) {
          items {
            __typename  
            ${StoryRows}
          }
        }
      }
    }
  }
`;
