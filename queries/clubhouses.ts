import gql from "fake-tag";

export default gql`
  query getClubhouses {
    clubhouses(id: "${process.env.CLUBHOUSES_INDEX_ID}") {
      title
      description
      heroImage {
        title
        description
        url
      }
      blurbTitle
      blurbContent
      carousel {
        slidesCollection {
          items {
            ... on Image {
              aspect
              image {
                title
                url
              }
            }
          }
        }
      }
    }
    
    clubhouseRegionCollection(limit: 5) {
      items {
        name
        clubhousesCollection {
          items {
            name
            slug
          }
        }
      }
    }
  }
`;
