import gql from "fake-tag";

export default gql`
  query getCategory(
    $category: String!
    $preview: Boolean
    $locale: String
    $showOutStock: Boolean
  ) {
    productCategoryCollection(
      where: { category: $category }
      limit: 1
      preview: $preview
      locale: $locale
    ) {
      items {
        title
        category
        hasHero
        contentSlots
        metaTitle
        metaImage {
          url
          title
        }
        description {
          json
        }
        heroCarousel {
          slidesCollection(limit: 3) {
            items {
              __typename
              ... on Image {
                aspect
                image {
                  url
                  title
                }
              }
              ... on VideoPlayer {
                posterImage {
                  url
                  title
                }
                autoPlay
                video {
                  url
                  title
                }
              }
            }
          }
        }

        contentBlocksCollection(limit: 4) {
          items {
            __typename
            ... on ProductCategoryContent {
              heading
              body {
                json
              }
              asset {
                url
                title
                contentType
              }
            }
            ... on Story {
              heading
              subHeading
              heroMedia {
                url
                title
              }
              slug
              categoryCollection {
                items {
                  slug
                }
              }
              subCategoryCollection {
                items {
                  slug
                }
              }
            }
          }
        }
      }
    }
    hrybisProductCategory(
      category: $category
      locale: $locale
      showOutStock: $showOutStock
    ) {
      items {
        code
        name
        sanitizedName
        colors {
          code
          colorName
          price {
            formattedValue
          }
          wasPrice {
            formattedValue
          }
          cmsComponents {
            items {
              swatch
              sku
              marketingMessage
              manequinImagesCollection {
                items {
                  url
                  title
                }
              }
            }
          }
        }
      }
    }
  }
`;
