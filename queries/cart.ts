import gql from "fake-tag";

export default gql`
  {
    basket(id: "asd") {
      count
      guid
      totalPrice {
        value
        formattedValue
      }
      subTotalPrice {
        value
        formattedValue
      }
      deliveryMode {
        price {
          formattedValue
        }
      }
      items {
        code
        name
        count
        size {
          code
          sanitizedName
        }
        color {
          sanitizedName
          code
          cmsComponents {
            items {
              manequinImagesCollection {
                items {
                  url
                  title
                }
              }
            }
          }
        }
        price {
          value
          formattedValue
        }
        wasPrice {
          value
          formattedValue
        }
        totalPrice {
          value
          formattedValue
        }
      }
    }

    wishlist(id: "asdasd") {
      guid
      count
      subTotalPrice {
        value
        formattedValue
      }
      items {
        code
        name
        count
      }
      items {
        code
        name
        count
        size {
          sanitizedName
        }
        color {
          sanitizedName
        }
        price {
          value
          formattedValue
        }
        wasPrice {
          value
          formattedValue
        }
        totalPrice {
          value
          formattedValue
        }
      }
    }
  }
`;
