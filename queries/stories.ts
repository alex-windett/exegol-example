import gql from "fake-tag";

const STORY_LIMIT = 25;
const CATEGORY_LIMIT = 10;

const STORY_COLLECTION = gql`
  storyCollection(limit: ${STORY_LIMIT}) {
    items {
      sys {
        id
      }
      heading
      subHeading
      slug
      subCategoryCollection(limit: 1) {
        items {
          name
          slug
          parentCategory {
            name
            slug
          }
        }
      }
      heroMedia {
        url
      }
    }
  }
`;

export const allStoriesQuery = gql`
  query getStories($locale: String) {
    storyCollection: storyCategoryCollection(limit: ${STORY_LIMIT}, locale: $locale) {
      items {
        name
        slug
        linkedFrom {
          ${STORY_COLLECTION}
        }
      }
    }

    category: storyCategoryCollection(limit: ${CATEGORY_LIMIT}, locale: $locale) {
      items {
        name
        slug
      }
    }
  }
`;

export const categoryQuery: string = gql`
  query getStories($categorySlug: String, $locale: String) {
    storyCollection: storyCategoryCollection(limit: ${STORY_LIMIT}, locale: $locale, where: { slug: $categorySlug }) {
      items {
        name
        slug
        linkedFrom {
          ${STORY_COLLECTION}
        }
      }
    }

    category: storyCategoryCollection(limit: ${CATEGORY_LIMIT}, locale: $locale, where: { slug: $categorySlug	}) {
      items {
        name
        slug
      }
    }
  }
`;

export const subCategoryQuery = gql`
  query getStories($subCategorySlug: String, $categorySlug: String, $locale: String) {
    storyCollection: storySubCategoryCollection(
      limit: ${STORY_LIMIT},
      locale: $locale,
      where: { slug: $subCategorySlug }
    ) {
      items {
        name
        slug
        linkedFrom {
          ${STORY_COLLECTION} 
        }
      }
    }

    category: storyCategoryCollection(limit: ${CATEGORY_LIMIT}, locale: $locale, where: { slug: $categorySlug }) {
      items {
        name
        slug
      }
    }

    subCategory: storySubCategoryCollection(limit: ${CATEGORY_LIMIT}, locale: $locale, where: { slug: $subCategorySlug }) {
      items {
        name
        slug
      }
    }
  }
`;
