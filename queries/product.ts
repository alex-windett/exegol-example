/* A fake template literal tag to trick syntax highlighters, linters and formatters into action. Interpolations and escapes are tested. */
import gql from "fake-tag";
export default gql`
  query getProduct($sku: String!, $preview: Boolean, $locale: String) {
    product(sku: $sku, locale: $locale) {
      code
      baseProduct
      name
      sanitizedName
      price {
        value
        formattedValue
      }
      wasPrice {
        value
        formattedValue
      }
      reviews {
        TotalResults
        Results {
          Rating
        }
      }
      breadcrumbs {
        category {
          name
          code
        }
        subCategory {
          name
          code
        }
      }
      cmsComponents {
        items {
          summary

          video {
            entryTitle
            muted
            autoPlay
            description
            video {
              title
              contentType
              url
            }
            posterImage {
              title
              contentType
              url
            }
          }
          description {
            json
          }
          fitGuide {
            entryTitle
            name
            description
            modelFitDescription
            modelFitImagesCollection {
              items {
                title
                url
              }
            }
            modelAttributes {
              json
            }
            primaryFit {
              entryTitle
              name
              use
              description
              image {
                title
                contentType
                url
              }
              sideImage {
                title
                contentType
                url
              }
              rearImage {
                title
                contentType
                url
              }
            }
            secondaryFit {
              entryTitle
              name
              use
              description
              image {
                title
                contentType
                url
              }
              sideImage {
                title
                contentType
                url
              }
              rearImage {
                title
                contentType
                url
              }
            }
          }
          showcaseCollection {
            items {
              url
              title
            }
          }
          detailsAndMaterials {
            details
          }
          careInstructions {
            instructions
          }
          sizeGuide {
            sizingTable
            instructions {
              json
            }
            assetsCollection(limit: 2) {
              items {
                url
                title
              }
            }
          }
          features {
            slidesCollection(limit: 6) {
              items {
                slideTitle
                slideSummary
                slideMedia {
                  url
                  title
                }
              }
            }
          }
          triptic {
            assetsCollection(limit: 3) {
              items {
                contentType
                url
                title
              }
            }
          }
        }
      }
      colors {
        code
        colorName
        stockValue
        price {
          formattedValue
          value
        }
        wasPrice {
          formattedValue
          value
        }
        cmsComponents {
          items {
            swatch
            sku
            manequinImagesCollection(preview: $preview, locale: $locale) {
              items {
                url
                title
              }
            }
          }
        }
        sizes {
          code
          name
          sanitizedName
          stockValue
        }
      }
    }
  }
`;
