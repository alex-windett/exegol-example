import React from "react";
import styled from "styled-components";
import { dim } from "@rapharacing/token";
import UI from "@rapharacing/rapha-ui";
import { withTranslation } from "@Server/withI18n";

const { Typography, Image } = UI;

const { H3, Body } = Typography;

const Wrapper = styled.section`
  text-align: center;
  margin: ${dim(7)};
`;

const PageError = ({ t, statusCode }: any) => (
  <Wrapper>
    <H3>{t("common_went_wrong")}</H3>

    <Body>
      {statusCode
        ? t("error_with_status", { statusCode })
        : t("error_without_status")}
    </Body>

    <Image src="https://i1.adis.ws/i/rapha/404" alt={t("common_went_wrong")} />
  </Wrapper>
);

export default withTranslation("common")(PageError);
