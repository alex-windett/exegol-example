import styled from "styled-components";
import tokens, { dim } from "@rapharacing/tokens";
import UI from "@rapharacing/rapha-ui";

const { Typography } = UI;

export const StyledLabel = styled(Typography.Label)`
  margin-right: ${dim(2)};
`;

export const StyledBody = styled(Typography.Body)`
  margin-bottom: ${dim(4)};
`;

export const Anchors = styled.div`
  display: flex;
  justify-content: space-evenly;
`;

export const Header = styled.div`
  text-align: center;
  border-bottom: 1px solid ${tokens.color("grey15")};
  margin-bottom: ${dim(4)};
`;

export const Block = styled.div`
  padding: ${dim(2)} 0;
`;
