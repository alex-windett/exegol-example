import styled from "styled-components";
import tokens from "@rapharacing/token";
import UI from "@rapharacing/rapha-ui";

const { Typography } = UI;

export const Title = styled(Typography.H3)`
  text-align: center;
`;

export const SubTitle = styled(Typography.H4)`
  text-align: center;
`;

export const Section = styled.section`
  display: grid;
  grid-template-columns: repeat(12, 1fr);
`;

export const LoginGrid = styled.div`
  grid-column: 2 / span 4;
`;
export const RegisterGrid = styled.div`
  grid-column: 8 / span 4;
  display: flex;
  flex-direction: column;
`;

export const Body = styled(Typography.Body)`
  text-align: center;
`;

export const Border = styled.div`
  grid-column: 7 / span 1;
  width: 1px;
  background: ${tokens.color("grey15")};
  height: 100%;
`;

export const Submit = styled.div`
  margin-top: 32px;
  width: 100%;

  button {
    width: 100%;
  }
`;

export const Confirm = styled.div`
  grid-column: 2 / span 4;
`;

export const Resend = styled.div`
  grid-column: 8 / span 4;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
