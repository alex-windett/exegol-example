import UI from "@rapharacing/rapha-ui";
import styled from "styled-components";
import tokens, { dim } from "@rapharacing/tokens";
import { RAPHA_DEFAULT } from "@Utils/constants";
import { Link } from "@WithTranslations";

import { IBasketItem } from "@Types/basketWishlist";

const { Typography, Button } = UI;

const { Label, Body, Anchor } = Typography;

const Wrapper = styled.ul`
  overflow-y: scroll;
  overflow-x: hidden;
  list-style-type: none;
  padding: 0;
  margin: 0;
  height: calc(100% - 335px);
`;

const Item = styled.li`
  display: grid;
  grid-template-columns: 35% 40% 25%;
  align-items: center;
  padding: ${dim(4)} ${dim(2)};

  & + & {
    border-top: 1px solid ${tokens.color("grey15")};
  }
`;

const Details = styled.div`
  margin-left: ${dim(4)};
`;

const TotalPrice = styled.div`
  align-self: baseline;
  text-align: right;
`;

const WasPrice = styled.span`
  text-decoration: line-through;
  color: ${tokens.color("grey15")};
`;

const Actions = styled.section`
  display: flex;
  justify-content: space-between;

  button {
    padding: 0;
    text-align: left;
  }
`;

interface IProps {
  items: IBasketItem[];
  allowQuantityChange?: boolean;
  allowRremoveFromBasket?: boolean;
  allowRremoveFromWishlist?: boolean;
  allowMoveToWishlist?: boolean;
  allowMoveToBasket?: boolean;
}

const MiniBasketProducts = ({
  items,

  // @ts-ignore
  allowQuantityChange = false,
  allowRremoveFromBasket = false,
  allowRremoveFromWishlist = false,
  allowMoveToWishlist = false,
  allowMoveToBasket = false,
}: IProps) => {
  return (
    <Wrapper>
      {items &&
        items.map((item: IBasketItem, ind: number) => {
          const Route: React.FC<{}> = ({ children }) => (
            <Link
              href={{
                pathname: "/product/[sku]/[slug]",
                query: {
                  sku: item.size.code,
                  slug: item.size.sanitizedName,
                },
              }}
              as={`/product/${item.size.code}/${item.size.sanitizedName}/`}
              passHref
            >
              <Anchor>{children}</Anchor>
            </Link>
          );

          return (
            <Item key={ind}>
              <div>
                <Route>
                  <img
                    src={
                      item.color.cmsComponents
                        ? item.color.cmsComponents.items[0]
                            .manequinImagesCollection.items[0].url
                        : RAPHA_DEFAULT
                    }
                    alt={item.color.sanitizedName}
                    title={item.color.sanitizedName}
                  />
                </Route>
              </div>

              <Details>
                <Route>
                  <Label varian="sml">{item.name}</Label>
                </Route>

                <Body variant="xSml">
                  {item.size && item.size.sanitizedName},{" "}
                  {item.color && item.color.sanitizedName}
                </Body>

                <Body variant="xSml">
                  {item.count} x {item.price.formattedValue}{" "}
                  <WasPrice>{item.wasPrice.formattedValue}</WasPrice>
                </Body>

                <Actions>
                  {allowRremoveFromBasket && (
                    <Button
                      variant="link"
                      onClick={() => alert(`Remove ${item.code} from basket`)}
                    >
                      Remove
                    </Button>
                  )}

                  {allowMoveToWishlist && (
                    <Button
                      variant="link"
                      onClick={() => alert(`Move ${item.code} to wishlist`)}
                    >
                      Move to wish list
                    </Button>
                  )}

                  {allowRremoveFromWishlist && (
                    <Button
                      variant="link"
                      onClick={() =>
                        alert(`Remove ${item.code} from wishlsits`)
                      }
                    >
                      Remove
                    </Button>
                  )}

                  {allowMoveToBasket && (
                    <Button
                      variant="link"
                      onClick={() => alert(`Move ${item.code} to basket`)}
                    >
                      Move to basket
                    </Button>
                  )}
                </Actions>
              </Details>

              <TotalPrice>
                <Label>{item.totalPrice.formattedValue}</Label>
              </TotalPrice>
            </Item>
          );
        })}
    </Wrapper>
  );
};

export default MiniBasketProducts;
