import React from "react";
import UI from "@rapharacing/rapha-ui";
import tokens, { dim } from "@rapharacing/tokens";
import styled from "styled-components";

const { Typography, Button } = UI;

const { Body, Label, H4 } = Typography;

const BasketSummary = styled.ul`
  list-style-type: none;
  padding: 0;
  margin: 0;
`;

const BasketSummaryItem = styled.li`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: ${dim(3)} 0;

  p,
  h4,
  label {
    margin: 0;
  }

  & + & {
    border-top: 1px solid ${tokens.color("grey15")};
  }
`;

const CheckoutButton = styled(Button as any)`
  width: 100%;
`;

const CartBasketSummary = ({ wishlistActive }: any) => {
  if (wishlistActive) {
    return <p>wishlist summary</p>;
  }
  return (
    <BasketSummary>
      <BasketSummaryItem>
        <Body variant="xSml">Gift Wrap (£5.00)</Body>
        <Label>£5.00</Label>
      </BasketSummaryItem>
      <BasketSummaryItem>
        <Body variant="xSml">Promo code</Body>
        <Button variant="secondary">Add</Button>
      </BasketSummaryItem>
      <BasketSummaryItem>
        <Body variant="xSml">Subtotal</Body>
        <Label>£1234.56</Label>
      </BasketSummaryItem>
      <BasketSummaryItem>
        <Body variant="xSml">Order Discounts</Body>
        <Label>£1234.56</Label>
      </BasketSummaryItem>
      <BasketSummaryItem>
        <Body variant="xSml">Delivery</Body>
        <Label>£4.56</Label>
      </BasketSummaryItem>
      <BasketSummaryItem>
        <Label>Total</Label>
        <H4>£1234.56</H4>
      </BasketSummaryItem>
      <CheckoutButton>Checkout</CheckoutButton>
    </BasketSummary>
  );
};

export default CartBasketSummary;
