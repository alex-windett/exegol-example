import styled, { css } from "styled-components";
import UI from "@rapharacing/rapha-ui";

import tokens, { dim } from "@rapharacing/tokens";
import Products from "./Items";

import { IBasketItem } from "@Types/basketWishlist";

const { Loader, Typography } = UI;

const { Body } = Typography;

const Content = styled.div`
  position: relative;
  height: 100%;
`;

const Tabs = styled.nav`
  display: grid;
  grid-template-columns: 50% 50%;
`;

const Tab = styled.button`
  appearance: none;
  border: none;
  background: none;
  font-family: ${tokens.get("type.fontFamily.heading")};
  font-weight: bold;
  font-size: 14px;
  text-transform: uppercase;
  padding: ${dim(2)} 0;
  border-top: 1px solid ${tokens.color("grey15")};

  & + & {
    border-left: 1px solid ${tokens.color("grey15")};
  }

  &:hover {
    cursor: pointer;
    color: ${tokens.color("brand.pink")};
  }

  ${({ active }: { active: boolean }) =>
    active &&
    css`
      color: ${tokens.color("grey15")};
      background-color: ${tokens.color("grey1")};
      border-bottom: 1px solid ${tokens.color("grey15")};
    `}
`;

const Count = styled.span`
  color: ${tokens.color("grey15")};
  padding-left: ${dim(1)};
`;

const Center = styled.div`
  text-align: center;
  display: block;
  text-align: center;
  margin: 0 auto;
`;

interface IProps {
  products: IBasketItem[] | undefined;
  loading?: boolean;
  wishlistActive: boolean;
  basketCount?: number;
  wishlistCount?: number;
  hanldeWishlistActive: () => void;
  handleBasketActive: () => void;
  allowQuantityChange?: boolean;
  allowRremoveFromBasket?: boolean;
  allowRremoveFromWishlist?: boolean;
  allowMoveToWishlist?: boolean;
  allowMoveToBasket?: boolean;
}

const BasketContents = ({
  products,
  loading,
  hanldeWishlistActive,
  wishlistActive,
  handleBasketActive,
  basketCount,
  wishlistCount,
  ...props
}: IProps) => {
  return (
    <Content>
      <Tabs>
        <Tab
          title="Basket"
          active={wishlistActive}
          onClick={handleBasketActive}
        >
          Basket <Count>{basketCount}</Count>
        </Tab>

        <Tab
          title="Wish List"
          active={!wishlistActive}
          onClick={hanldeWishlistActive}
        >
          Wish List <Count>{wishlistCount}</Count>
        </Tab>
      </Tabs>

      {loading ? (
        <Center>
          <Loader dark />
        </Center>
      ) : products ? (
        <Products items={products} {...props} />
      ) : (
        <Center>
          <Body>Your {wishlistActive ? "Wishlist" : "Basket"} is empty.</Body>
        </Center>
      )}
    </Content>
  );
};

export default BasketContents;
