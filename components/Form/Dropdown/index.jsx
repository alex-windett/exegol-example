import React, { Fragment } from "react";

import Select from "react-select";
import { useFormikContext, useField } from "formik";

import Label from "../Label";

const Dropdown = ({ options, label, ...props }) => {
  const { setFieldValue, setFieldTouched } = useFormikContext();
  const [field, { error, touched }] = useField(props);

  function handleOptionChange(selection) {
    setFieldValue(props.name, selection);
  }

  function updateBlur() {
    setFieldTouched(props.name, true);
  }

  return (
    <Fragment>
      <Label for={props.id}>
        {label} {props.required ? <span>required</span> : null}
        <Select
          {...field}
          {...props}
          options={options}
          onBlur={updateBlur}
          onChange={handleOptionChange}
        />
      </Label>
      {touched && error ? <span id={props.id}>{error}</span> : null}
    </Fragment>
  );
};

export default Dropdown;
