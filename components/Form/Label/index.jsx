import React from "react";
import styled from "styled-components";

import tokens from "@rapharacing/tokens";

const StyledLabel = styled.label`
  font-family: ${tokens.get("type.fontFamily.heading")};
  font-weight: ${tokens.get("type.fontWeights.semiBold")};
  font-size: 14px;
  line-height: 18px;
  letter-spacing: 0.4px;
  text-transform: uppercase;
`;

const Label = (props) => {
  return <StyledLabel {...props}>{props.children}</StyledLabel>;
};

export default Label;
