import React, { Fragment, FunctionComponent } from "react";
import { useField } from "formik";

import Label from "../Label";

interface Props {
  id: string;
  label: string;
  name: string;
  placeholder: string;
  type: string;
  required?: boolean;
}

const Input: FunctionComponent<Props> = (props) => {
  const [field, { error, touched }] = useField(props);

  return (
    <Fragment>
      <Label for={props.id}>
        {props.label} {props.required ? <span>required</span> : null}
        <input
          {...field}
          {...props}
          aria-required={props.required}
          aria-invalid={!!error}
          aria-describedby={props.id}
        />
      </Label>
      {touched && error ? <span id={props.id}>{error}</span> : null}
    </Fragment>
  );
};

export default Input;
