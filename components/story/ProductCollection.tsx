import UI from "@rapharacing/rapha-ui";

const { ProductCollection: StoryProductCollection } = UI;

const Collection = ({ productsCollection, ...props }: any) => {
  return <p>Product Collection module waiting on Product API</p>;

  return (
    <StoryProductCollection products={productsCollection.items} {...props} />
  );
};

export default Collection;
