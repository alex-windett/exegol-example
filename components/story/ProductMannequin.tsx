import UI from "@rapharacing/rapha-ui";
import React from "react";
import { compose } from "recompose";
import withGraphql from "@Hocs/withApollo";
import { ProductMannequin as ProductMannequinQuery } from "@Utils/graphqlSnippets";
import * as Types from "StoryTypes";

const { StoryProductMannequin, Typography } = UI;

interface EnhancedProps {
  data: { entry: Types.Product };
  rest: any;
  sys: { id: string };
}

const ProductMannequin: React.FC<EnhancedProps> = ({ data, sys, ...rest }) => {
  const { product, name, description, hero, sku } = data.entry;

  if (!product) {
    return (
      <Typography.H1>
        Unable to find product {sku} on ProductMannequin with{" "}
        {sys ? `ID ${sys.id}` : `Name ${name}`}
      </Typography.H1>
    );
  }

  const props = {
    name: !name ? product.name : name,
    description: !description ? product.summary : description
  };

  return (
    <StoryProductMannequin
      {...rest}
      hero={hero}
      image={product.imageURI}
      {...props}
    />
  );
};

const enhance = compose(
  // @ts-ignore
  withGraphql(({ sys }: Props) => {
    return {
      query: `
        query getPrievew($id: String!) {
          entry: productMannequin(id: $id) {
            ${ProductMannequinQuery}
          }
        }
      `,
      variables: {
        // id: sys.id
        id: "1bJtdUAkL3xcgP9eINKqHC"
      }
    };
  })
);

// @ts-ignore
export default enhance(ProductMannequin);
