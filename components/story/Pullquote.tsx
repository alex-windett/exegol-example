import UI from "@rapharacing/rapha-ui";

const { PullQuote } = UI;

const Pullquote = <P extends {}>(props: P) => <PullQuote {...props} />;

export default Pullquote;
