import UI from "@rapharacing/rapha-ui";

const { StoryImagePullQuote } = UI;

const Pullquote: React.FC = <P extends {}>(props: P) => (
  <StoryImagePullQuote {...props} />
);

export default Pullquote;
