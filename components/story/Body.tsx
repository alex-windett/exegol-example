import UI from "@rapharacing/rapha-ui";
import { ReactNode } from "react";
import { formatRichText } from "@Utils/index";

const { StoryBodyText } = UI;

interface Text {
  text: {
    json: any;
  };
}

export default (text: Text) => {
  const formatted: ReactNode = formatRichText(text.text);
  return <StoryBodyText>{formatted}</StoryBodyText>;
};
