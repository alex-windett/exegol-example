import UI from "@rapharacing/rapha-ui";
import { compose } from "recompose";
import withGraphQL from "@Hocs/withApollo";
import { withRouter } from "next/router";

import { CarouselFeatures as query } from "@Utils/graphqlSnippets";
import { ContentfulEntry } from "@Types/index";

import * as Types from "StoryTypes";

const { Carousel } = UI;

interface Props extends ContentfulEntry {}

interface EnhancedProps {
  data?: any;
}

const CarouselFeatures: React.FC<EnhancedProps> = ({ data }) => {
  const { carouselFeatures } = data;

  if (!carouselFeatures) return null;

  const { slidesCollection: slides } = carouselFeatures;

  return slides ? (
    <Carousel.FeatureCarousel>
      {slides.items.map(
        (
          { slideTitle, slideSummary, slideMedia }: Types.CarouselSlide,
          i: number
        ) => (
          <Carousel.FeaturesSlide
            key={i}
            {...slideMedia}
            feature={{ title: slideTitle, summary: slideSummary }}
          />
        )
      )}
    </Carousel.FeatureCarousel>
  ) : null;
};

const enhanced = compose(
  withRouter,
  withGraphQL(({ sys }: Props) => {
    return {
      query: `query entry($id: String!, $locale: String) {
        carouselFeatures(id: $id, locale: $locale) {
          ${query}
        }
      }`,
      variables: {
        id: sys.id
      }
    };
  })
);

// @ts-ignore
export default enhanced(CarouselFeatures);
