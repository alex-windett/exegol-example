import * as Types from "StoryTypes";

import UI from "@rapharacing/rapha-ui";

const { Image } = UI;

const image: React.FC<Types.Image> = ({ image, ...rest }) => {
  // Filter out any Null values
  const props = Object.entries(rest).filter(prop => !!prop[1]);

  return <Image {...image} {...props} />;
};

export default image;
