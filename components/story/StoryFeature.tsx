import UI from "@rapharacing/rapha-ui";
import { Link } from "@Server/withI18n";
import { formatRichText } from "@Utils/index";

const { Card } = UI;

const type = {
  StoryFeature: "feature",
  StoryProfile: "profile"
};

const storyFeature: React.FC<any> = props => {
  // Overrides for story
  const { image, body, title } = props;

  // Linked story
  const {
    story: linkedStory = {
      heading: "",
      subHeading: "",
      slug: "",
      heroMedia: { url: "", title: "" }
    }
  } = props;

  const [subCategory] = props.story.subCategoryCollection.items;
  const category = subCategory.parentCategory;

  const returnBody = () => {
    debugger;
    if (!body) return linkedStory.subHeading;

    if (body.json) {
      // @ts-ignore
      return formatRichText(body);
    }

    return body;
  };

  const Router: React.FC<{}> = ({ children }) => (
    <Link
      href={{
        pathname: "/stories/[category]/[subCategory]",
        query: {
          category: category.slug,
          subCategory: subCategory.slug,
          slug: linkedStory.slug
        }
      }}
      as={`/stories/${category.slug}/${subCategory.slug}/${linkedStory.slug}`}
      passHref
    >
      <a>{children}</a>
    </Link>
  );
  // Create Props based off which fields are filled
  const cardProps = {
    body: returnBody(),
    title: title || linkedStory.heading,
    image: {
      src: !!image ? image.url : linkedStory.heroMedia.url,
      alt: !!image ? image.title : linkedStory.heroMedia.title
    }
  };

  return (
    <Card
      // @ts-ignore
      type={type[props.__typename]}
      {...props}
      {...cardProps}
      Router={Router}
    />
  );
};

export default storyFeature;
