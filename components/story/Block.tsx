import { ReactNode } from "react";
import { compose } from "recompose";
import { Block as query } from "@Queries/story";
import withGraphQL from "@Hocs/withApollo";
import UI from "@rapharacing/rapha-ui";
import { SingletonRouter } from "next/router";
import { ContentfulEntry } from "@Types/index";
import mapModelToComponent from "@Utils/mapModelToComponent";

const { Block } = UI;

interface Props extends ContentfulEntry {}

interface EnhacedProps {
  router?: SingletonRouter;
  data?: any;
  children?: ReactNode;
}

const block: React.FC<EnhacedProps> = ({ router, data }) => {
  const { blockItemsCollection } = data.block;

  if (!blockItemsCollection.items) return null;

  return (
    <Block>
      {blockItemsCollection.items.map((module: any, i: number) => {
        if (!module) return null;

        let Comp = mapModelToComponent({ moduleName: module.__typename });
        Comp = require("./" + Comp).default;

        if (!Comp) {
          console.log(
            `Cannot find component for ${module.__typename} on Story Block Module`
          );
          return null;
        }

        return <Comp key={i} {...module} router={router} />;
      })}
    </Block>
  );
};

const enhanced = compose(
  withGraphQL(({ sys }: Props) => {
    return {
      query: `query getEntry($id: String!, $locale: String) {
        ${query}
      }`,
      variables: {
        id: sys.id
      }
    };
  })
);

export default enhanced(block);
