declare module "StoryTypes" {
  export interface AnchorProps {
    title: string;
    linkUrl: string;
    linkText: string;
  }

  export interface CarouselSlide {
    slideTitle: string;
    slideSummary: string;
    slideMedia: { url: string; title: string };
  }

  export interface CarouselTypes<T> {
    [key: string]: T;
  }

  export interface Image {
    image: { url: string; title: string };
    aspect: string;
    rest: any;
  }

  export interface ImageSet {
    imagesCollection: { items: Image[] };
  }

  export interface KeyDetails {
    hero: {
      image: string;
      title: string;
    };
    itemsCollection: {
      items: [KeyDetailsItem];
    };
  }

  export interface KeyDetailsItem {
    title: string;
    body: any;
  }

  export interface Product {
    product: any;
    name: string;
    description: string;
    sku: string;
    hero: { url: string };
  }

  export interface ContentfulEntry {
    sys: { id: string };
  }

  export interface VideoPlayer {
    image: { url: string; title: string };
    aspect: string;
    poster: { url: string };
    video: { url: string };
  }
}
