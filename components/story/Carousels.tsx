import React, { ReactNode } from "react";
import UI from "@rapharacing/rapha-ui";
import { compose } from "recompose";
import { withRouter, SingletonRouter } from "next/router";

import withGraphQL from "@Hocs/withApollo";
import {
  CarouselShowcase as CarouselShowcaseQuery,
  CarouselFullWidth as CarouselFullWidthQuery,
  CarouselFeatures as CarouselFeaturesQuery
} from "@Utils/graphqlSnippets";
import mapModelToComponent from "@Utils/mapModelToComponent";
import { ContentfulEntry } from "@Types/index";

import * as Types from "StoryTypes";

const { Carousel: RaphaCarousel } = UI;

interface Props extends ContentfulEntry {
  __typename: string;
}

interface EnhancedProps {
  data?: { entry: any };
  router?: SingletonRouter;
  __typename?: String;
  children?: ReactNode;
}

const query = (name: string): string => {
  switch (name) {
    case "CarouselShowcase":
      return `
        entry: carouselShowcase(id: $id, locale: $locale) {
          ${CarouselShowcaseQuery}
        }
      `;

    case "CarouselFullWidth":
      return `
        entry: carouselFullWidth(id: $id, locale: $locale) {
          ${CarouselFullWidthQuery}
        }
      `;

    case "CarouselFeatures":
      return `
        entry: carouselFeature(id: $id, locale: $locale) {
          ${CarouselFeaturesQuery}
        }
      `;
    default:
      return ``;
  }
};

const PreviewCarousels = ({ data, router, __typename }: EnhancedProps) => {
  if (!data) return <p>No Data returned from query</p>;

  const Carousels: Types.CarouselTypes<string> = {
    // "typename": "storybook-name"
    CarouselFullWidth: "FullWidthCarousel",
    CarouselShowcase: "ShowcaseCarousel"
  };

  const CarouselType: string = Carousels[`${__typename}`];

  if (!CarouselType) {
    return <h1>Cannot find Carousel - "{CarouselType}"</h1>;
  }

  // @ts-ignore
  const CarouselComponent: React.FC<any> = RaphaCarousel[CarouselType];

  const { entry } = data;

  return (
    <CarouselComponent itemsPerSlide={entry.visibleSlides}>
      {entry.slidesCollection.items
        .filter((i: any) => i)
        .map((module: any, i: number) => {
          if (!module.__typename) {
            console.log("Please return `__typename` in GraphQL query");

            return;
          }

          let Comp = mapModelToComponent({ moduleName: module.__typename });
          Comp = require("./" + Comp).default;

          if (!Comp) {
            return (
              <h1>
                Cannot find component for {module.__typename} on {CarouselType}.
              </h1>
            );
          }

          return <Comp key={i} {...module} router={router} />;
        })}
    </CarouselComponent>
  );
};

const enhanced = compose(
  withRouter,
  withGraphQL(({ __typename, sys }: Props) => {
    return {
      query: `query getEntry($id: String!, $locale: String) {
        ${query(__typename)}
      }`,
      variables: {
        id: sys.id
      }
    };
  })
);

export default enhanced(PreviewCarousels);
