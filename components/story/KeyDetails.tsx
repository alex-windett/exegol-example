import UI from "@rapharacing/rapha-ui";

import * as Types from "StoryTypes";
import { formatRichText } from "@Utils/index";

const { StoryKeyDetails } = UI;

const KeyDetails: React.FC<Types.KeyDetails> = ({ hero, itemsCollection }) => {
  if (!itemsCollection.items) return null;

  const data = itemsCollection.items.map((item: Types.KeyDetailsItem) => ({
    ...item,
    body: formatRichText(item.body)
  }));
  return <StoryKeyDetails hero={hero} items={data} />;
};

export default KeyDetails;
