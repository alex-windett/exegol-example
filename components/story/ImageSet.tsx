import * as Types from "StoryTypes";

import UI from "@rapharacing/rapha-ui";

const { ImageSet, Image } = UI;

const StoryImageSet = <P extends Types.ImageSet>({
  imagesCollection,
  ...props
}: P) =>
  imagesCollection.items.length > 0 && (
    <ImageSet {...props}>
      {imagesCollection.items.map((item: any, i: number) => (
        <Image key={i} {...item} {...item.image} />
      ))}
    </ImageSet>
  );

export default StoryImageSet;
