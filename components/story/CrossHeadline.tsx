import UI from "@rapharacing/rapha-ui";

const { StoryCrossHeadline } = UI;

const CrossHeadline = (props: {}) => {
  return <StoryCrossHeadline {...props} />;
};

export default CrossHeadline;
