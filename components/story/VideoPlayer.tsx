import UI from "@rapharacing/rapha-ui";
import * as Types from "StoryTypes";

const { VideoPlayer, Typography } = UI;

const Image = <P extends Types.VideoPlayer>({ video, poster, ...rest }: P) => {
  if (!video.url) return <Typography.H3>No Video URL provided</Typography.H3>;

  // Filter out any Null values
  const props = Object.entries(rest).filter(prop => !!prop[1]);

  return (
    <VideoPlayer
      mediaSource={video.url}
      poster={poster && poster.url}
      {...props}
    />
  );
};

export default Image;
