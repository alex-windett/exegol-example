import React from "react";
import UI from "@rapharacing/rapha-ui";

import * as Types from "StoryTypes";

const { StoryAnchorLink } = UI;

const Item: React.FC<Types.AnchorProps> = ({ title, linkUrl, linkText }) => (
  <StoryAnchorLink
    title={title}
    link={{
      text: linkText,
      url: linkUrl
    }}
  />
);

export default Item;
