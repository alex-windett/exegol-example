import React from "react";
import UI from "@rapharacing/rapha-ui";
import styled from "styled-components";

const { Image } = UI;

const Article = styled.article`
  max-width: 800px;
  margin: 0 auto;
`;

interface Props {
  product: any;
}

const ProductCell: React.FC<Props> = ({ product }) => (
  <Article>
    <a href={`https://rapha.cc/products/${product.code}`}>
      <p>{product.code}</p>

      <p>{product.name}</p>

      <Image src={product.imageURI} alt={product.name} aspect="1:1" />

      <p>{product.summary}</p>

      <p>{product.price.formatted}</p>
    </a>
  </Article>
);

export default ProductCell;
