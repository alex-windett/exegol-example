import tokens, { dim, mq } from "@rapharacing/tokens";
import styled from "styled-components";
import UI from "@rapharacing/rapha-ui";
import {
  ICityPartner,
  IClubhouseCity,
  IClubhouseRegion,
} from "@Types/clubhouses";

import { Link } from "@Server/withI18n";

const { Typography } = UI;

const Regions = styled.section`
  display: grid;
  margin-top: ${dim(2)};

  ${mq.sm`
    grid-template-columns: repeat(2, 1fr);
  `}

  ${mq.md`
    grid-template-columns: repeat(4, 1fr);
  `};
`;

const Region = styled.div`
  padding: 0 ${dim(2)};
  border-left: 1px solid ${tokens.color("grey15")};
`;

const Cities = styled.ul`
  border-top: 2px solid ${tokens.color("grey15")};
  list-style-type: none;
  margin: 0;
  padding: 0;
`;

const City = styled.li`
  padding: ${dim(4)} 0;
  & + & {
    border-top: 1px solid ${tokens.color("grey15")};
  }
`;

const CityDetail = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: ${dim(1)};
`;

const ComapnionApp = styled.div`
  display: none;
  text-align: center;

  margin: ${dim(3)} ${dim(3)} 0;

  ${mq.md`
    display: unset
  `};
`;

interface IProps {
  regions: IClubhouseRegion[];
}

const Locations = ({ regions }: IProps) => {
  return (
    <Regions>
      <ComapnionApp>
        <img
          src="/images/rcc/app-companion-locations.jpeg"
          alt="Find and join your local group on the app"
          title="Find and join your local group on the app"
        />

        <Typography.H4>Find and join your local group on the app</Typography.H4>

        <Link
          href={{
            pathname: "/",
          }}
          as="/"
          passHref
        >
          <Typography.Anchor title="Find Out More">
            Find Out More
          </Typography.Anchor>
        </Link>
      </ComapnionApp>

      {regions.map((region: IClubhouseRegion, ind: number) => (
        <Region key={ind}>
          <Typography.H3>{region.name}</Typography.H3>

          <Cities>
            {region.citiesCollection.items.map(
              (city: IClubhouseCity, ind: number) => (
                <City key={ind}>
                  <Typography.H4>{city.name}</Typography.H4>

                  {city.clubhouse && (
                    <CityDetail>
                      <span>Clubhouse:</span>
                      <Link
                        href={{
                          pathname: "/clubhouses/[slug]",
                          query: {
                            slug: city.clubhouse.slug,
                          },
                        }}
                        as={`/clubhouse/${city.clubhouse.slug}`}
                        passHref
                      >
                        <Typography.Anchor title={city.clubhouse.name}>
                          {city.clubhouse.name}
                        </Typography.Anchor>
                      </Link>
                    </CityDetail>
                  )}

                  {city.bikeHire && (
                    <CityDetail>
                      <span>Bike Hire:</span> <span>Yes</span>
                    </CityDetail>
                  )}

                  {city.membersGroup && (
                    <CityDetail>
                      <span>Members Group:</span> <span>Yes</span>
                    </CityDetail>
                  )}

                  {city.rideLeaders && (
                    <CityDetail>
                      <span>Ride Leaders:</span> <span>Yes</span>
                    </CityDetail>
                  )}

                  <>
                    {city.partnersCollection.items.map(
                      (partner: ICityPartner, ind: number) => (
                        <CityDetail key={ind}>
                          <span>Partner Cafe:</span>
                          <span>
                            <Typography.Anchor
                              target="_blank"
                              href={partner.url}
                            >
                              {partner.name}
                            </Typography.Anchor>
                          </span>
                        </CityDetail>
                      )
                    )}
                  </>
                </City>
              )
            )}
          </Cities>
        </Region>
      ))}
    </Regions>
  );
};

export default Locations;
