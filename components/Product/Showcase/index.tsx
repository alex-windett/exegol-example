import React from "react";
import UI from "@rapharacing/rapha-ui";
const { Carousel: RaphaCarousel } = UI;

import { ShowcaseCollection } from "@Types/product";
import { IContentfulAsset } from "@Types/productCategory";

const Showcase = ({ cms }: { cms: ShowcaseCollection }) => (
  <RaphaCarousel.FullWidthCarousel>
    {cms.showcaseCollection.items.map((i: IContentfulAsset, ind: number) => (
      <img key={ind} src={i.url} alt={i.title} title={i.title} />
    ))}
  </RaphaCarousel.FullWidthCarousel>
);

export default React.memo(Showcase);
