import styled from "styled-components";
import tokens, { dim, mq } from "@rapharacing/tokens";

import UI from "@rapharacing/rapha-ui";
import { formatRichText } from "@Utils/index";
import { IContentfulAsset } from "@Types/productCategory";
import { ISizeGuideInstructions } from "@Types/product";

const { Typography, Image } = UI;

const SizeReference = styled.section`
  display: grid;
  grid-column-gap: ${dim(2)};
  margin-top: ${dim(4)};
  grid-template-columns: repeat(6, 1fr);

  ${mq.md`
    grid-template-columns: repeat(12, 1fr);
  `}
`;

const Instructions = styled.div`
  text-align: left;
  grid-column: span 6;
  ${mq.md`
    grid-column: 1 / span 3;
  `}
`;

const Assets = styled.div`
  display: grid;
  grid-template-columns: 50% 50%;
  grid-column-gap: ${dim(2)};

  grid-column: span 6;

  ${mq.md`
    grid-column: 4 / span 9;
  `}

  img:only-child {
    grid-column: 1 / -1;
  }
`;

const SizingNotes = styled.div`
  ol {
    list-style: none;
    counter-reset: sizing-list-counter;
  }
  li {
    counter-increment: sizing-list-counter;
    position: relative;
  }
  li::before {
    content: counter(sizing-list-counter);
    background: transparent;
    width: ${dim(4)};
    height: ${dim(4)};
    border-radius: 50%;
    display: inline-block;
    line-height: 2rem;
    color: ${tokens.color("pink")};
    border: 2px solid ${tokens.color("pink")};
    text-align: center;
    position: absolute;
    left: ${dim(-6)};
    top: ${dim(-1)};
    font-weight: bold;
    font-family: ${tokens.get("type.fontFamily.heading")};
  }
`;

interface IProps {
  instructions: ISizeGuideInstructions;
  assets: { items: IContentfulAsset[] };
}

const SizingReference = ({ instructions, assets }: IProps) => {
  return (
    <SizeReference>
      <Instructions>
        {instructions && (
          <>
            <Typography.Label>Help with measuring</Typography.Label>
            <SizingNotes>{formatRichText(instructions)}</SizingNotes>
          </>
        )}
      </Instructions>

      {assets.items && (
        <Assets>
          {assets.items.map((asset: IContentfulAsset, ind: number) => (
            <Image
              key={ind}
              src={asset.url}
              alt={asset.title}
              title={asset.title}
              aspect="2:3"
            />
          ))}
        </Assets>
      )}
    </SizeReference>
  );
};

export default SizingReference;
