import styled from "styled-components";
import tokens, { dim, mq } from "@rapharacing/tokens";
import UI from "@rapharacing/rapha-ui";

import SizingReference from "./SizeReference";

const { Typography, SizeGuide } = UI;

const Section = styled.section`
  margin-top: ${dim(2)};
  padding: ${dim(1)} 0;
  border-top: 1px solid ${tokens.color("grey15")};

  ${mq.md`
    padding: ${dim(2)} 0;
    display: grid;
    grid-template-columns: 30% 70%;
    grid-gap: 10px;
  `};
`;

const Sizing = ({ sizeGuide, instructions, assets }: any) => {
  return (
    <Section>
      <Typography.VerticalHeading>
        <Typography.H4>Size Guide</Typography.H4>
      </Typography.VerticalHeading>

      <div>
        <SizeGuide sizeGuide={sizeGuide} />

        <SizingReference instructions={instructions} assets={assets} />
      </div>
    </Section>
  );
};

export default Sizing;
