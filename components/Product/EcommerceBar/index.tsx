import UI from "@rapharacing/rapha-ui";
import styled, { css } from "styled-components";
import tokens, { dim, mq } from "@rapharacing/tokens";
import { STOCK_VALUE } from "@Utils/constants";
import { IProduct, IProductSize, IProductCellColor } from "@Types/product";

const { Typography, Button } = UI;

const Div = styled.div`
  position: sticky;
  top: 0;
  z-index: 2;
  background-color: ${tokens.color("brand.black")};
  color: ${tokens.color("brand.white")};
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: ${dim(2)} ${dim(4)};
  transition: all ease-in-out 0.5s;

  ${({ isVisible }: { isVisible: boolean }) =>
    !isVisible &&
    css`
      opacity: 0;
      transition: all ease-in-out 0.3s;
      z-index: -10;
    `}
`;

const Actions = styled.div`
  display: flex;
  align-items: center;
`;

const Price = styled(Typography.H4)`
  display: none;

  ${mq.md`
    display: unset;
    margin: 0 ${dim(2)} 0 0;
  `}
`;

const WasPrice = styled(Price as any)`
  margin-left: ${dim(1)};
  color: ${tokens.color("grey15")};
  text-decoration: line-through;
`;

const Title = styled(Typography.H4)`
  margin: 0;
`;

interface IProps {
  product: IProduct;
  handleAddToBasket: (e: MouseEvent) => void;
  handleNotifyMe: (e: MouseEvent) => void;
  activeColor: IProductCellColor;
  activeSize: IProductSize | false;
  isVisible: boolean;
}

const ECommerceBar = ({
  product,
  handleAddToBasket,
  handleNotifyMe,
  activeColor,
  activeSize,
  isVisible,
}: IProps) => {
  const Buttons = () => {
    if (activeSize && activeSize.stockValue === STOCK_VALUE.COMING_SOON_STOCK) {
      return (
        <Button
          disabled={false}
          x
          loading={false}
          onClick={handleNotifyMe}
          type="button"
          variant="secondaryReversed"
        >
          Notify Me When Available
        </Button>
      );
    }

    return (
      <Button
        disabled={
          activeSize && activeSize.stockValue === STOCK_VALUE.OUT_OF_STOCK
        }
        loading={false}
        onClick={handleAddToBasket}
        type="button"
        variant="secondaryReversed"
      >
        {activeSize && activeSize.stockValue === STOCK_VALUE.OUT_OF_STOCK
          ? "Out Of Stock"
          : "Add To Basket"}
      </Button>
    );
  };

  return (
    <Div isVisible={isVisible}>
      <div>
        <Title>{product.name}</Title>
      </div>

      <Actions>
        <Price>{activeColor.price.formattedValue}</Price>
        {!!activeColor.wasPrice && (
          <WasPrice>{activeColor.wasPrice.formattedValue}</WasPrice>
        )}
        <Buttons />
      </Actions>
    </Div>
  );
};

export default ECommerceBar;
