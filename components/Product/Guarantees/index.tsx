import UI from "@rapharacing/rapha-ui";

const { Guarantees } = UI;

const ProductGuarantees = () => {
  const data = [
    {
      logo: "Guarantee",
      title: "Classics Guarantee",
      body:
        "Rapha Classics products come with a 30 day no-quibble riding guarantee. If you're not totally satisfied, return your worn Classics product for a full refund.",
    },
    {
      logo: "Repairs",
      title: "Free Repairs",
      body:
        "Rapha Classics products come with a 30 day no-quibble riding guarantee. If you're not totally satisfied, return your worn Classics product for a full refund.",
    },
    {
      logo: "Returns",
      title: "Free Returns",
      body:
        "Rapha Classics products come with a 30 day no-quibble riding guarantee. If you're not totally satisfied, return your worn Classics product for a full refund.",
    },
    {
      logo: "Downsize",
      title: "Jreset Downsize",
      body:
        "Rapha Classics products come with a 30 day no-quibble riding guarantee. If you're not totally satisfied, return your worn Classics product for a full refund.",
    },
  ];

  return <Guarantees data={data} />;
};

export default ProductGuarantees;
