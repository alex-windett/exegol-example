import React from "react";

import styled from "styled-components";
import { formatRichText } from "@Utils/index";
import { SizeGuide } from "@Components/Product";
import UI from "@rapharacing/rapha-ui";
import tokens, { mq, dim } from "@rapharacing/tokens";

import { isVideo, isImage } from "@Utils/index";

const {
  Typography,
  Image,
  Triptych,
  Carousel,
  VideoPlayer,
  Fit,
  StoryPlayer,
} = UI;

import {
  IProductCMS,
  IProductTripticItem,
  IFeaturesCarouselSlide,
} from "@Types/product";

const TwoCol = styled.section`
  ${mq.md`
    display: grid;
    grid-template-columns: 50% 50%;
  `}
`;

const CMSEntries = styled.div`
  > * {
    padding: ${dim(2)} 0;
    border-top: 1px solid ${tokens.color("grey15")};

    ${mq.md`
      padding: ${dim(4)} 0;
    `}
  }
`;

const ProductDetailsList = styled.ul`
  padding: 0;
  margin: 0;
  list-style-type: none;

  li + li {
    border-top: 1px solid ${tokens.color("grey15")};
  }

  ${mq.md`
    margin: ${dim(11)} 0;
  `}
`;

const CMS = ({ cms }: IProductCMS) =>
  !!cms && (
    <CMSEntries>
      <Triptych>
        {cms.triptic &&
          cms.triptic.assetsCollection.items.map(
            (item: IProductTripticItem, key: number) => (
              <>
                {isVideo(item.contentType) && (
                  <VideoPlayer autoPlay mediaSource={item.url} />
                )}
                {isImage(item.contentType) && (
                  <Image key={key} src={item.url} alt={item.title} />
                )}
              </>
            )
          )}
      </Triptych>

      {!!cms.video && (
        <StoryPlayer
          mediaSource={cms.video.video.url}
          title={cms.video.entryTitle}
          body={cms.video.description}
          autoPlay={cms.video.autoPlay}
          muted={cms.video.muted}
          poster={cms.video.posterImage.url}
        />
      )}

      {!!cms.description && (
        <TwoCol>
          <Typography.VerticalHeading>
            <Typography.H3>Description</Typography.H3>
          </Typography.VerticalHeading>

          <Typography.Body variant="sml">
            {formatRichText(cms.description)}
          </Typography.Body>
        </TwoCol>
      )}

      {!!cms.detailsAndMaterials && (
        <TwoCol>
          <Typography.VerticalHeading>
            <Typography.H3>Details and Materials</Typography.H3>
          </Typography.VerticalHeading>

          <ProductDetailsList>
            {cms.detailsAndMaterials.details.map(
              (detail: string, ind: number) => (
                <li key={ind}>
                  <Typography.Body variant="sml">{detail}</Typography.Body>
                </li>
              )
            )}
          </ProductDetailsList>
        </TwoCol>
      )}

      {!!cms.careInstructions && (
        <TwoCol>
          <Typography.VerticalHeading>
            <Typography.H3>Care Instructions</Typography.H3>
          </Typography.VerticalHeading>

          <ProductDetailsList>
            {cms.careInstructions.instructions.map(
              (instruction: string, ind: number) => (
                <li key={ind}>
                  <Typography.Body variant="sml">{instruction}</Typography.Body>
                </li>
              )
            )}
          </ProductDetailsList>
        </TwoCol>
      )}

      {!!cms.fitGuide && (
        <Fit
          description={cms.fitGuide.description}
          modelFitDescription={cms.fitGuide.modelFitDescription}
          modelFitImagesCollection={cms.fitGuide.modelFitImagesCollection}
          modelAttributes={formatRichText(cms.fitGuide.modelAttributes)}
          name={cms.fitGuide.name}
          primaryFit={cms.fitGuide.primaryFit}
          secondaryFit={cms.fitGuide.secondaryFit}
        />
      )}

      {!!cms.features && (
        <Carousel.FeatureCarousel>
          {cms.features.slidesCollection.items.map(
            (slide: IFeaturesCarouselSlide, ind: number) => (
              <Carousel.FeaturesSlide
                key={ind}
                feature={{
                  title: slide.slideTitle,
                  summary: slide.slideSummary,
                }}
                title={slide.slideMedia.title}
                url={slide.slideMedia.url}
              />
            )
          )}
        </Carousel.FeatureCarousel>
      )}

      {!!cms.sizeGuide && (
        <SizeGuide
          sizeGuide={cms.sizeGuide.sizingTable}
          instructions={cms.sizeGuide.instructions}
          assets={cms.sizeGuide.assetsCollection}
        />
      )}
    </CMSEntries>
  );

//@ts-ignore
export default React.memo(CMS);
