import UI from "@rapharacing/rapha-ui";
import { IProduct, IProductCellColor, IColorCMS } from "@Types/product";
import { IContentfulAsset } from "@Types/productCategory";
import { RAPHA_DEFAULT } from "@Utils/constants";

const { Carousel, Image } = UI;

interface IProps {
  activeColorCms: IColorCMS;
  product: IProduct;
  activeColor: IProductCellColor;
}

const ManequinCarousel = ({ activeColorCms, product, activeColor }: IProps) => (
  <Carousel.ManequinCarousel>
    {activeColorCms ? (
      activeColorCms.manequinImagesCollection.items.map(
        (image: IContentfulAsset, ind: number) => (
          <Image
            aspect="1:1"
            key={ind}
            src={image.url}
            alt={`${product.name} - ${activeColor.code} - ${activeColor.colorName}`}
            title={`${product.name} - ${activeColor.code} - ${activeColor.colorName}`}
          />
        )
      )
    ) : (
      <Image
        aspect="1:1"
        src={RAPHA_DEFAULT}
        alt={`${product.name} - ${activeColor.code} - ${activeColor.colorName}`}
        title={`${product.name} - ${activeColor.code} - ${activeColor.colorName}`}
      />
    )}
  </Carousel.ManequinCarousel>
);

export default ManequinCarousel;
