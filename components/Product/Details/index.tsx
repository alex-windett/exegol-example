import React from "react";
import UI from "@rapharacing/rapha-ui";
import styled from "styled-components";
import tokens, { dim, mq } from "@rapharacing/tokens";
import { STOCK_VALUE } from "@Utils/constants";
import { IProduct, IProductCellColor, IProductSize } from "@Types/product";

const { Typography, Button, Dropdown } = UI;

const Swatches = styled.div`
  display: inline-block;
`;

const SwatchWrapper = styled.div`
  ${(props: { checked: boolean }) =>
    props.checked && `border: 1px solid black;`}

  border-radius: 100%;
  display: inline-block;
  width: calc(${dim(5)} + 2px);
  height: calc(${dim(5)} + 2px);

  & + & {
    margin-left: ${dim()};
  }
`;

const Swatch = styled.button`
  appearance: none;
  border: none;
  background-color: ${({
    background = "transparent",
  }: {
    background: string;
  }) => background};

  appearance: none;
  width: ${dim(5)};
  height: ${dim(5)};
  border-radius: 100%;
  position: relative;
  top: 1px;
  left: 1px;
`;

const Grid = styled.div`
  ${mq.md`
    display: grid;
    grid-template-columns: 50% 50%;
    grid-row-gap: ${dim(8)};
    align-items: center;
  `}
`;

const WasPrice = styled.span`
  color: ${tokens.color("grey15")};
  text-decoration: line-through;
`;

const Colors = styled.div``;
const Sizes = styled.div``;

const Pricing = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex-direction: column;

  ${mq.md`
    flex-direction: row;
  `}
`;

const OtherActions = styled.div`
  margin: ${dim(4)} 0;

  justify-content: center;
  display: flex;

  ${mq.md`
    margin: unset;
    display: unset;
  `}

  button + button {
    padding-left: ${dim(2)};

    /* !Important Keeps the border on hover */
    border-left: 1px solid ${tokens.color("grey15")} !important;
  }
`;

const Price = styled(Typography.H4)`
  margin: 0 0 ${dim(2)};

  ${mq.md`
    margin: 0 ${dim(4)};
  `}
`;

interface IProps {
  product: IProduct;
  activeColor: IProductCellColor;
  activeSize: IProductSize | false;
  handleSizeChange: (e: React.ChangeEvent<HTMLSelectElement>) => void;
  handleNotifyMe: () => void;
  handleAddToBasket: () => void;
  handleAddWishlist: () => void;
  handleColorChange: (index: number) => void;
  forwardRef: any;
}

const ProductDetails = ({
  product,
  activeColor,
  handleColorChange,
  activeSize,
  handleSizeChange,
  handleNotifyMe,
  handleAddToBasket,
  handleAddWishlist,
  forwardRef,
}: IProps) => {
  const IN_STOCK =
    activeSize &&
    (activeSize.stockValue === STOCK_VALUE.IN_STOCK ||
      activeSize.stockValue === STOCK_VALUE.LOW_STOCK);
  return (
    <Grid ref={forwardRef}>
      <Colors>
        <Typography.Label variant="md">
          Colour: {activeColor.colorName}
        </Typography.Label>

        <Swatches>
          {product.colors &&
            product.colors.map((color: IProductCellColor, index: number) => {
              const swatch =
                color.cmsComponents.items.length > 0 &&
                color.cmsComponents.items[0].swatch;

              return (
                <SwatchWrapper checked={color.code === activeColor.code}>
                  <Swatch
                    key={index}
                    background={swatch}
                    type="button"
                    value={color.code}
                    title={color.colorName}
                    onClick={() => handleColorChange(index)}
                  />
                </SwatchWrapper>
              );
            })}
        </Swatches>
      </Colors>

      <Sizes>
        <Typography.Label variant="md">Size</Typography.Label>
        <Dropdown onChange={handleSizeChange}>
          <option disabled selected>
            Please select a size
          </option>
          {activeColor.sizes &&
            activeColor.sizes.map((size: IProductSize, index: number) => {
              const disabled =
                size.stockValue === STOCK_VALUE.OUT_OF_STOCK ||
                size.stockValue === STOCK_VALUE.COMING_SOON_STOCK;
              return (
                <option key={index} disabled={disabled} value={size.code}>
                  {size.name}{" "}
                  {size.stockValue === STOCK_VALUE.COMING_SOON_STOCK &&
                    "Coming Soon"}
                </option>
              );
            })}
        </Dropdown>
      </Sizes>

      <OtherActions>
        <Button onClick={handleAddWishlist} variant="link">
          Add to wishlist
        </Button>
        <Button variant="link">Sizing Guide</Button>
      </OtherActions>

      <Pricing>
        {activeColor.price.formattedValue && (
          <Price>
            {!!activeColor.wasPrice && (
              <>
                <WasPrice>{activeColor.wasPrice.formattedValue}</WasPrice>{" "}
              </>
            )}
            {activeColor.price.formattedValue}
          </Price>
        )}

        <div>
          {!activeSize && <Button disabled>Select a size</Button>}

          {activeSize && activeSize.stockValue === STOCK_VALUE.OUT_OF_STOCK && (
            <Button disabled>Out of stock</Button>
          )}

          {activeSize && IN_STOCK && (
            <>
              <Button onClick={handleAddToBasket}>Add To Basket</Button>
              {activeSize.stockValue === STOCK_VALUE.LOW_STOCK && (
                <span> &nbsp; (low stock)</span>
              )}
            </>
          )}

          {activeSize &&
            activeSize.stockValue === STOCK_VALUE.COMING_SOON_STOCK && (
              <Button onClick={handleNotifyMe}>Notify Me When Available</Button>
            )}
        </div>
      </Pricing>
    </Grid>
  );
};

export default ProductDetails;
