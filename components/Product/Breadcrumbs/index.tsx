import React from "react";
import tokens from "@rapharacing/tokens";
import UI from "@rapharacing/rapha-ui";
import styled from "styled-components";
import { Link } from "@WithTranslations";
import { IBreadcrumb } from "@Types/product";

const { Breadcrumb, Typography } = UI;

const { Body } = Typography;

interface IProps {
  breadcrumbs: {
    category: IBreadcrumb;
    subCategory: IBreadcrumb;
  };
  name: string;
}

const A = styled.a`
  color: ${tokens.color("black")};

  &:hover {
    color: ${tokens.color("pink")};
  }
`;

const BoldBody = styled(Body)`
  margin: 0;
  font-weight: ${tokens.get("type.fontWeights.bold")};
`;

const StyledBody = styled(Body)`
  margin: 0;
`;

const Breadcrumbs = ({ breadcrumbs, name }: IProps) => {
  return (
    <Breadcrumb>
      {breadcrumbs.category && (
        <Link
          href={{
            pathname: "/products/[category]",
            query: {
              category: breadcrumbs.category.code,
            },
          }}
          as={`/products/${breadcrumbs.category.code}`}
          passHref
        >
          <A>
            <BoldBody variant="xSml">{breadcrumbs.category.name}</BoldBody>
          </A>
        </Link>
      )}
      {breadcrumbs.subCategory && (
        <Link
          href={{
            pathname: "/products/[category]",
            query: {
              category: breadcrumbs.subCategory.code,
            },
          }}
          as={`/products/${breadcrumbs.subCategory.code}`}
          passHref
        >
          <A>
            <BoldBody variant="xSml">{breadcrumbs.subCategory.name}</BoldBody>
          </A>
        </Link>
      )}
      {name && <StyledBody variant="xSml">{name}</StyledBody>}
    </Breadcrumb>
  );
};

export default React.memo(Breadcrumbs);
