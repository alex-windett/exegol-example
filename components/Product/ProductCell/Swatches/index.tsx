import React from "react";
import styled from "styled-components";
import { Link } from "@Server/withI18n";
import { IProductCellColor } from "@Types/product";
import { mq, dim } from "@rapharacing/tokens";

const Container = styled.ul`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  margin: ${dim(2)} ${dim(2)} 0 0;
  padding: 0;
`;

const Item = styled.li`
  display: inline-block;
  padding: 0 0 ${dim(2)} ${dim(2)};
`;

const Swatch = styled.div`
  background-color: ${({ bg }: { bg: string }) => bg};
  border: 1px solid rgba(0, 0, 0, 0.15);
  border-radius: 100%;
  box-sizing: border-box;
  height: ${dim(3)};
  width: ${dim(3)};

  &:hover {
    cursor: pointer;
  }

  ${mq.lg`
    height: ${dim(2)};
    width: ${dim(2)};
  `}
`;

interface IProps {
  colors: IProductCellColor[];
  onChildMouseOver: (i: number) => void;
  sanitizedName: string;
}

const Swatches = ({ colors, onChildMouseOver, sanitizedName }: IProps) => {
  const handleColorChange = (i: number) => {
    onChildMouseOver(i);
  };

  return (
    <Container>
      {colors.map((color: IProductCellColor, i: number) => (
        <Item key={i}>
          <Link
            href={{
              pathname: "/product/[sku]/[name]",
              query: {
                sku: color.code,
                name: sanitizedName
              }
            }}
            as={`/product/${color.code}/${sanitizedName}`}
            passHref
          >
            <Swatch
              onMouseOver={() => handleColorChange(i)}
              bg={color.swatch}
            />
          </Link>
        </Item>
      ))}
    </Container>
  );
};

Swatches.defaultProps = {
  colors: []
};

Swatches.displayName = "Swatches";

export default Swatches;
