import React, { useState } from "react";
import styled from "styled-components";

import UI from "@rapharacing/rapha-ui";
import tokens, { dim, mq } from "@rapharacing/tokens";
import { IProductCellColor } from "@Types/product";

const { Typography } = UI;

import Details from "./Details";
import Mannequin from "./Mannequin";
import Swatches from "./Swatches";

const Container = styled.article`
  margin-bottom: ${dim(2)};
  margin-top: ${dim(4)};
  max-width: 100%;
  text-align: center;
`;

export const StyledLabel = styled(Typography.Label)`
  color: ${tokens.color("grey40")};
  margin-top: ${dim(3)};

  ${mq.md`
    margin-bottom: ${dim(4)};
  `}
`;

interface IProps {
  code: string;
  colors: IProductCellColor[];
  name: string;
  rating: number;
  marketingMessage: string;
  sanitizedName: string;
  showPrice: boolean;
  showRating: boolean;
  showMarketing: boolean;
  showSwatches: boolean;
}

const Product = ({
  colors,
  name,
  rating,
  sanitizedName,
  showPrice = true,
  showRating = true,
  showMarketing,
  showSwatches,
  marketingMessage
}: IProps) => {
  const [activeColorIndex, setActiveColorIndex] = useState(0);

  const [firstColor] = colors;

  const activeColor: IProductCellColor = colors[activeColorIndex] || firstColor;

  if (!activeColor) return <p>No Product Colors / Variants found</p>;

  const marketing = activeColor.marketingMessage || marketingMessage;

  return (
    <Container>
      {showMarketing && marketing && (
        <StyledLabel variant="md">{marketing}</StyledLabel>
      )}

      <Mannequin
        activeColor={activeColor}
        sanitizedName={sanitizedName}
        name={name}
      />

      <Details
        activeColor={activeColor}
        name={name}
        rating={rating}
        sanitizedName={sanitizedName}
        showPrice={showPrice}
        showRating={showRating}
      />

      {showSwatches && colors && (
        <Swatches
          colors={colors}
          onChildMouseOver={i => setActiveColorIndex(i)}
          sanitizedName={sanitizedName}
        />
      )}
    </Container>
  );
};

Product.defaultProps = {
  code: "",
  colors: [],
  name: "",
  rating: null,
  sanitizedName: "",
  showMarketing: true,
  showSwatches: true,
  marketingMessage: ""
};

export default Product;
