import React from "react";
import styled from "styled-components";

import UI from "@rapharacing/rapha-ui";
import { mq, dim } from "@rapharacing/tokens";

const { Stars } = UI;

const Container = styled.div`
  display: inline-block;
  height: 17px;
  margin: ${dim(3)} 0 0;
  max-width: 98px;
  overflow: hidden;
  position: relative;

  ${mq.md`
		margin-top: ${dim(4)};
	`}
`;

const Background = styled.span`
  ${({ rating }: { rating: number }) => `
		background: linear-gradient(
			90deg,
			rgba(0, 0, 0, 1) ${(rating / 5) * 100}%,
			rgba(204, 204, 204, 1) ${(rating / 5) * 100}%
    );
    height: ${dim(2)};
    left: 12px;
    position: absolute;
    top: 1px;
		width: 82px;
		z-index: -1;
	`}
`;

const Rating = ({ rating }: { rating: number }) => {
  const text = `${rating} / 5`;

  return (
    <Container>
      <Background rating={rating} />
      <Stars text={text} />
    </Container>
  );
};

Rating.defaultProps = {
  rating: 0
};

Rating.displayName = "Rating";

export default Rating;
