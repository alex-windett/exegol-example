import React, { Fragment } from "react";
import styled from "styled-components";
import { Link } from "@Server/withI18n";

import UI from "@rapharacing/rapha-ui";
import tokens, { dim } from "@rapharacing/tokens";

const { Typography } = UI;

import Rating from "../Rating";
import { IProductCellColor } from "@Types/product";

const Name = styled(Typography.Label)`
  margin: ${dim(1)} 0 0;
`;

const Price = styled(Typography.Body)`
  margin: ${dim(1)} 0 0;
`;

const WasPrice = styled(Typography.StrikeThrough)`
  margin-left: ${dim(1)};
  color: ${tokens.color("grey40")};
`;

interface IProps {
  activeColor: IProductCellColor;
  name: string;
  rating: number;
  showPrice: boolean;
  showRating: boolean;
  sanitizedName: string;
}

const Details = ({
  activeColor,
  name,
  rating,
  showPrice,
  showRating,
  sanitizedName
}: IProps) => (
  <Fragment>
    {showRating && !!rating && rating > 0 && <Rating rating={rating} />}

    {name && (
      <Link
        href={{
          pathname: "/product/[sku]/[name]",
          query: {
            sku: activeColor.code,
            name: sanitizedName
          }
        }}
        as={`/product/${activeColor.code}/${sanitizedName}`}
        passHref
      >
        <Typography.Anchor>
          <Name variant="md">{name}</Name>
        </Typography.Anchor>
      </Link>
    )}

    {showPrice && activeColor.price.formattedValue && (
      <Price variant="xSml">
        <span>{activeColor.price.formattedValue}</span>
        {activeColor.wasPrice && (
          <WasPrice>{activeColor.wasPrice.formattedValue}</WasPrice>
        )}
      </Price>
    )}
  </Fragment>
);

Details.displayName = "Details";

export default Details;
