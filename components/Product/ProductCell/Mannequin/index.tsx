import React, { Fragment } from "react";
import styled from "styled-components";
import UI from "@rapharacing/rapha-ui";

import { RAPHA_DEFAULT } from "@Utils/constants";
import { IProductCellColor } from "@Types/product";
import { Link } from "@Server/withI18n";

const { Image } = UI;

const Container = styled.div`
  padding-top: 100%; /* 1:1 Aspect Ratio */
  position: relative;
  width: 100%;

  img {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
  }
`;

interface IProps {
  activeColor: IProductCellColor;
  name: string;
  sanitizedName: string;
}

const Mannequin = ({ activeColor, name, sanitizedName }: IProps) => {
  const Asset = ({ src }: { src: string }) => (
    <Fragment>
      <Link
        href={{
          pathname: "/product/[sku]/[name]",
          query: {
            sku: activeColor.code,
            name: sanitizedName
          }
        }}
        as={`/product/${activeColor.code}/${sanitizedName}`}
        passHref
      >
        <a>
          <Container>
            <Image aspect="1:1" src={src} alt={name} />
          </Container>
        </a>
      </Link>
    </Fragment>
  );

  const { mannequins } = activeColor;

  if (!mannequins) {
    return <Asset src={RAPHA_DEFAULT} />;
  }

  const [first] = mannequins;

  const src = first ? first.url : RAPHA_DEFAULT;

  return <Asset src={src} />;
};

Mannequin.displayName = "MannequinImage";

export default Mannequin;
