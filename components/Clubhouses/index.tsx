import React from "react";
import styled from "styled-components";
import UI from "@rapharacing/rapha-ui";
import Link from "next/link";
import tokens, { dim, mq } from "@rapharacing/tokens";
import { IClubhouseRegionList, IClubhouseItem } from "@Types/clubhouses";

const { Typography } = UI;

const Section = styled.section`
  padding: ${dim(4)} 0;
  margin: ${dim(8)} ${dim(2)} 0;
  grid-template-columns: repeat(1, 1fr);

  ${mq.md`
    padding: ${dim(4)} 0;
    margin: ${dim(9)} ${dim(4)} 0;
    display: grid;
    border-top: 1px solid ${tokens.color("grey15")};
    grid-template-columns: repeat(3, 1fr);
  `}
`;

const Title = styled(Typography.H4)`
  margin: 0;
  padding: 0 ${dim(2)} 0 ${dim(5)};
  writing-mode: tb-rl;
  transform: rotate(-180deg);
  white-space: nowrap;
  text-align: right;
`;

const SubModule = styled.div`
  grid-column: span 4;
  padding-left: ${dim(2)};
  border-left: 1px solid ${tokens.color("grey15")};
`;

const SubTitle = styled(Typography.Label)`
  margin: 0;
  color: ${tokens.color("grey60")};
`;

const Region = styled.div`
  display: flex;
  padding-bottom: ${dim(4)};
  margin-bottom: ${dim(4)};
  border-top: 1px solid ${tokens.color("grey15")};
  padding-top: ${dim(4)};

  ${mq.md`
    border-top: unset;
    padding-top: unset;
  `}

  & + & {
    ${Title} {
      // On the right as the header has been rotated 180deg
      border-right: 1px solid ${tokens.color("grey15")};
    }
  }
`;

const ClubhousesModal = ({
  clubhouseRegionCollection,
}: IClubhouseRegionList) => (
  <Section>
    {clubhouseRegionCollection.items.map((region: any, ind: number) => (
      <Region key={ind}>
        <Title>{region.name}</Title>
        <SubModule>
          <SubTitle variant="md">Clubhouses &mdash;</SubTitle>
          {region.clubhousesCollection && (
            <div>
              {region.clubhousesCollection.items.map(
                (clubhouse: IClubhouseItem, i: number) => (
                  <Typography.Body variant="xSml">
                    <Link
                      key={i}
                      href={{
                        pathname: `/clubhouses/${clubhouse.slug}`,
                      }}
                      as={`/clubhouses/${clubhouse.slug}`}
                      passHref
                    >
                      <Typography.Anchor>{clubhouse.name}</Typography.Anchor>
                    </Link>
                  </Typography.Body>
                )
              )}
            </div>
          )}
        </SubModule>
      </Region>
    ))}
  </Section>
);

export default ClubhousesModal;
