import { Fragment } from "react";
import styled from "styled-components";
import UI from "@rapharacing/rapha-ui";
import tokens from "@rapharacing/token";
import { formatRichText } from "@Utils/index";
import { ICategoryCMSEntry, IHeroCarouselItem } from "@Types/productCategory";
import { CONTENTFUL } from "@Utils/constants";

const { Typography, Image, Carousel, VideoPlayer } = UI;

const Header = styled.header`
  text-align: center;
  border-bottom: 1px solid ${tokens.color("grey15")};
`;

const ProductsHeader = ({ category }: { category: ICategoryCMSEntry }) => {
  return (
    !!category && (
      <Header>
        <Typography.H1>{category.title}</Typography.H1>
        {!!category.description && (
          <Typography.Body variant="sml">
            {formatRichText(category.description)}
          </Typography.Body>
        )}

        {!!category.heroCarousel && (
          <Carousel.FullWidthCarousel>
            {category.heroCarousel.slidesCollection.items.map(
              (slide: IHeroCarouselItem, ind: number) => (
                <Fragment key={ind}>
                  {slide.__typename === CONTENTFUL.IMAGE && slide.image && (
                    <Image
                      src={slide.image.url}
                      alt={slide.image.title}
                      title={slide.image.title}
                      aspect={slide.aspect}
                    />
                  )}
                  {slide &&
                    slide.__typename === CONTENTFUL.VIDEO &&
                    slide.video && (
                      <VideoPlayer
                        fullwidth
                        mediaSource={slide.video.url}
                        poster={slide.posterImage && slide.posterImage.url}
                      />
                    )}
                </Fragment>
              )
            )}
          </Carousel.FullWidthCarousel>
        )}
      </Header>
    )
  );
};

export default ProductsHeader;
