import UI from "@rapharacing/rapha-ui";
import { formatRichText } from "@Utils/index";
import { Link } from "@Server/withI18n";
import { IProductCategoryContentSlot } from "@Types/productCategory";
import { TYPE_NAMES } from "@Utils/constants";
import { isVideo, isImage } from "@Utils/index";

const { PLPContentCard, Typography } = UI;

const ContentCard = ({
  content,
  t,
}: {
  content: IProductCategoryContentSlot;
  t: any;
}) => {
  if (content.__typename === TYPE_NAMES.STORY) {
    const category =
      content.categoryCollection && content.categoryCollection.items[0].slug;

    const subCategory =
      content.subCategoryCollection &&
      content.subCategoryCollection.items[0].slug;

    const slug = content.slug;

    const Route = () => (
      <Link
        href={{
          pathname: "/stories/[category]/[subCategory]/[slug]",
          query: {
            category,
            subCategory,
            slug,
          },
        }}
        as={`/stories/${category}/${subCategory}/${slug}`}
        passHref
      >
        <Typography.Anchor>{t("common_read_more")}</Typography.Anchor>
      </Link>
    );

    return (
      <PLPContentCard
        heading={content.heading}
        body={content.subHeading}
        image={content.heroMedia && { src: content.heroMedia.url }}
        Link={Route}
      />
    );
  }

  if (content.__typename === TYPE_NAMES.PRODUCT_CATEGORY_CONTENT) {
    return (
      <PLPContentCard
        heading={content.heading}
        body={content.body && formatRichText(content.body)}
        image={
          content.asset &&
          isImage(content.asset.contentType) && { url: content.asset.url }
        }
        video={
          content.asset &&
          isVideo(content.asset.contentType) && {
            mediaSource: content.asset.url,
          }
        }
      />
    );
  }

  return null;
};

export default ContentCard;
