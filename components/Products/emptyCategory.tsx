import { SingletonRouter } from "next/router";
import styled from "styled-components";
import UI from "@rapharacing/rapha-ui";
const { Typography } = UI;

const Header = styled.header`
  text-align: center;
`;

const EmptyCategory = ({ router }: { router: SingletonRouter }) => (
  <Header>
    <Typography.H1>{router.query.category}</Typography.H1>
    <Typography.H3>
      We couldn't find any products for {router.query.category}
    </Typography.H3>
  </Header>
);

export default EmptyCategory;
