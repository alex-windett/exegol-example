import styled from "styled-components";
import tokens, { dim } from "@rapharacing/tokens";
import UI from "@rapharacing/rapha-ui";
import { IBasketWishlist } from "@Types/basketWishlist";
import { Link } from "@WithTranslations";

const { Typography, Button } = UI;

const { Label } = Typography;

const Summary = styled.section`
  border-top: 1px solid ${tokens.color("grey15")};
  width: 100%;
  position: absolute;
  bottom: 0;
  left: 0;
  background: ${tokens.color("brand.white")};
`;

const SummaryPad = styled.div`
  padding: ${dim(2)} ${dim(4)} ${dim(4)};
`;

const SummaryRow = styled.div`
  display: flex;
  justify-content: space-between;
`;

const SummaryAction = styled(Button)`
  width: 100%;
  max-width: 100%;
`;

const WishlistActions = styled.div`
  display: grid;
  grid-template-columns: 50% 50%;
  grid-column-gap: ${dim(2)};
`;

const Title = styled(Label)`
  margin-top: 0;
`;

interface IProps {
  wishlistActive: boolean;
  data: IBasketWishlist;
}

const MiniSummary = ({ wishlistActive, data }: IProps) => {
  if (!data) return null;

  if (wishlistActive) {
    return (
      <Summary>
        <SummaryPad>
          <SummaryRow>
            <Title>Share</Title>
            <Title>sahre stuff here</Title>
          </SummaryRow>

          {data.wishlist && data.wishlist.subTotalPrice && (
            <SummaryRow>
              <Title>Subtotal</Title>
              <Title>{data.wishlist.subTotalPrice.formattedValue}</Title>
            </SummaryRow>
          )}

          <WishlistActions>
            <SummaryAction variant="secondary">Edit Wishlsit</SummaryAction>
            <SummaryAction>Move All to Basket</SummaryAction>
          </WishlistActions>
        </SummaryPad>
      </Summary>
    );
  }

  return (
    <Summary>
      <SummaryPad>
        {data.basket.deliveryMode && (
          <SummaryRow>
            <Title>Delivery</Title>
            <Title>
              {data.basket.deliveryMode.price.formattedValue === 0
                ? "Free"
                : data.basket.deliveryMode.price.formattedValue}
            </Title>
          </SummaryRow>
        )}

        {data.basket.subTotalPrice && (
          <SummaryRow>
            <Title>Subtotal</Title>
            <Title>{data.basket.subTotalPrice.formattedValue}</Title>
          </SummaryRow>
        )}

        <Link href="/cart" passHref>
          <a>
            <SummaryAction>Go To Basket</SummaryAction>
          </a>
        </Link>
      </SummaryPad>
    </Summary>
  );
};

export default MiniSummary;
