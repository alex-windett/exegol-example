export const bezierTiming = "cubic-bezier(0.23, 1, 0.32, 1)";
export const transition = `height .6s ${bezierTiming}, opacity .4s ease-in-out, visibility .4s ease-in-out, transform .3s ease-in-out`;