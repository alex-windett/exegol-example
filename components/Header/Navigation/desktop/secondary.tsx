import React, { useContext } from "react";
import styled, { css } from "styled-components";
import tokens, { dim } from "@rapharacing/tokens";
import UI from "@rapharacing/rapha-ui";

import { INavigationItem } from "@Types/layout";
import { HeaderContext } from "@Containers/Header";
import { Link } from "@Server/withI18n";
import { INavigationContext } from "@Types/layout";

import Tertiary from "./tertiary";
import { transition } from "../utils";

const { Typography } = UI;
const { Label, Anchor } = Typography;

const Container = styled.div<{ height: number | null; navOpen: boolean }>`
  background: ${tokens.color("white")};
  height: 0;
  left: -${dim(4)};
  padding: 0 ${dim(4)};
  position: absolute;
  top: ${dim(5)};
  transition: ${transition};
  visibility: hidden;
  width: calc(100vw - ${dim(8)});

  ${({ navOpen }) =>
    navOpen &&
    css`
      height: ${({ height }: { height: number | null }) => height}px;
      visibility: visible;

      &:before {
        background: ${tokens.color("grey15")};
        content: "";
        height: 1px;
        left: ${dim(4)};
        position: absolute;
        top: 0;
        width: calc(100% - ${dim(8)});
      }
    `};
`;

const List = styled.ol<{ isActive: boolean }>`
  display: grid;
  grid-template-columns: repeat(6, 1fr);
  list-style: none;
  margin: 0;
  opacity: 0;
  position: absolute;
  padding: ${dim(6)} 0 ${dim(4)};
  transition: opacity 0.4s ease-in-out, transform 0.3s ease-in-out,
    visibility 0.4s ease-in-out;
  visibility: hidden;
  width: calc(100% - ${dim(8)});

  ${({ isActive }) =>
    isActive &&
    css`
      opacity: 1;
      visibility: visible;
    `};
`;

const Item = styled.li<{ isActive: boolean }>`
  transform: translateX(${dim(6)});
  transition: ${transition};

  ${({ isActive }) =>
    isActive &&
    css`
      transform: translateX(0);
    `};

  & + & {
    border-left: 1px solid ${tokens.color("grey15")};
    padding-left: ${dim(2)};
  }
`;

const A = styled(Anchor)`
  margin: 0;
  padding: 0 0 ${dim(2)};
  text-decoration: none;
`;

interface IProps {
  activeIndex: number | null;
  height: number | null;
  setRef: (ref: HTMLElement | null, navIndex: number) => void;
}

const Secondary = ({ activeIndex = null, height = null, setRef }: IProps) => {
  // @ts-ignore
  const { navigation, navOpen } = useContext<INavigationContext>(HeaderContext);

  return (
    <Container height={height} navOpen={navOpen}>
      {navigation.map((nav: INavigationItem, navIndex: number) => (
        <List
          key={navIndex}
          isActive={activeIndex === navIndex && navOpen}
          ref={(secondaryRef) => setRef(secondaryRef, navIndex)}
        >
          {nav.items &&
            nav.items.map((secondary: INavigationItem, i: number) => (
              (secondary.url && <Item key={i} isActive={activeIndex === navIndex}>
                <Link href="/[...params]" as={secondary.url} passHref>
                  <A>
                    <Label>{secondary.title}</Label>
                  </A>
                </Link>
                {secondary.items && <Tertiary secondary={secondary} />}
              </Item>)
            ))}
        </List>
      ))}
    </Container>
  );
};

export default Secondary;
