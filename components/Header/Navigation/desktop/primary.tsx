import React from "react";
import styled from "styled-components";
import tokens, { dim } from "@rapharacing/tokens";
import UI from '@rapharacing/rapha-ui'

const { Typography } = UI;

const { Label } = Typography

const Item = styled.li`
	line-height: ${dim(4)};
	margin: 0 ${dim(4)} 0 0;
`;

const Link = styled.a`
	cursor: pointer;
	text-decoration: none;
`;

const StyledLabel = styled(Label)`
	border-bottom: 1px solid transparent;
	color: ${({ isActive }) =>
		isActive ? `${tokens.color("pink")}` : `${tokens.color("black")}`};
	margin: 0;
	padding: 0;
	transition: border-bottom ease-in-out 0.2s;
`;

interface IProps {
	isActive: boolean;
	onMouseOver: (event: React.MouseEvent<HTMLElement>) => void;
	nav: {
		title: string;
	}
}

const Primary = ({
	isActive,
	nav = {
		title: ""
	},
	onMouseOver
}: IProps) => (
	<Item>
		<Link onMouseOver={onMouseOver}>
			<StyledLabel isActive={isActive}>{nav.title}</StyledLabel>
		</Link>
	</Item>
);

export default Primary;
