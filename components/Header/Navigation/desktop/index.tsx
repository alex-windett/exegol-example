import React, {
  createRef,
  Fragment,
  useContext,
  useEffect,
  useState,
} from "react";
import styled from "styled-components";

import Primary from "./primary";
import Secondary from "./secondary";

import { HeaderContext } from "@Containers/Header";
import { INavigationItem, INavigationContext } from "@Types/layout";

const Flex = styled.div`
  align-items: center;
  display: flex;
  grid-column: 1 / span 5;
  position: relative;
  z-index: 5;
`;

const List = styled.ol`
  display: flex;
  list-style: none;
  margin: 0;
  padding: 0;
`;

const Nav = () => {
  const { navigation, navOpen, setNavOpen } = useContext<INavigationContext>(
    // @ts-ignore
    HeaderContext
  );

  const [activeIndex, setActiveIndex] = useState<null | number>(null);
  const [height, setHeight] = useState(0);

  const navRef = createRef<HTMLElement>();
  const secondaryRef: HTMLElement[] = [];

  useEffect(
    () =>
      document &&
      document.addEventListener(
        "mousedown",
        (e) =>
          navRef.current &&
          // @ts-ignore
          !navRef.current.contains(e.target) &&
          setNavOpen(false)
      )
  );

  const calculateRefHeight = (i: number) => {
    let ref = secondaryRef && secondaryRef[i];

    setHeight(ref.offsetHeight);
  };

  const setRef = (ref: HTMLElement | null, i: number) => {
    if (ref) {
      secondaryRef[i] = ref;
    }
  };

  return (
    <Fragment>
      <Flex>
        {navigation && (
          <Fragment>
            <nav role="navigation" ref={navRef}>
              <List>
                {navigation.map((nav: INavigationItem, i: number) => {
                  const handleMouseOver = () => {
                    setActiveIndex(i);
                    calculateRefHeight(i);
                    setNavOpen(true);
                  };

                  return (
                    <Primary
                      key={i}
                      nav={nav}
                      onMouseOver={handleMouseOver}
                      isActive={navOpen && i === activeIndex}
                    />
                  );
                })}
              </List>
            </nav>
          </Fragment>
        )}

        <Secondary activeIndex={activeIndex} height={height} setRef={setRef} />
      </Flex>
    </Fragment>
  );
};

export default Nav;
