import React from "react";
import styled from "styled-components";
import tokens, { dim } from "@rapharacing/tokens";
import { Link } from '@WithTranslations'
import UI from '@rapharacing/rapha-ui'
import { INavigationItem } from '@Types/layout'

const { Typography } = UI;

const { Body } = Typography

const List = styled.ol`
  list-style: none;
	margin: 0;
	padding: 0 0 ${dim(3)};
`;

const Item = styled.li`
  color: ${tokens.color("black")};
  display: list-item;
	margin-bottom: 0;
	margin-top: 0;
	padding-right: ${dim(1)};
	text-decoration: none;

  &:not(:last-child) {
    margin-bottom: ${dim(1)};
  }

	&:hover, *:hover  {
		color: ${tokens.color("pink")};
	}
`;

const StyledBody = styled(Body)`
	margin: 0;
`;

const A = styled.a`
  cursor: pointer;
  text-decoration: none;
  color: ${tokens.color("black")};
`;

interface IProps {
  secondary: {
    items?: INavigationItem[]
  }
}

const Tertiary = ({
  secondary = {}
}: IProps) => 
  <List>
    {secondary.items && secondary.items.map((tertiary: INavigationItem, i: number) => (
      (tertiary.url && <Item key={i}>
        <Link href="/[...params]" as={tertiary.url} passHref>
          <A>
            <StyledBody variant="xSml" >
              {tertiary.title}
            </StyledBody>
          </A>
        </Link>
      </Item>)
    ))}
  </List>

export default Tertiary;