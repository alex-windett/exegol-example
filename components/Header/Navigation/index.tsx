import React, { useContext } from "react";

import Desktop from "./desktop";
import Mobile from "./mobile";

import { HeaderContext } from "@Containers/Header";

const Navigation = () => {
  // @ts-ignore
  const { desktopNav, navigation } = useContext(HeaderContext);

  const Nav = desktopNav ? Desktop : Mobile;

  return <Nav />;
};

export default Navigation;
