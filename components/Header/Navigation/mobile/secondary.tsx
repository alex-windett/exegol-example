import React, { useContext } from "react";
import styled from "styled-components";
import tokens, { dim, mq } from "@rapharacing/tokens";
import UI from "@rapharacing/rapha-ui";

import { Link } from "@WithTranslations";
import { HeaderContext } from "@Containers/Header";

import { bezierTiming } from "../utils";

import Tertiary from "./tertiary";
import { INavigationItem } from "@Types/layout";

const { Typography } = UI;

const { Label, Anchor } = Typography;

const Container = styled.div<{ transition: string }>`
  display: grid;
  grid-template-rows: 1fr;
  grid-template-columns: repeat(6, 1fr);
  height: auto;
  padding-left: 0;
  transition: max-height 1s ${bezierTiming}, overflow 1s ${bezierTiming};

  ${({ transition }) => {
    switch (transition) {
      case "entering":
        return "max-height: 0; overflow: hidden;";
      case "entered":
        return `max-height: ${dim(100)}; overflow: visible;`;
      case "exiting":
        return `max-height: ${dim(100)}; overflow: visible;`;
      case "exited":
        return "max-height: 0; overflow: hidden;";
      default:
        break;
    }
  }}

  ${mq.md`
		grid-template-columns: repeat(12, 1fr);
  `}
`;

const List = styled.ol`
  grid-column: 2 / span 5;
  list-style-type: none;
  margin: 0;
  padding: 0;

  &:last-child {
    border-bottom: none;
  }

  ${mq.md`
		grid-template-columns: repeat(12, 1fr);
		grid-column: 2 / span 11;
  `}
`;

const Item = styled.li`
  align-items: flex-start;
  border-top: 1px solid ${tokens.color("grey15")};
  cursor: pointer;
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  line-height: ${dim(4)};
  margin-left: 0;
  min-height: ${dim(7)};
  width: 100%;

  &:hover span {
    color: ${tokens.color("pink")};
  }
`;

const A = styled(Anchor)`
  margin: 0;
  text-decoration: none;
`;

const Secondary = ({ transition = "" }) => {
  // @ts-ignore
  const { navigation } = useContext(HeaderContext);

  return (
    <Container transition={transition}>
      {navigation
        ? navigation.map((nav: INavigationItem, navIndex: number) => (
            <List key={navIndex}>
              {nav.items &&
                nav.items.map((secondary: INavigationItem, i: number) => (
                  (secondary.url && <Item key={i}>
                    <Link href="/[...params]" as={secondary.url} passHref>
                      <A>
                        <Label>{secondary.title}</Label>
                      </A>
                    </Link>
                    <Tertiary secondary={secondary} i={i} />
                  </Item>)
                ))}
            </List>
          ))
        : null}
    </Container>
  );
};

export default Secondary;
