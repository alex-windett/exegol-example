import React, { useState, useContext } from "react";
import classNames from "classnames";
import styled from "styled-components";
import { dim } from "@rapharacing/tokens";

import { HeaderContext } from "@Containers/Header";
import Primary from "./primary";

import styles from "./styles.min.js";

const Flex = styled.div`
	display: flex;
	grid-column: 1 / span 1;
	height: ${dim(8)};
	position: relative;
	z-index: 5;
`;

export const HamburgerButton = styled.button`
	position: relative;
	z-index: 3;
`;

const Wrapper = styled.span`
	${styles}
`;

const Nav = () => {
	// @ts-ignore
	const { navigation, navOpen, setNavOpen } = useContext(HeaderContext);

	const [subMenuOpen, setSubMenuOpen] = useState(false);

	const buttonClasses = classNames({
		hamburger: true,
		"hamburger--spin": true,
		"is-active": navOpen
	});

	return (
		<Wrapper>
			<Flex>
				<HamburgerButton
					className={buttonClasses}
					type="button"
					onClick={() => setNavOpen(!navOpen)}
					aria-label="Open"
				>
					<span className="hamburger-box">
						<span className="hamburger-inner" />
					</span>
				</HamburgerButton>

				<Primary
					// @ts-ignore
					navigation={navigation}
					isActive={navOpen}
					setMenuOpen={setNavOpen}
					subMenuOpen={subMenuOpen}
					setSubMenuOpen={setSubMenuOpen}
				/>
			</Flex>
		</Wrapper>
	);
};

export default Nav;
