import React, { Fragment, useState, useContext, useEffect } from "react";
import styled from "styled-components";
import { Transition } from "react-transition-group";
import tokens, { dim, mq } from "@rapharacing/tokens";
import UI from '@rapharacing/rapha-ui'

import { HeaderContext } from "@Containers/Header";
import Secondary from "./secondary";
import { INavigationItem } from "@Types/layout";

const { SVG, Typography } = UI
const { Label } = Typography
const { Caret } = SVG

const Nav = styled.nav<{ navOpen: boolean }>`
	display: grid;
	grid-template-rows: 1fr;
	grid-template-columns: repeat(6, 1fr);
	grid-column: 1 / span 6;
	background: ${tokens.color("white")};
	left: -${dim(2)};
	min-height: 100vh;
	overflow: hidden;
	padding: 0 ${dim(2)};
	position: absolute;
	top: ${dim(8)};
	transform: translateX(-100%);
	transition: all 0.6s cubic-bezier(0.23, 1, 0.32, 1);
	visibility: hidden;
	width: calc(100vw - ${dim(4)});
	z-index: 2;

	${mq.md`
		grid-template-columns: repeat(12, 1fr);
		grid-column: 1 / span 12;
    left: -${dim(4)};
  `}

	${({ navOpen }) =>
		navOpen &&
		`
      transform: translateX(0);
      visibility: visible;
  `};
`;

const List = styled.ol`
	grid-column: 1 / span 6;
	padding: 0;
	margin: 0;

	${mq.md`
		grid-column: 3 / span 8;
  `}
	
`;

const Link = styled.li<{ isOpen: boolean }>`
	align-items: center;
	border-top: 1px solid ${tokens.color("grey15")};
	cursor: pointer;
	display: block;
	justify-content: space-between;
	line-height: ${dim(4)};
	margin-left: 0;
	min-height: ${dim(7)};
	width: 100%;

	&:hover span {
		color: ${tokens.color("pink")};
	}

	&:last-of-type {
		border-bottom: 1px solid ${tokens.color("grey15")};
	}
`;

const Trigger = styled.button<{ isOpen: boolean }>`
	transition: transform 150ms ease-in-out;
	background: none;
	border: none;
	margin: ${dim(1)};
	position: relative;
	overflow: hidden;
	display: flex;
	transform: ${({ isOpen }) => isOpen && `rotate(180deg)`};
`;

const Anchor = styled.a`
	align-items: center;
	display: flex;
	justify-content: space-between;
	transition: color 0.2s ease-in-out 0s;

	&:hover {
		color: ${tokens.color("pink")};
	}
`;

const Primary = () => {
	// @ts-ignore
	const { navigation, navOpen } = useContext(HeaderContext);

	const [isOpen, setIsOpen] = useState([]);

	useEffect(() => {
		let defaultOpenState: any = [];

		navigation.forEach(() => defaultOpenState.push(false));

		setIsOpen(defaultOpenState);
	}, []);

	return (
		<Nav navOpen={navOpen} role="navigation">
			<List>
				{navigation.map((primary: INavigationItem, i: number) => {
					const handleOnClick = () => {
						const isOpenArray = [...isOpen];

						// @ts-ignore
						isOpenArray[i] = !isOpenArray[i];

						setIsOpen(isOpenArray);
					};

					return (
						<Fragment key={`${primary.url}-${i}`}>
							<Link
								isOpen={isOpen[i]}
								onClick={handleOnClick}
								aria-label={`Open ${primary.title} Navigation`}
							>
								<Anchor>
									<Label isActive={navOpen}>{primary.title}</Label>
									<Trigger type="button" isOpen={isOpen[i]}>
										<Caret direction="down" />
									</Trigger>
								</Anchor>

								<Transition timeout={150} in={isOpen[i]}>
									{state => <Secondary transition={state} />}
								</Transition>
							</Link>
						</Fragment>
					);
				})}
			</List>
		</Nav>
	);
};

export default Primary;