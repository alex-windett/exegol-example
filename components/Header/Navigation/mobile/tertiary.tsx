import React from "react";
import styled from "styled-components";
import tokens, { dim } from "@rapharacing/tokens";
import UI from '@rapharacing/rapha-ui'
import { INavigationItem } from '@Types/layout'

import { Link } from '@WithTranslations'

const { Typography } = UI;

const { Anchor } = Typography

const Container = styled.ol`
	list-style-type: none;
	margin: 0;
	padding: 0;
  width: 100%;

	&:last-child {
		border-bottom: none;
	}
`;

const Item = styled.li`
	align-items: center;
	border-top: 1px solid ${tokens.color("grey15")};
	cursor: pointer;
	display: flex;
	justify-content: space-between;
	line-height: ${dim(4)};
	margin-left: 0;
	min-height: ${dim(7)};
	width: 100%;

	&:hover span {
		color: ${tokens.color("pink")};
	}
`;

const A = styled(Anchor)`
  font-weight: ${tokens.get("type.fontWeights.normal")};
  text-decoration: none;
`;

interface IProps {
	secondary: INavigationItem,
	i: number
}

const Tertiary = ({
	secondary,
	i
}: IProps) => {
	return (
    <Container key={`${secondary.title}-${i}`}>
      {secondary.items && secondary.items.map((tertiary: INavigationItem, i: number) => (
				(tertiary.url && <Item key={`${tertiary.url}-${i}`}>
					<Link href="/[...params]" as={tertiary.url} passHref> 
						<A>{tertiary.title}</A>
					</Link>
        </Item>)
      ))}
    </Container>
	);
};

export default Tertiary;
