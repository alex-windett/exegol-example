import styled from "styled-components";
import { dim } from "@rapharacing/token";

const Div = styled.div`
  margin: 0 ${dim(4)};
`;

export default ({ children }: any) => <Div>{children}</Div>;
