export default () => (
  <div className="icon checkbox-tick-icon">
    <svg
      width="11px"
      height="11px"
      viewBox="0 0 11 11"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g transform="translate(-500.000000, -1030.000000)" fill="#FFFFFF">
        <g transform="translate(480.000000, 932.000000)">
          <g transform="translate(17.000000, 96.000000)">
            <g transform="translate(8.283081, 7.191033) rotate(-45.000000) translate(-8.283081, -7.191033) translate(3.783081, 4.691033)">
              <rect
                x="-4.4211548e-14"
                y="-4.4211548e-14"
                width="1.55555556"
                height="4.66666667"
              ></rect>
              <rect
                x="1.55555556"
                y="3.11111111"
                width="7"
                height="1.55555556"
              ></rect>
            </g>
          </g>
        </g>
      </g>
    </svg>
  </div>
);
