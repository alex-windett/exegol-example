import styled from "styled-components";
import tokens, { dim, mq } from "@rapharacing/tokens";

const FieldGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-column-gap: ${dim(4)};

  ${mq.md`
    grid-template-columns: ${({ fields }) => `repeat(${fields}, 1fr)`};
    margin-bottom: ${dim(4)};

    &:first-child {
      grid-template-columns: repeat(3, 1fr);
      div {
        &:first-child {
          grid-column: 1 / span 2;
        }

        &:last-child {
          grid-column: 1 / span 3;
        }
      }
    }
  `}
`;

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(6, 1fr);

  ${mq.md`
    grid-template-columns: repeat(8, 1fr);
  `}

  ${mq.lg`
    grid-template-columns: repeat(12, 1fr);
  `}
`;

const AddressContainer = styled.div`
  grid-column: 1 / span 6;

  ${mq.md`
    grid-column: 1 / span 8;
  `}

  ${mq.lg`
    grid-column: 2 / span 6;
  `}
`;

export { AddressContainer, FieldGroup, Wrapper };
