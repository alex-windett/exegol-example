import React from "react";
import styled from "styled-components";

import tokens, { dim } from "@rapharacing/tokens";

export const ErrorMessage = styled.span`
  bottom: -${dim(3)};
  display: block;
  font-family: ${tokens.get("type.fontFamily.body")};
  font-size: 12px;
  font-weight: ${tokens.get("type.fontWeights.normal")};
  position: absolute;
`;

const Error = (props) => {
  return <ErrorMessage id={props.id}>{props.children}</ErrorMessage>;
};

export default Error;
