import React from "react";

import CheckboxTick from "@Components/Svgs/checkboxTick";

import {
  Checkmark,
  CheckInner,
  Container,
  StyledCheckbox,
  StyledLabel,
  StyledText,
} from "./ui";

export default ({
  blurred,
  changed,
  focussed,
  label,
  messageType,
  options,
  ...props
}) => (
  <Container>
    {options.map((option, index) => (
      <StyledLabel key={index}>
        <StyledCheckbox
          type="checkbox"
          name={props.name}
          label={option.title}
          required
        />
        <Checkmark>
          <CheckInner>
            <CheckboxTick />
          </CheckInner>
        </Checkmark>
        <StyledText>{option.title}</StyledText>
      </StyledLabel>
    ))}
  </Container>
);
