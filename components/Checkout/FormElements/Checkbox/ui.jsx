import styled from "styled-components";
import tokens, { dim } from "@rapharacing/tokens";

const StyledText = styled.span`
  font-family: ${tokens.get("type.fontFamily.body")};
  font-size: 12px;
  line-height: ${tokens.get("type.lineHeight.text")};
  margin-left: ${dim(2)};
`;

const Checkmark = styled.span`
  background-color: transparent;
  border: 1px solid ${tokens.color("black")};
  border-radius: 3px;
  height: ${dim(2)};
  left: 0;
  position: absolute;
  top: 0;
  width: ${dim(2)};
`;

const CheckInner = styled.span`
  display: none;
  left: 50%;
  position: absolute;
  top: 50%;
  transform: translate(-50%, -50%);
`;

const StyledCheckbox = styled.input`
  opacity: 0;
  position: absolute;

  &:checked ~ ${Checkmark} {
    background-color: ${tokens.color("black")};
  }

  &:checked ~ ${Checkmark} ${CheckInner} {
    display: block;
  }
`;

const StyledLabel = styled.label`
  cursor: pointer;
  display: block;
  margin-bottom: ${dim(1)};
  padding-left: ${dim(1)};
  position: relative;
  user-select: none;

  &:last-of-type {
    margin-bottom: 0;
  }

  &:hover {
    ${StyledCheckbox}:not(:checked) ~ ${Checkmark} {
      border-color: ${tokens.color("pink")};
    }

    ${StyledCheckbox}:not(:checked) ~ ${StyledText} {
      color: ${tokens.color("pink")};
    }
  }
`;

const Container = styled.div`
  margin: ${dim(2)} 0 0;
`;

export {
  Checkmark,
  CheckInner,
  Container,
  StyledCheckbox,
  StyledText,
  StyledLabel,
};
