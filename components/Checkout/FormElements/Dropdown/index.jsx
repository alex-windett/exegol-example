import React, { Fragment, useState } from "react";

import tokens, { dim } from "@rapharacing/tokens";

import Select from "react-select";
import { useFormikContext, useField } from "formik";

import Error from "../Error";
import Label from "../Label";

import { Container, Wrapper } from "./ui";

const Dropdown = ({ options, label, ...props }) => {
  const { setFieldValue, setFieldTouched } = useFormikContext();
  const [field, { error, touched }] = useField(props);
  const [focussed, setFocussed] = useState(false);

  const customStyles = {
    control: (provided) => ({
      ...provided,
      border: `1px solid ${tokens.color("grey15")}`,
      borderRadius: `3px`,
      boxSizing: `unset`,
      fontFamily: `${tokens.get("type.fontFamily.body")}`,
      fontWeight: `${tokens.get("type.fontWeights.normal")}`,
      fontSize: `12px`,
      height: `${dim(5)}`,
    }),
  };

  function handleOptionChange(selection) {
    setFieldValue(props.name, selection);

    if (selection) {
      setFocussed(true);
    } else {
      setFocussed(false);
    }
  }

  function handleFocus() {
    setFocussed(true);
  }

  function updateBlur(event) {
    setFieldTouched(props.name, true);
  }

  return (
    <Wrapper>
      <Container>
        <Label htmlFor={props.id} focussed={focussed}>
          {label} {!props.required ? <span>(optional)</span> : null}
        </Label>
        <Select
          styles={customStyles}
          {...field}
          {...props}
          options={options}
          onBlur={updateBlur}
          onChange={handleOptionChange}
          onFocus={handleFocus}
          placeholder=""
          components={{
            IndicatorSeparator: () => null,
          }}
        />
      </Container>
      {touched && error ? <Error {...props}>{error}</Error> : null}
    </Wrapper>
  );
};

export default Dropdown;
