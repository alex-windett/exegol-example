import styled from "styled-components";
import { dim, mq } from "@rapharacing/tokens";

const Container = styled.div`
  margin-bottom: ${dim(4)};
  position: relative;
  width: auto;

  label {
    z-index: 1;
  }

  ${mq.md`
    margin-bottom: 0;
  `}
`;

const Wrapper = styled.div`
  position: relative;
`;

export { Container, Wrapper };
