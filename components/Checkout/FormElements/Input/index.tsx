import React, { FunctionComponent, useState } from "react";
import { useField } from "formik";

import Error from "../Error";
import Label from "../Label";
import { Container, StyledInput, Wrapper } from "./ui";

interface Props {
  id: string;
  label: string;
  name: string;
  placeholder: string;
  type: string;
  required?: boolean;
}

const Input: FunctionComponent<Props> = (props) => {
  const [field, { error, touched }] = useField(props);
  const [focussed, setFocussed] = useState(false);

  const handleFocus = () => {
    setFocussed(true);
  };

  function handleBlur(event: any) {
    field.onBlur(event);

    if (event.currentTarget.value) {
      setFocussed(true);
    } else {
      setFocussed(false);
    }
  }

  return (
    <Wrapper>
      <Container>
        <Label htmlFor={props.id} focussed={focussed}>
          {props.label} {!props.required ? <span>(optional)</span> : null}
        </Label>
        <StyledInput
          {...field}
          {...props}
          aria-required={props.required}
          aria-invalid={!!error}
          aria-describedby={props.id}
          onFocus={handleFocus}
          onBlur={handleBlur}
        />
      </Container>
      {touched && error ? <Error {...props}>{error}</Error> : null}
    </Wrapper>
  );
};

export default Input;
