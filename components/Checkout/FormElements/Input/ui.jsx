import styled from "styled-components";
import tokens, { dim, mq } from "@rapharacing/tokens";

const Container = styled.div`
  border: 1px solid ${tokens.color("grey15")};
  border-radius: 3px;
  min-height: ${dim(5)};
  position: relative;
  margin-bottom: ${dim(4)};

  ${mq.md`
    margin-bottom: 0;
  `}
`;

const StyledInput = styled.input`
  border: none;
  box-sizing: border-box;
  font-family: ${tokens.get("type.fontFamily.body")};
  font-size: 12px;
  font-weight: ${tokens.get("type.fontWeights.normal")};
  height: 100%;
  min-height: inherit;
  outline: 0;
  padding: 0 12px;
  width: 100%;
`;

const Wrapper = styled.div`
  display: block;
  position: relative;
`;

export { Container, StyledInput, Wrapper };
