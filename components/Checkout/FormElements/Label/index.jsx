import React from "react";
import styled from "styled-components";

import tokens, { dim } from "@rapharacing/tokens";

export const StyledLabel = styled.label`
  background-color: ${tokens.color("white")};
  color: ${tokens.color("grey40")};
  font-family: ${tokens.get("type.fontFamily.body")};
  font-size: 12px;
  font-weight: ${tokens.get("type.fontWeights.normal")};
  left: ${dim(1)};
  padding: 0 2px;
  letter-spacing: 0.4px;
  line-height: 14px;
  position: absolute;
  top: ${({ focussed }) => (focussed ? "-6px" : "50%")};
  transform: ${({ focussed }) => (focussed ? null : "translateY(-50%)")};
  transition: top 0.3s linear;
`;

const Label = (props) => {
  return (
    <StyledLabel {...props} focussed={props.focussed}>
      {props.children}
    </StyledLabel>
  );
};

export default Label;
