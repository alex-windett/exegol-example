import styled from "styled-components";
import { mq } from "@rapharacing/tokens";

export default styled.div`
	position: fixed;
	top: 0;
	left: 0;
  background-color: rgba(0, 0, 0, 0.25);
	height: 100%;
	opacity: 0;
	visibility: hidden;
  width: 100%;
  z-index: 0;

	${mq.md`
    &.overlay-entering {
      visibility: visible;
      opacity: 1;
      transition: opacity 0.2s ease-in-out, visibility 0.2s ease-in-out;
    }

    &.overlay-entered {
      visibility: visible;
      opacity: 1;
    }

    &.overlay-exiting {
      visibility: hidden;
      opacity: 0.01;
      transition: opacity 0.4s ease-in-out, visibility 0.4s ease-in-out;
    }

    &.overlay-exited {
      visibility: hidden;
      opacity: 0;
      transition: opacity 0.4s ease-in-out, visibility 0.4s ease-in-out;
    }
  `}
`;
