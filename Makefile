DIR := $(shell pwd)

default: 
	yarn
	yarn test

pre_push:
	yarn build
	# yarn test

post_merge:
	yarn

start_server:
	# yarn build
	yarn start`

	while ! grep -q "http://localhost:3000" nodeserver.log; do \
  	sleep .1
	done

	echo -e "server has started\n"

	exit 0

stop_server:
	lsof -ti:3000 | xargs kill