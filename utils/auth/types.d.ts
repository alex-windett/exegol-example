import { IAmplifyToken, IUser } from "@Types/index";

declare module "AuthTypes" {
  export interface IGetUser extends IGetToken {
    token?: IAmplifyToken;
    user?: IUser;
    success: Boolean;
  }

  export interface AuthLogin {
    token?: string;
    success: Boolean;
  }

  export interface HandleLogin {
    username: string;
    password: string;
  }

  export interface AuthLogout {
    success: Boolean;
    error?: string;
  }

  export interface IGetToken {
    token: string | undefined;
    user: any;
  }

  export interface IResetPassword {
    success: boolean;
    error?: any;
  }

  export interface IHandleConfirmResetPassword {
    username: string;
    code: string;
    passowrd: string;
  }

  export interface IConfirmResetPassword {
    success: boolean;
    error?: any;
  }
}
