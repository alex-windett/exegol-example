// /// <reference path="types.d.ts" />

// // @ts-ignore
// import * as AuthTypes from "AuthTypes";
import { useState, useEffect } from "react";
import type { AuthClass } from "@aws-amplify/auth/lib-esm/Auth";
import type { HubCapsule } from "@aws-amplify/core";
import { Hub, withSSRContext } from "aws-amplify";
import {
  GetServerSideProps,
  GetServerSidePropsContext,
  GetServerSidePropsResult,
} from "next";

export type AuthSSRContext = { Auth: AuthClass };
export const { Auth }: AuthSSRContext = withSSRContext();

// import Cookies from "js-cookie";
// import Router from "next/router";

// import { fromUnixTime } from "date-fns";

// import { Auth as Amplify, Cache } from "aws-amplify";

// const setTime = (minutes: number = 1): Date =>
//   new Date(new Date().getTime() + minutes * 60 * 1000);

type User = {
  email: string;
  sub: string;
};

export type AuthenticatedPageProps = {
  user: User;
};

type State = {
  loading: boolean;
  authenticated: boolean;
};

const initialState: State = {
  loading: true,
  authenticated: false,
};

export function redirectTo(
  { res }: Pick<GetServerSidePropsContext, "res">,
  path: string
) {
  if (res.headersSent) return;
  res.writeHead(302, { Location: path });
  res.end();
}

export async function getServerSideUser(ctx: GetServerSidePropsContext) {
  const { Auth }: AuthSSRContext = withSSRContext(ctx);
  try {
    const user = await Auth.currentSession();

    return user.getIdToken().decodePayload() as User;
  } catch (e) {
    return null;
  }
}

export function authenticatedServerSideProps<P = unknown>(
  fn: (
    ctx: GetServerSidePropsContext,
    user: User
  ) => Promise<GetServerSidePropsResult<P>>
) {
  const getServerSideProps: GetServerSideProps = async (ctx) => {
    const user = await getServerSideUser(ctx);
    if (!user) {
      redirectTo(ctx, "/login");
      return { props: {} };
    }

    return await fn(ctx, user);
  };

  return getServerSideProps;
}

export const authenticatedPage: GetServerSideProps = async (ctx) => {
  const user = await getServerSideUser(ctx);

  if (!user) {
    redirectTo(ctx, "/login");
    return { props: {} };
  }

  return { props: { user } };
};

export const notAuthenticatedPage: GetServerSideProps = async (ctx) => {
  const user = await getServerSideUser(ctx);
  if (!user) return { props: {} };

  redirectTo(ctx, "/");
  return { props: {} };
};

export function useIsAuthenticated() {
  const [state, setState] = useState(initialState);

  useEffect(() => {
    async function getAuthInfo() {
      try {
        await Auth.currentAuthenticatedUser();
        setState({ loading: false, authenticated: true });
      } catch (e) {
        setState({ loading: false, authenticated: false });
      }
    }

    getAuthInfo();
  }, []);

  useEffect(() => {
    function listener({ payload: { event } }: HubCapsule) {
      switch (event) {
        case "signIn": {
          setState({ loading: false, authenticated: true });
          break;
        }
        case "signOut": {
          setState({ loading: false, authenticated: false });
          break;
        }
      }
    }

    const cleanup = Hub.listen("auth", listener);
    return () => cleanup();
  }, []);

  return state;
}

// export default class Auth {
//   private LOCAL_STORAGE_TOKEN: string = "AUTH_TOKEN";
//   private LOCAL_STORAGE_REFRESH_TOKEN: string = "AUTH_REFRESH_TOKEN";
//   private LOCAL_STORAGE_USER_ATTR: string = "AUTH_USER_ATTR";
//   private LOCAL_STORAGE_EXPIRY: string = "AUTH_EXPIRY_TIME";
//   private CACHE_USER_ITEM: string = "USER";

//   private cookieOptions({
//     expires = setTime(60),
//     secure = true,
//   }): { expires: Date; secure: boolean } {
//     return { expires, secure };
//   }

//   constructor() {}

//   public getToken(
//     ctx: NextPageContext | false = false
//   ): AuthTypes.IGetToken | false {
//     const token =
//       ctx && ctx.req
//         ? // @ts-ignore
//           ctx.req.cookies[this.LOCAL_STORAGE_TOKEN]
//         : Cookies.get(this.LOCAL_STORAGE_TOKEN);
//     const user =
//       ctx && ctx.req
//         ? // @ts-ignore
//           ctx.req.cookies[this.LOCAL_STORAGE_USER_ATTR]
//         : Cookies.get(this.LOCAL_STORAGE_USER_ATTR);

//     if (!token && !user) return false;

//     return { token, user };
//   }

//   // get server side user
//   public async getServerSideUser(ctx?: GetServerSidePropsContext) {
//     const { Auth }: AuthSSRContext = withSSRContext(ctx);

//     try {
//       const user = await Auth.currentSession();
//       return user.getIdToken().decodePayload() as User;
//     } catch (e) {
//       return null;
//     }
//   }

//   public async isAuthenticated(ctx?: NextPageContext): Promise<boolean> {
//     return new Promise(async (resolve, reject) => {
//       try {
//         if (!!ctx) {
//           const tokens = this.getToken(ctx);

//           resolve(!!tokens);
//         } else {
//           const user = await this.getAuthenticatedUser();

//           return resolve(!!user);
//         }
//       } catch (error) {
//         // console.log(error);

//         reject(false);
//       }
//     });
//   }

//   public async getAuthenticatedUser(): Promise<object | boolean> {
//     return new Promise(async (resolve, reject) => {
//       try {
//         resolve(await Amplify.currentAuthenticatedUser());
//       } catch (error) {
//         console.info(error);
//         reject(false);
//       }
//     });
//   }

//   public async getUser(ctx?: NextPageContext): Promise<AuthTypes.IGetUser> {
//     return new Promise(async (resolve, reject) => {
//       try {
//         const auth: any = await this.getToken(ctx);

//         if (auth) {
//           const cookies = Object.keys(auth).reduce(
//             (acc: { [key: string]: string }, curr: string) => ({
//               ...acc,
//               [curr]: JSON.parse(auth[curr]),
//             }),
//             {}
//           );

//           return resolve({ ...cookies, success: true });
//         }

//         resolve({ success: false });
//       } catch (error) {
//         reject({
//           success: false,
//           error,
//         });
//       }
//     });
//   }

//   public async handleLogin({ username, password }: any) {
//     return new Promise(async (resolve, reject) => {
//       try {
//         const user = await Amplify.signIn(username, password);

//         Cache.setItem(this.CACHE_USER_ITEM, user, { priority: 3 });

//         console.log("user", user);

//         const expires = fromUnixTime(
//           user.signInUserSession.accessToken.payload.exp
//         );

//         const options = this.cookieOptions({ expires });

//         await Cookies.set(
//           this.LOCAL_STORAGE_TOKEN,
//           user.signInUserSession.accessToken.jwtToken,
//           options
//         );

//         await Cookies.set(
//           this.LOCAL_STORAGE_REFRESH_TOKEN,
//           user.signInUserSession.refreshToken.token,
//           options
//         );

//         await Cookies.set(
//           this.LOCAL_STORAGE_USER_ATTR,
//           JSON.stringify(user.attributes),
//           options
//         );

//         resolve({ ...user, success: true });
//       } catch (error) {
//         reject({
//           success: false,
//           error,
//         });
//       }
//     });
//   }

//   public async handleChangePassword({
//     oldPassword,
//     newPassword,
//   }: {
//     oldPassword: string;
//     newPassword: string;
//   }) {
//     return new Promise(async (resolve, reject) => {
//       try {
//         const user = await this.getAuthenticatedUser();

//         await Amplify.changePassword(user, oldPassword, newPassword);

//         await this.logout();

//         resolve({ success: true });
//       } catch (error) {
//         reject({ success: false, error });
//       }
//     });
//   }

//   public async handleChangePersonalDetails(attributes: {
//     [key: string]: string;
//   }) {
//     return new Promise(async (resolve, reject) => {
//       try {
//         const user = await this.getAuthenticatedUser();
//         await Amplify.updateUserAttributes(user, attributes);
//         resolve({ success: true });
//       } catch (error) {
//         reject({
//           success: false,
//           error,
//         });
//       }
//     });
//   }

//   public verifyToken(ctx: NextPageContext | false = false): Boolean {
//     const auth = this.getToken(ctx);

//     if (auth && auth.token) {
//       return !!auth.token;
//     }

//     return false;
//   }

//   public logout(): Promise<AuthTypes.AuthLogout> {
//     return new Promise(async (resolve, reject) => {
//       try {
//         [
//           this.LOCAL_STORAGE_TOKEN,
//           this.LOCAL_STORAGE_REFRESH_TOKEN,
//           this.LOCAL_STORAGE_USER_ATTR,
//           this.LOCAL_STORAGE_EXPIRY,
//         ].forEach((cookie) => Cookies.remove(cookie));

//         await Amplify.signOut();

//         setTimeout(() => {
//           Router.push("/");
//         }, 2000);

//         resolve({ success: true });
//       } catch (error) {
//         reject({
//           success: false,
//           error,
//         });
//       }
//     });
//   }

//   public resetPassword(username: string): Promise<AuthTypes.IResetPassword> {
//     return new Promise(async (resolve, reject) => {
//       try {
//         await Amplify.forgotPassword(username);

//         resolve({ success: true });
//       } catch (error) {
//         reject({ success: false, error });
//       }
//     });
//   }

//   public confirmResetPassword({
//     username,
//     code,
//     password,
//   }: AuthTypes.IHandleConfirmResetPassword): Promise<
//     AuthTypes.IConfirmResetPassword
//   > {
//     return new Promise(async (resolve, reject) => {
//       try {
//         await Amplify.forgotPasswordSubmit(username, code, password);

//         resolve({ success: true });
//       } catch (error) {
//         reject({ success: false, error });
//       }
//     });
//   }
// }
