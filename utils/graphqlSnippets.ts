const CAROUSEL_SLIDE_LIMIT: string = "15";
const STORY_BLOCK_LIMIT: string = "4";
const IMAGE_SET_LIMIT: string = "2";

export const Node = `
  __typename
  sys {
    id
  }
`;

export const imageQuery = `
    url
    fileName
    title
`;

export const linkQuery = `
    text
    url
`;

export const RaphaProductPrice = `
    formatted
    currencyISO
    value
`;

export const RaphaProduct = `
    code
    name
    summary
    url
    imageURI,
    price {
      ${RaphaProductPrice}
    }
`;

export const linkedStory = `
  slug
  heading 
  subHeading
  heroMedia {
    url
    title
  }
  subCategoryCollection(limit: 1) { 
    items {
      name
      slug
      parentCategory {
        name
        slug
      }
    }
  }
`;

export const storyImageQuery = `
  image {
    ${imageQuery}
  }
  width
  aspect
`;

export const ImagePullquote = `
  quote
  credit
  direction 
  link {
    ${linkQuery}
  }
  image {
    ${imageQuery}
  }
`;

export const Body = `
  text {
    json
  }
`;

export const PullQuote = `
    quote
    reference
`;

export const CrossHeadline = `
    head
    subhead
`;

export const ProductMannequin = `
  ... on ProductMannequin {
    sku
    description
    name
    hero {
      ${imageQuery}
    }
    link {
      ${linkQuery} 
    }
    product {
      ${RaphaProduct}
    }
  }
`;

export const StoryFeature = `  
  body {
    json
  }
  image {
    url
  }
  story {
    ${linkedStory}
  }
`;

export const StoryProfile = `
  body {
    json
  }
  story {
    ${linkedStory}
  }
`;

export const AnchorItem = `
  title
  linkUrl
  linkText
`;

export const Block = `
  ... on Block {
    blockItemsCollection(limit: ${STORY_BLOCK_LIMIT}) {
      items {
        __typename
       
        ... on StoryFeature {
          ${StoryFeature}
        }

        ... on StoryProfile {
          ${StoryProfile}
        }

        ... on AnchorItem {
          ${AnchorItem}
        }
      }
    }
  }
`;

export const Image = `
    ${storyImageQuery}
`;

export const ImageSet = `
  imagesCollection(limit: ${IMAGE_SET_LIMIT}) {
    items {
      ${storyImageQuery}
    }
  }
`;

export const VideoPlayer = `
  muted
  autoPlay
  video { 
    url
    title
  }
  posterImage {
    url
  }
`;

export const HybrisProduct = `
  ... on HybrisProduct {
    sku
    product {
      ${RaphaProduct}
    }
  }
`;

export const CarouselShowcase = `
  products
  visibleSlides
  slidesCollection(limit: ${CAROUSEL_SLIDE_LIMIT}) {
    items {
      __typename
      
      ... on Body {
        ${Body}
      }

      ... on Pullquote {
        ${PullQuote} 
      }

      ... on Image {
        ${Image}
      }

      ... on CrossHeadline {
        ${CrossHeadline}
      }

      ... on ImagePullquote {
        ${ImagePullquote}
      }

      ... on StoryFeature {
        ${StoryFeature}
      }
      
      ... on StoryProfile {
        ${StoryProfile}
      }
      
      ${ProductMannequin}

      ${HybrisProduct}
    }
  }
`;

export const KeyDetails = `
  hero {
    url
    title
  }
  itemsCollection(limit: 3) {
    items {
      title
      body {
        json
      }
    }
  }
`;

export const CarouselFullWidth = `
  slidesCollection(limit: ${CAROUSEL_SLIDE_LIMIT}) {
    items {
      __typename

      ... on Body {
          ${Body}
      }

      ... on Pullquote {
          ${PullQuote} 
      }
      
      ... on Image {
          ${Image}
      }

      ${HybrisProduct}
    }
  }
`;

export const CarouselFeatures = `
  slidesCollection(limit: ${CAROUSEL_SLIDE_LIMIT}) {
    items { 
      slideTitle
      slideSummary
      slideMedia {
        title
        url
      }
    }
  }
`;
