import Axios from "axios";

// @ts-ignore
export const createIssue = async ({ error, info }: any): Promise<any> => {
  const createBody = `**User:** user123 <br/><br/> \
    **Environment Info:** <br/>\
    Platform=${window.navigator.platform} <br/> \
    Online=${window.navigator.onLine} <br/> \
    Language=${window.navigator.language} <br/><br/> \
    **${error.toString()}** <br/><br/> \
    **Component error was detected in:** <br/> \
    ${JSON.stringify(info, null, 2)
      .replace(/\\r/g, "<br/>")
      .replace(/\\n/g, "<br/>")} <br/><br/>`;

  const params = {
    title: error.toString(),
    description: createBody,
    labels: ["bug"]
  };

  try {
    await Axios({
      url: "https://gitlab.com/api/v4/projects/12587284/issues",
      method: "post",
      params,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "PRIVATE-TOKEN": "r2qvT7hBdfLNPf9Te8Cq"
      }
    });
  } catch (e) {
    console.log(e);
  }
};
