import { NextWebVitalsMetric } from "next/app";

declare global {
  interface Window {
    gtag: any;
  }
}

export const report = (metric: NextWebVitalsMetric) => {
  const { name } = metric;

  switch (name) {
    case "Next.js-hydration":
      // handle hydration results
      break;
    case "Next.js-route-change-to-render":
      // handle route-change to render results
      break;
    case "Next.js-render":
      // handle render results
      break;
    default:
      sendEvent(metric);
      break;
  }
};

const sendEvent = ({ id, name, label, value }: NextWebVitalsMetric) => {
  // get rid of this once we implement gtm properly
  if (typeof window !== "undefined" && typeof window.gtag !== "undefined") {
    window.gtag("event", name, {
      event_category:
        label === "web-vital" ? "Web Vitals" : "Next.js custom metric",
      value: Math.round(name === "CLS" ? value * 1000 : value), // values must be integers
      event_label: id, // id unique to current page load
      non_interaction: true, // avoids affecting bounce rate.
    });
  } else {
    console.info(`WebVitalsMetric: ${name} - ${value}`);
  }
};

export default report;
