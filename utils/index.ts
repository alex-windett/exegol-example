import i18n from "i18next";

export { default as formatRichText } from "./formatRichText";

export const getCurrentURL = (path: string) => {
  const base = process.env.PUBLIC_URL;

  if (!path) return base;

  const href = path.split("");
  const [first, ...rest] = href;

  if (first !== "/") {
    const str = ["/", first, ...rest];
    return process.env.PUBLIC_URL + str.join("");
  }

  return base + path;
};

export const getCurrentLanguage = () => {
  return (
    i18n.language ||
    (typeof window !== "undefined" && window.localStorage.i18nextLng) ||
    "en"
  );
};

export const formatProductData = (data: any) => ({
  ...data,
  colors: !data.colors
    ? []
    : data.colors.map((color: any) => {
        const cmsItem = color.cmsComponents.items.find(
          (item: any) => item.sku === color.code
        );

        const mannequins =
          cmsItem && cmsItem.manequinImagesCollection
            ? cmsItem.manequinImagesCollection.items.map((x: any) => x)
            : [];

        return {
          ...color,
          ...cmsItem,
          mannequins,
        };
      }),
});

export const isVideo = (str: string | undefined) =>
  str && str.includes("video");
export const isImage = (str: string | undefined) =>
  str && str.includes("image");
