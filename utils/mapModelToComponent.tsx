/**
 * Map the module name from contentful to a directory.
 * Pass the module name into the function to retrive the identical
 * file name, or pass a specified file name
 */
export default ({ moduleName }: { moduleName: string }) => {
  const mappings: any = {
    Body: () => moduleName,
    // Highlight: () => moduleName,
    ImagePullquote: () => moduleName,
    CrossHeadline: () => moduleName,
    Pullquote: () => moduleName,
    Block: () => moduleName,
    StoryFeature: () => moduleName,
    ProductMannequin: () => moduleName,
    Image: () => moduleName,
    ImageSet: () => moduleName,
    HybrisProduct: () => moduleName,
    AnchorLink: () => moduleName,
    CarouselFeatures: () => moduleName,
    VideoPlayer: () => moduleName,
    ProductCollection: () => moduleName,
    KeyDetails: () => moduleName,

    // Conditional type in file as they are Profile + Feature are the same thing
    StoryProfile: () => "StoryFeature",

    // Carousels are bundled in one file and type is detemined from __typename in GraphQL
    CarouselShowcase: () => "Carousels",
    CarouselFullWidth: () => "Carousels"
  };

  const mapping =
    typeof mappings[moduleName] === "function"
      ? mappings[moduleName]()
      : mappings[moduleName];

  if (!mapping)
    return () => <h1>Model "{moduleName}" has not been intergrated yet</h1>;

  return mapping;
};
