import * as Yup from "yup";

interface IRequired {
  required?: boolean;
}

export const HEAD = {
  pageTitle: "pageTitle",
  ogTitle: "ogTitle",
  ogImage: "ogImage",
  ogURL: "ogURL",
};

export const RAPHA_DEFAULT =
  "https://images.ctfassets.net/1stkoghspd4h/UpwGgf1kXPb2PY0WPcPGg/073d8f8330307d4cf1e96968623eeca0/pdp-default-image.png";

// const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

export const FORM_VALIDATION = {
  EMAIL: ({ required = true }: IRequired): Yup.StringSchema<string> => {
    let str = Yup.string();

    if (required) {
      str = str.required("Email is required");
    }

    return str.email("Invalid Email");
  },
  PASSWORD: ({ required = true }: IRequired): Yup.StringSchema<string> => {
    let str = Yup.string();

    if (required) {
      str = str.required("Password is required");
    }

    return str
      .min(6, "Must be at least 6 characters long")
      .matches(/[a-z]/, "Must include one lower case letter")
      .matches(/[A-Z]/, "Must include one upper case letter")
      .matches(
        /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d])|(.*[\W]){1,})(?!.*\s).{6,}$/,
        "Must include one special character or number"
      );
  },
  // TELEPHONE: Yup.string().matches(phoneRegExp, "Phone number is not valid")
  TELEPHONE: Yup.string(),
};

export const TYPE_NAMES = {
  STORY: "Story",
  PRODUCT_CATEGORY_CONTENT: "ProductCategoryContent",
};

export const STOCK_VALUE = {
  IN_STOCK: "IN_STOCK",
  OUT_OF_STOCK: "OUT_OF_STOCK",
  LOW_STOCK: "LOW_STOCK",
  COMING_SOON_STOCK: "COMING_SOON_STOCK",
};

export const CONTENTFUL = {
  IMAGE: "Image",
  VIDEO: "VideoPlayer",
};
