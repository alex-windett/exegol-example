// https://npmjs.com/package/@contentful/rich-text-react-renderer
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { BLOCKS, MARKS } from "@contentful/rich-text-types";
import UI from "@rapharacing/rapha-ui";

const { H1, H2, H3, H4, H5, Label, Body } = UI.Typography;

const options = {
  renderMark: {
    [MARKS.BOLD]: (text: any) => `<strong>${text}<strong>`,
  },
  renderNode: {
    [BLOCKS.HEADING_1]: (node: any) =>
      node.content.map((n: any) => <H1>{n.value}</H1>),
    [BLOCKS.HEADING_2]: (node: any) =>
      node.content.map((n: any) => <H2>{n.value}</H2>),
    [BLOCKS.HEADING_3]: (node: any) =>
      node.content.map((n: any) => <H3>{n.value}</H3>),
    [BLOCKS.HEADING_4]: (node: any) =>
      node.content.map((n: any) => <H4>{n.value}</H4>),
    [BLOCKS.HEADING_5]: (node: any) =>
      node.content.map((n: any) => <H5>{n.value}</H5>),
    [BLOCKS.HEADING_6]: (node: any) =>
      node.content.map((n: any) => <Label variant="lrg">{n.value}</Label>),
    [BLOCKS.PARAGRAPH]: (node: any) =>
      node.content.map((n: any) => <Body variant="sml">{n.value}</Body>),
  },
};

interface Content {
  json: any;
}

const richText = (content: Content | false) => {
  if (!content || !content.json) return null;

  return documentToReactComponents(content.json, options);
};

export default richText;
