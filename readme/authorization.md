# Athorization

#### Authenticate User

- Method: `post`
- URL: `https://kre9cthsce.execute-api.eu-west-2.amazonaws.com/dev/login`
- body: `{ username, password }`

#### Fetch User

- Method: `get`
- URL: `https://kre9cthsce.execute-api.eu-west-2.amazonaws.com/dev/user`
- Headers: `{ Authaurization: Bearer [token] }`
