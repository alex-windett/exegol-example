# Meta Tags

## Next.js

Next.js has the ability to de-dube HTML meta tags that have the same key

See here:

[https://github.com/zeit/next.js/#populating-head](https://github.com/zeit/next.js/#populating-head)

## List of Current Meta Tags

| Name     | Description                       | Meta Key |
| -------- | --------------------------------- | -------- |
| Title    | Current Page Title as seen in tab | title    |
| OG Title | OG tag title                      | ogTitle  |
| OG Image | OG tag Image                      | ogImage  |
| OG URL   | OG tag URL                        | ogURL    |
