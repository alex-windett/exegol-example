# Contributing

The following is a set of guidelines for contributing to Rapha and its repositories and packages, which are hosted in the [Rapha Racing](https://gitlab.com/rapharacing) group on GitLab. These are mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this document in a pull request.

When contributing to any repository or package, please first discuss the change you wish to make and be sure you have a task, bug or spike to work against in Jira.

#### Table Of Contents

[Code of Conduct](#code-of-conduct)

[I don't want to read this whole thing, I just have a question!!!](#i-dont-want-to-read-this-whole-thing-i-just-have-a-question)

[What should I know before I get started?](#what-should-i-know-before-i-get-started)
  * [Rapha Racing Repositories and Packages](#rapha-racing-repositories-and-packages)

[How Can I Contribute?](#how-can-i-contribute)
  * [Reporting Bugs](#reporting-bugs)
  * [Suggesting Enhancements](#suggesting-enhancements)
  * [Pull Requests](#pull-requests)

[Styleguides](#styleguides)
  * [Git Commit Messages](#git-commit-messages)
  * [JavaScript Styleguide](#javascript-styleguide)

## Code of Conduct

This project and everyone participating in it is governed by the [Rapha Code of Conduct](CODE_OF_CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to [technology.team@rapha.cc](mailto:technology.team@rapha.cc).

## I don't want to read this whole thing I just have a question!!!

If you have not already, you can join the Rapha Racing Slack Workspace.

* [Join the Rapha Racing Slack Workspace](https://rapha.slack.com/)
    * Even though Slack is a chat service, sometimes it takes several hours for community members to respond &mdash; please be patient!
    * Use the `#team-technology` channel for general questions or discussion about anything Technology related
    * Use the `#team-front-end` channel for questions about Front End
    * There are many other channels available and most likely will align with a squad you are working in

## What should I know before I get started?

### Rapha Racing Repositories and Packages

Rapha is currently migrating to enable us to be more modular. We are currently trying to abstract reusable code into packages.

Here's a list of the most active repositories and packages:

* [rapha](https://github.com/rapharacing/rapha.cc) - E-Commerce website for [www.rapha.cc](https://www.rapha.cc)
* [rides-app](https://github.com/rapharacing/rides-app) - RCC Rides iOS App
* [rapha-android](https://github.com/rapharacing/rapha-android) - RCC Rides Android App
* [hybris-deployment](https://github.com/rapharacing/hybris-deployment) - Deployment project to deploy hybris using unix scripts for Rapha.
* [caravane](https://github.com/rapharacing/caravane) - Rapha rides and Events platform
* [graphql](https://github.com/rapharacing/rapha-graphql) - Rapha graphql server to parse plp data and stitch contentful apis
* [rapha-custom](https://github.com/rapharacing/rapha-custom) - Rapha custom frontend

## How Can I Contribute?

### Reporting Bugs

This section guides you through submitting a bug report. Following these guidelines helps maintainers and the developers understand your report :pencil:, reproduce the behavior :computer: :computer:, and find related reports :mag_right:.

Before creating bug reports, please check [Jira](https://raphatech.atlassian.net/secure/RapidBoard.jspa?rapidView=48&projectKey=RAPHACC&view=planning) as you might find out that you don't need to create one. When you are creating a bug report, please [include as many details as possible](#how-do-i-submit-a-good-bug-report). Fill out [the required template](), the information it asks for helps us resolve issues faster.

> **Note:** If you find a **Closed** Bug that seems like it is the same thing that you're experiencing, open a new Bug and include a link to the original Bug in the body of your new one.

#### Before Submitting A Bug Report

* **Perform a [cursory search](https://raphatech.atlassian.net/secure/RapidBoard.jspa?rapidView=48&projectKey=RAPHACC&view=planning)** to see if the problem has already been reported. If it has **and the Bug is still open**, add a comment to the existing Bug instead of opening a new one.

#### How Do I Submit A (Good) Bug Report?

Bugs are tracked in [Jira](https://www.atlassian.com/software/jira/bug-tracking). When you create an Bug on the correct repository provide as much information by following [the bug template]().

Explain the problem and include additional details to help developers reproduce the problem:

* **Use a clear and descriptive title** for the issue to identify the problem.
* **Describe the exact steps which reproduce the problem** in as many details as possible. When listing steps, **don't just say what you did, but explain how you did it**.
* **Provide specific examples to demonstrate the steps**. Include links to files or GitLab projects, or copy/pasteable snippets, which you use in those examples. If you're providing snippets in the issue, use [Markdown code blocks](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html).
* **Describe the behavior you observed after following the steps** and point out what exactly is the problem with that behavior.
* **Explain which behavior you expected to see instead and why.**
* **Include screenshots and screencasts** which show you following the described steps and clearly demonstrate the problem. You can use [this tool](https://www.cockos.com/licecap/) to record GIFs on macOS and Windows, and [this tool](https://github.com/colinkeenan/silentcast) or [this tool](https://github.com/GNOME/byzanz) on Linux.

Provide more context by answering these questions:

* **Can you reliably reproduce the issue?** If not, provide details about how often the problem happens and under which conditions it normally happens.

Include details about your configuration and environment:

* **What's the name and version of the OS you're using**?
* **Are you running in a virtual machine?** If so, which VM software are you using and which operating systems and versions are used for the host and the guest?

### Suggesting Enhancements

This section guides you through submitting an enhancement suggestion for Rapha, including completely new features and minor improvements to existing functionality. Following these guidelines helps develoers, product owners and business analysts understand your suggestion :pencil: and find related suggestions :mag_right:.

Before creating enhancement suggestions, please check [this list](#before-submitting-an-enhancement-suggestion) as you might find out that you don't need to create one. When you are creating an enhancement suggestion, please [include as many details as possible](#how-do-i-submit-a-good-enhancement-suggestion). Fill in [the template](), including the steps that you imagine you would take if the feature you're requesting existed.

#### Before Submitting An Enhancement Suggestion

* **Check the [backlog](https://raphatech.atlassian.net/secure/RapidBoard.jspa?rapidView=48&projectKey=RAPHACC&view=planning.nodetail)** to see if the enhancement has already been suggested. If it has, add a comment to the existing issue instead of opening a new one.

#### How Do I Submit A (Good) Enhancement Suggestion?

Enhancement suggestions are tracked as [Jira stories or tasks](https://www.atlassian.com/agile/project-management/user-stories). After you've determined [which project](https://raphatech.atlassian.net/projects) your enhancement suggestion is related to, create an story/task and provide the following information:

* **Use a clear and descriptive title** for the issue to identify the suggestion.
* **Provide a step-by-step description of the suggested enhancement** in as many details as possible.
* **Provide specific examples to demonstrate the steps**. Include copy/pasteable snippets which you use in those examples, as [Markdown code blocks](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html).
* **Describe the current behavior** and **explain which behavior you expected to see instead** and why.
* **Include screenshots and screencasts** which show you following the described steps and clearly demonstrate the problem. You can use [this tool](https://www.cockos.com/licecap/) to record GIFs on macOS and Windows, and [this tool](https://github.com/colinkeenan/silentcast) or [this tool](https://github.com/GNOME/byzanz) on Linux.
* **Explain why this enhancement would be useful** to users.
* **Specify the name and version of the OS you're using.**

### Pull Requests

The process described here has several goals:

- Maintain quality
- Fix problems that are important to users

Please follow these steps to have your contribution considered by the maintainers:

1. Follow the [styleguides](#styleguides)
2. After you submit your pull request, verify that all [status checks](https://help.github.com/articles/about-status-checks/) are passing <details><summary>What if the status checks are failing?</summary>If a status check is failing, and you believe that the failure is unrelated to your change, please leave a comment on the pull request explaining why you believe the failure is unrelated. A maintainer will re-run the status check for you. If we conclude that the failure was a false positive, then we will open an issue to track that problem with our status check suite.</details>

While the prerequisites above must be satisfied prior to having your pull request reviewed, the reviewer(s) may ask you to complete additional design work, tests, or other changes before your pull request can be ultimately accepted.

## Styleguides

### Git Commit Messages

Mix and match of the best git commit messaging standards.
- [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0-beta.4/#summary)
- [Commit message guidelines](https://gist.github.com/robertpainsi/b632364184e70900af4ab688decf6f53)

1. The summary line will be written in the imperative form
   - 'fix bug' not 'fixed bug' 
   - 'add feature' not 'added feature'
2. Ideally the summary line should be 50 chars or less 
3. All subsequent lines in the commit message are 72 characters or less
4. First line of the commit is a brief summary 
5. The body will contain a more detailed explanation of the change.
   **A blank line MUST seperate the summary from the body**  
6. If a commit introduces a breaking change then the body will be
   prefaced by `BREAKING CHANGE:`
   - Can be used with **ANY** type of commit  
7. The JIRA ticket id will be added as a new line with prefix
   `ticket_id`
8. The summary will take the following form:-

#### Format

```
<type> (scope) (!) : <description>

type = ['fix','feat','chore','style','refactor','improvement']
scope = what part of the codebase was changed
```

#### Example

```
feat(raphafacades): add single purchase of product

BREAKING CHANGE: Add new `singlePurchaseProduct` field, add new solr 
index and ensure product can only be purchased once.

ticket_id: RAPHACC-007
```

Please refer to our [commit message template](COMMIT_MESSAGE.md)

### JavaScript Styleguide

All JavaScript must adhere to [JavaScript Standard Style](https://standardjs.com/).

* Prefer the object spread operator (`{...anotherObj}`) to `Object.assign()`
* Inline `export`s with expressions whenever possible
  ```js
  // Use this:
  export default class ClassName {

  }

  // Instead of:
  class ClassName {

  }
  export default ClassName
  ```
* Place requires in the following order:
    * Built in Node Modules (such as `path`)
    * Built in Rapha Modules (such as `tokens`, `request`)
    * Local Modules (using relative paths)
* Place class properties in the following order:
    * Class methods and properties (methods starting with `static`)
    * Instance methods and properties