require("dotenv").config();

const path = require("path");
const fse = require("fs-extra");
const fs = require("fs");
const axios = require("axios");
const reduce = require("lodash/reduce");
const merge = require("lodash/merge");
const map = require("lodash/map");
const AWS = require("aws-sdk");
const tmp = require("tmp-promise");
const os = require("os");

const BUILD_DIR = path.join(__dirname, "../public/static/locales");

const { AIRTABLE_KEY, AIRTABLE_SHEET_ID } = process.env;

const config = {
  AIRTABLE_API_KEY: AIRTABLE_KEY,
  TRANSLATIONS_SHEET_BASE: `https://api.airtable.com/v0/${AIRTABLE_SHEET_ID}/`, // Make sure there is a trailing "/"
  AWS_API_KEY: "AKIA6DQ2NXJRAIZKB3GJ",
  AWS_API_SECRET: "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",
  S3_BUCKET_NAME: "rapha-translations-storage",
};

// Each Page of the Airtable Document
const tableUrls = ["Common", "Home", "Error"];

const localeToFileName = (locale) => {
  // const split = locale.split("_");

  // const [prefix, postfix] = split;

  // return postfix
  //   ? `${prefix.toLowerCase()}_${postfix.toUpperCase()}`
  //   : prefix.toLowerCase();

  return locale;
};

/*
  This function goes to airtable and fetches all of the translations, as the max records airtable can return in a single call is 100 we use a recursive function to call it with the offset key if it exists.
*/
const fetchTranslations = async ({ offset = "", url = "" }) => {
  try {
    const response = await axios.get(url, {
      params: {
        view: "Grid view",
        offset,
      },
      headers: {
        Authorization: `Bearer ${config.AIRTABLE_API_KEY}`,
      },
    });

    let records = response.data.records;

    if (response.data.offset) {
      const moreRecords = await fetchTranslations({
        offset: response.data.offset,
        url: response.config.url,
      });
      records = [...records, ...moreRecords];
    }

    return records;
  } catch (e) {
    console.log(e);
  }
};

/*
  This takes the data from the API response and maps over it, first we grab the Key value as this is the translations key. We then rduce the fields object which contains the following shape data
  {
    Key: 'translation_key',
    EN: 'English translation',
    ES: 'Spanish Translation',
    ... etc
  }
  We ignore the Key iteration as we dont need it, and then create an object that contains the locale key followed by and object that contains the translation key and the locale value:
  {
    EN: {
      translation_key: 'English translation'
    },
    ES: {
      translation_key: 'Spanish Translation'
    }
  },
  {
    EN: {
      another_translation_key: 'English translation'
    },
    ES: {
      another_translation_key: 'Spanish Translation'
    }
  }

  This does the same for each row in the airtable table.
*/
const createLocaleObjects = (data) => {
  return data.map((d) => {
    const { Key: keyValue } = d.fields;
    return reduce(
      d.fields,
      (result, value, key) => {
        if (key === "Key") {
          return result;
        }
        result[key] = { [keyValue]: value };
        return result;
      },
      {}
    );
  });
};

/*
  Next we need to simple merge all the objects into one massive object that contains the locale key followed by the object full of keys like so
  {
    EN: {
      translation_key: 'English translation',
      another_translation_key: 'English translation'
    },
    ES: {
      translation_key: 'Spanish translation',
      another_translation_key: 'Spanish translation'
    }
  }
*/
const mergeAllLocales = (data) =>
  data.reduce((acc, curr) => merge(acc, curr), {});

const createAdditionalLocales = (data) => {
  return {
    ...data,
    "en-AU": data["EN-GB"],
    "en-US": data["EN-GB"],
  };
};

// The final step is to map over each locale and use the locale key as the filename and then write the object to the correct file
const writeLocalesToFiles = (data) => {
  map(data, (value, key) => {
    const fileName = `${BUILD_DIR}/${localeToFileName(key)}/common.json`;

    fse
      .outputJson(fileName, value, { spaces: 2 })
      .then(() => {
        console.log(`File: ${fileName} has been saved!`);
      })
      .catch((e) => console.log(e));
  });
};

const writeToS3 = async (data) => {
  const S3 = new AWS.S3({
    accessKeyId: config.AWS_API_KEY,
    secretAccessKey: config.AWS_API_SECRET,
  });

  await map(data, async (values, key) => {
    // Key = local id e.g EN, FR,
    // Values = translations strings
    tmp.file({ dir: os.tmpdir(), postfix: ".json" }).then((o) => {
      fse
        .outputJson(o.path, values, { spaces: 2 })
        .then(async () => {
          const fileContent = fs.readFileSync(o.path);

          const params = {
            Bucket: config.S3_BUCKET_NAME,
            Key: `${localeToFileName(key)}/common.json`,
            Body: fileContent,
            ACL: "public-read",
          };

          S3.upload(params, (err, data) => {
            if (err) {
              throw err;
            }
            console.log(`File uploaded successfully to ${data.Location}`);
          });
        })
        .catch((err) => {
          console.log(`Error writing to temp file for ${key} locale`);
          throw err;
        });
    });
  });
};

const createTranslationsArray = async () => {
  try {
    const tables = tableUrls.map(async (table) => {
      const encodedTableName = encodeURIComponent(table);
      return await fetchTranslations({
        url: config.TRANSLATIONS_SHEET_BASE + encodedTableName,
      });
    });

    const res = await Promise.all(tables);
    const concatArray = [];

    return concatArray.concat(...res);
  } catch (e) {
    console.log(e);
  }
};

// This runs each of the steps above
const runScript = async () => {
  if (!AIRTABLE_KEY || !AIRTABLE_SHEET_ID) {
    throw Error("Missing variable, one of AIRTABLE_KEY or AIRTABLE_SHEET_ID");
  }

  const translations = await createTranslationsArray();
  const localeObjects = createLocaleObjects(translations);
  const mergedLocales = mergeAllLocales(localeObjects);
  const additionalLocales = createAdditionalLocales(mergedLocales);
  // const writeToFiles = NODE_ENV === "production" ? writeToS3 : writeLocalesToFiles;
  // writeToFiles(additionalLocales)
  writeLocalesToFiles(additionalLocales);
};

runScript();
