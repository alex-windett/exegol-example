import React, { FunctionComponent } from "react";
import Head from "next/head";
import styled from "styled-components";
import ui from "@rapharacing/rapha-ui";
import tokens, { dim } from "@rapharacing/tokens";

import UpdateEmail from "@Containers/Forms/Account/UpdateEmail";
import UpdatePassword from "@Containers/Forms/Account/update-password";
import UpdatePersonalDetails from "@Containers/Forms/Account/update-personal-details";

import Layout from "@Containers/PageLayout";

const { Typography, Grid } = ui;

import { authenticatedServerSideProps } from "@Utils/auth";

const Header = styled.div`
  text-align: center;
  border-bottom: 1px solid ${tokens.color("grey15")};
  margin-bottom: ${dim(4)};
`;

const Block = styled.div`
  padding: ${dim(2)} 0;
`;

const AccountDetails: FunctionComponent<any> = ({ user = {} }) => {
  if (!user) {
    return (
      <Layout>
        <h2>Something went wrong</h2>
      </Layout>
    );
  }

  return (
    <Layout>
      <Head>
        <title key="title">Edit My Details | Rapha</title>
      </Head>

      <Header>
        <Typography.H1>Edit my Details</Typography.H1>
        <Typography.Body variant="sml">
          Use this form to update your personal information and manage your
          Rapha Cycling Club membership.
        </Typography.Body>
      </Header>

      <Grid columns={{ sm: 1, md: 1, lg: 4 }}>
        <Typography.VerticalHeading>
          <Typography.Label>Personal Information</Typography.Label>
        </Typography.VerticalHeading>

        <Block>
          <UpdatePersonalDetails user={user} />
        </Block>
        <Block>
          <UpdateEmail user={user} />
        </Block>
        <Block>
          <UpdatePassword />
        </Block>
      </Grid>
    </Layout>
  );
};

export const getServerSideProps = authenticatedServerSideProps<any>(
  // @ts-ignore
  async (ctx, user) => {
    return {
      props: { user },
    };
  }
);

export default AccountDetails;
