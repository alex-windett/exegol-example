import React, { Fragment, FunctionComponent } from "react";
import { compose } from "recompose";
import Head from "next/head";
import withGraphql from "@Hocs/withApollo";
import { allStoriesQuery } from "@Queries/stories";
import Layout from "@Containers/PageLayout";
import UI from "@rapharacing/rapha-ui";
import { StyledLabel, StyledBody, Anchors } from "@Components/Account/ui";
import { authenticatedServerSideProps } from "@Utils/auth";
import { HEAD } from "@Utils/constants";

const { Account, Typography } = UI;
interface Props {
  authenticated: Boolean;
  user: any;
}

// const { Auth } = withSSRContext();

const AccountPage: FunctionComponent<Props> = ({
  authenticated,
  user = {},
}) => {
  // const initialState = {
  //   loading: true,
  //   authenticated: false,
  // };

  // const [state, setState] = useState(initialState);

  // useEffect(() => {
  //   async function getAuthInfo() {
  //     try {
  //       await Auth.currentAuthenticatedUser();
  //       setState({ loading: false, authenticated: true });
  //     } catch (e) {
  //       setState({ loading: false, authenticated: false });
  //     }
  //   }

  //   getAuthInfo();
  // }, []);

  if (!authenticated) {
    return (
      <Layout>
        <h2>Something went wrong</h2>
      </Layout>
    );
  }

  return (
    <Layout>
      <Head>
        <title key={HEAD.ogTitle}>My Account | Rapha</title>
      </Head>

      <h1>My Account</h1>

      <Account.Profile name={user.given_name} isMember={false}>
        <Fragment>
          <StyledLabel variant="md">Join The Rapha Cycling Club</StyledLabel>
          <StyledBody variant="xSml">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Atque,
            laudantium. Doloribus, numquam.
          </StyledBody>
          <Anchors>
            <Typography.Anchor href="/">Learn more</Typography.Anchor>
            <Typography.Anchor href="/">Activate membership</Typography.Anchor>
          </Anchors>
        </Fragment>
      </Account.Profile>

      <p>
        {user.given_name} {user.family_name}
      </p>

      <p>{user.email}</p>
    </Layout>
  );
};

const enhance = compose(withGraphql({ query: allStoriesQuery }));

// @ts-ignore
const EnhacedPage = enhance(AccountPage);

const Page = (props: any) => <EnhacedPage {...props} />;

export const getServerSideProps = authenticatedServerSideProps<Props>(
  // @ts-ignore
  async (ctx, user) => {
    return {
      props: { authenticated: true, user },
    };
  }
);

export default Page;
