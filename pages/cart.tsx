import React, { useState } from "react";
import Head from "next/head";
import { dim, mq } from "@rapharacing/tokens";
import { compose } from "recompose";
import styled from "styled-components";

import Layout from "@Containers/PageLayout";
import BasketContent from "@Components/Basket";
import BasketSummary from "@Components/Basket/Summary";
import withApollo from "@Hocs/withApollo";
import { IBasketItem } from "@Types/basketWishlist";

import query from "@Queries/cart";

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  margin-top: ${dim(6)};
  margin-bottom: ${dim(2)};
  padding: 0 ${dim(2)};

  ${mq.md`
    padding: 0 0 ${dim(7)};
  `}
`;

const ContentColumn = styled.div`
  grid-column: span 12;

  ${mq.md`
    grid-column: 2 / span 6;
  `}
`;

const AsideColumn = styled.div`
  grid-column: span 12;

  ${mq.md`
    grid-column: 9 / span 3;
  `}
`;

const Cart = ({ data }: any) => {
  const basket = data.basket || {};
  const wishlist = data.wishlist || {};
  const [wishlistActive, setwishlistActive] = useState(false);

  const [products, setProducts] = useState<IBasketItem[] | undefined>(
    basket.items
  );

  const hanldeWishlistActive = async () => {
    await setProducts(wishlist && wishlist.items);
    setwishlistActive(true);
  };

  const handleBasketActive = async () => {
    await setProducts(basket && basket.items);
    setwishlistActive(false);
  };

  return (
    <Layout>
      <Head>
        <title key="title">Your shopping cart | Rapha</title>
      </Head>

      <Wrapper>
        <ContentColumn>
          <BasketContent
            loading={data.loading}
            products={products}
            wishlistActive={wishlistActive}
            basketCount={basket.count}
            wishlistCount={wishlist.count}
            hanldeWishlistActive={hanldeWishlistActive}
            handleBasketActive={handleBasketActive}
            allowMoveToBasket={wishlistActive}
            allowRremoveFromWishlist={wishlistActive}
            allowMoveToWishlist={!wishlistActive}
            allowRremoveFromBasket={!wishlistActive}
          />
        </ContentColumn>

        <AsideColumn>
          <BasketSummary wishlistActive={wishlistActive} />
        </AsideColumn>
      </Wrapper>
    </Layout>
  );
};

const enhance = compose(
  withApollo({
    query,
  })
);

export default enhance(Cart);
