import React from "react";
import Layout from "@Containers/PageLayout";
import Head from "next/head";
import compose from "recompose/compose";
import { SingletonRouter, withRouter } from "next/router";
import withApollo from "@Hocs/withApollo";
import gql from "fake-tag";

const enhance = compose(
  withRouter,
  withApollo(({ router }: { router: SingletonRouter }) => {
    return {
      variables: {
        slug: router.query.slug,
      },
      query: gql`
        query getClubhouse($slug: String) {
          allOfTheClubhouses: clubhouseCollection {
            items {
              slug
              name
            }
          }

          theCurrentClubhouse: clubhouseCollection(where: { slug: $slug }) {
            items {
              slug
              name
              address
              location {
                lat
                lon
              }
            }
          }
        }
      `,
    };
  })
);

const Clubhouse = ({ data }: any) => {
  // Current clubhouse details
  const [clubhouse] = data.theCurrentClubhouse.items;

  return (
    <Layout>
      <Head>
        <title key="title">Clubhouses</title>
      </Head>

      {JSON.stringify(data)}

      <h1>{clubhouse.name}</h1>
    </Layout>
  );
};

const EnhancedClubhouse = enhance(Clubhouse);

export default EnhancedClubhouse;
