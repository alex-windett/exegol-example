import React, { useState, useRef } from "react";
import Head from "next/head";
import compose from "recompose/compose";
import styled from "styled-components";
import { mq, dim } from "@rapharacing/tokens";
import UI from "@rapharacing/rapha-ui";

import withApollo from "@Hocs/withApollo";
import { HEAD } from "@Utils/constants";
import Page from "@Components/Page";
import Layout from "@Containers/PageLayout";
import ClubhouseModal from "@Components/Clubhouses";
import { useOutsideClick, useScrollLock } from "@Utils/hooks";
import query from "@Queries/clubhouses";

const {
  HeadlineSubHeading,
  CloudinaryImage,
  Description,
  Carousel,
  Button,
  Modal,
  ModalProvider,
} = UI;

const { FullWidthCarousel } = Carousel;

const ButtonWrap = styled.div`
  text-align: center;
  padding-bottom: ${dim(5)};
`;

const Background = styled.span`
  width: 100vw;
  height: 100vh;
  background-color: rgba(0, 0, 0, 0.3);
  position: fixed;
  z-index: 5;
  top: 0;
  left: 0;
`;

const ModalCard = styled(Modal)`
  height: 100%;
  width: 100%;
  text-align: left;

  ${mq.md`
    height: unset;
  `}
`;

const enhance = compose<IProps, {}>(withApollo(() => ({ query })));

interface IProps {
  data: {
    clubhouseRegionCollection: any;
    clubhouseRegion: {
      name: string;
    };
    clubhouses: {
      title: string;
      description: string;
      heroImage: {
        url: string;
        title: string;
      };
      blurbTitle: string;
      blurbContent: string;
      carousel: {
        slidesCollection: {
          items: any[];
        };
      };
    };
  };
}

const Clubhouses = ({ data }: IProps) => {
  const [isNavOpen, setIsNavOpen] = useState(false);

  const ref = useRef(null);

  useScrollLock(isNavOpen);
  useOutsideClick(ref, () => setIsNavOpen(false));

  const toggleNav = () => setIsNavOpen((prev) => !prev);

  const { clubhouses } = data;

  return (
    <Layout>
      <ModalProvider>
        <Head>
          <title key={HEAD.pageTitle}>{`Clubhouses | Rapha`}</title>
        </Head>

        {isNavOpen ? (
          <>
            <Background />

            <ModalCard onClose={toggleNav}>
              <div ref={ref}>
                <ClubhouseModal
                  clubhouseRegionCollection={data.clubhouseRegionCollection}
                />
              </div>
            </ModalCard>
          </>
        ) : null}

        <Page>
          {clubhouses && (
            <HeadlineSubHeading
              heading={clubhouses.title}
              subHeading={clubhouses.description}
            />
          )}

          {data.clubhouseRegionCollection && (
            <ButtonWrap>
              <Button onClick={toggleNav}>Find a clubhouse</Button>
            </ButtonWrap>
          )}

          {clubhouses && (
            <>
              <CloudinaryImage
                src={clubhouses.heroImage.url}
                alt={clubhouses.heroImage.title}
              />
              <Description
                title={clubhouses.blurbTitle}
                text={clubhouses.blurbContent}
              />
              <FullWidthCarousel>
                {clubhouses.carousel &&
                  clubhouses.carousel.slidesCollection.items.map(
                    (s: any, i: number) => (
                      <CloudinaryImage
                        src={s.image.url}
                        alt={s.image.title}
                        key={i}
                      />
                    )
                  )}
              </FullWidthCarousel>
            </>
          )}
        </Page>
      </ModalProvider>
    </Layout>
  );
};

export default enhance(Clubhouses);
