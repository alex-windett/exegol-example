import { NextApiRequest, NextApiResponse } from "next";
import Amplify, { withSSRContext } from "aws-amplify";
import axios from "axios";
import config from "../../config";

// Amplify SSR configuration needs to be enabled within each API route
Amplify.configure({ config, ssr: true });

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { Auth } = withSSRContext({ req });

  try {
    const session = await Auth.currentSession();

    const {
      accessToken: { jwtToken },
    } = session;

    let config = {
      headers: {
        Authorization: jwtToken,
      },
    };

    await axios.post(
      "https://njw30l59ta.execute-api.eu-west-1.amazonaws.com/dev/preemailchange",
      { email: req.query.new_email },
      config
    );

    res.statusCode = 200;
    res.json({ success: true });
  } catch (err) {
    res.statusCode = 400;
    res.json({ error: err.data });
  }
};
