import axios from "axios";

const { TEST_CART_ID, TEST_USER_ID, TEST_ACCESS_TOKEN } = process.env;

export default async (req, res) => {
  try {
    const cartId = TEST_CART_ID;
    const userId = TEST_USER_ID;
    const url = `https://api.c616d-rapharaci1-d1-public.model-t.cc.commerce.ondemand.com/rest/v2/rapha/users/${userId}/carts/${cartId}/addresses/delivery`;
    const config = {
      headers: {
        Authorization: `Bearer ${TEST_ACCESS_TOKEN}`,
      },
    };

    const { country, region, ...rest } = req.query;

    const data = Object.assign(
      {
        country: {
          isocode: req.query.country,
        },
        region: {
          isocode: req.query.region,
        },
      },
      rest
    );

    await axios.post(url, data, config);

    res.statusCode = 200;
    res.json({ success: true });
  } catch (err) {
    res.statusCode = 400;
    res.json({ error: err.data });
  }
};
