import Amplify, { withSSRContext } from "aws-amplify";
import axios from "axios";
import config from "../../config";

// Amplify SSR configuration needs to be enabled within each API route
Amplify.configure({ config, ssr: true });

export default async (req, res) => {
  const { Auth } = withSSRContext({ req });

  try {
    const session = await Auth.currentSession();

    const {
      accessToken: { jwtToken },
    } = session;

    let config = {
      headers: {
        Authorization: jwtToken,
      },
    };

    await axios.get(
      `https://d2lp9vn2jg.execute-api.eu-west-1.amazonaws.com/dev/customer/foo`,
      config
    );

    res.json({ user: data });
  } catch (err) {
    res.statusCode = 400;
    res.json({ user: null });
  }
};
