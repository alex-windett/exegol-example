import React from "react";
import Head from "next/head";
import Router from "next/router";

import { LoginForm } from "@Containers/Forms";
import Layout from "@Containers/PageLayout";

import UI from "@rapharacing/rapha-ui";
const { Button } = UI;

import {
  Border,
  LoginGrid,
  RegisterGrid,
  Section,
  SubTitle,
  Title,
} from "@Components/Auth/ui";

const Login = () => {
  return (
    <Layout>
      <Head>
        <title key="title">Rapha | Login</title>
      </Head>

      <Title>Sign in to your account</Title>

      <Section>
        <LoginGrid>
          <LoginForm />
        </LoginGrid>

        <Border />

        <RegisterGrid>
          <SubTitle>New Customers</SubTitle>

          <Button variant="secondary">Sign Up</Button>
        </RegisterGrid>
      </Section>
    </Layout>
  );
};

Login.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default Login;
