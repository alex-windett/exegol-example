// /product/:slug/:sku
import Head from "next/head";
import { useState, useEffect, useRef } from "react";
import Router from "next/router";
import { withRouter, SingletonRouter } from "next/router";
import styled from "styled-components";
import withApollo from "@Hocs/withApollo";
import { compose } from "recompose";
import ErrorComponent from "@Components/PageError";
import { withTranslation } from "@WithTranslations";
import query from "@Queries/product";
import Layout from "@Containers/PageLayout";
import { Showcase } from "@Components/Product";
import VizSensor from "react-visibility-sensor";
import graphqlClient from "@Utils/graphqlClient";
import { gql } from "@apollo/client";

import {
  EcommerceBar,
  Details,
  MannequinCarousel,
  ProductGuarantees,
  Breadcrumbs,
  CMSEntries,
} from "@Components/Product";
import Page from "@Components/Page";
import UI from "@rapharacing/rapha-ui";
import { mq } from "@rapharacing/tokens";
import { HEAD } from "@Utils/constants";
import { IProduct, IProductSize } from "@Types/product";
import withAppContext from "@Hocs/withContext";
import { AppContext } from "@Types/index.d.ts";

const { ProductIntro, RatingAndQA } = UI;

const PDPWrapperGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(12, 1fr);
`;

const PDPWrapperColumn = styled.div`
  grid-column: 1 / -1;

  ${mq.md`
    grid-column: 3 / span 8;
  `}
`;

const enhance = compose(
  withTranslation("common"),
  withRouter,
  withAppContext,
  withApollo(({ router }: { router: SingletonRouter }) => {
    return {
      query,
      variables: {
        sku: router.query.sku,
      },
    };
  })
);

interface IProps {
  data: {
    product: IProduct;
  };
  t: any;
  router: SingletonRouter;
  appContext: AppContext;
}

const Product = (props: IProps) => {
  const [ecomBarVisible, setecomBarVisible] = useState(true);
  const detailsRef = useRef(null);

  if (!props.data || !props.data.product) {
    return <ErrorComponent statusCode={404} t={props.t} />;
  }

  const { product } = props.data;

  if (!product) {
    alert("No product found");
    return null;
  }

  if (props.router.query.name !== product.sanitizedName) {
    useEffect(() => {
      Router.push(
        "/product/[sku]/[slug]",
        `/product/${props.router.query.sku}/${product.sanitizedName}`,
        { shallow: true }
      );
    }, []);
  }

  const [cms] = product.cmsComponents.items;

  const [activeColor, setActiveColor] = useState(
    product.colors && product.colors[0]
  );
  const [activeSize, setActiveSize] = useState<false | IProductSize>(false);

  const [activeColorCms] = activeColor.cmsComponents.items;

  const handleAddToBasket = async () => {
    const { appContext } = props;

    if (activeSize) {
      const { data } = await graphqlClient.mutate({
        variables: {
          sku: activeSize.code,
          quantiy: 1,
          basketID: appContext.basketID,
        },
        mutation: gql`
          mutation addToBasket(
            $sku: String!
            $quantity: Int
            $basketID: String
          ) {
            addToBasket(sku: $sku, quantity: $quantity, basketID: $basketID) {
              basketID
              code
              quantity
              baseProduct
              stock
            }
          }
        `,
      });

      if (!appContext.basketID) {
        const id = data.addToBasket.basketID;
        appContext.setBasketID(id);
      }

      appContext.setMiniBasketOpen(true);

      return;
    }

    if (window && detailsRef.current) {
      window.scrollTo({
        // @ts-ignore
        top: detailsRef.current.offsetTop,
        left: 0,
        behavior: "smooth",
      });
    }
  };

  const handleNotifyMe = () => {
    // @ts-ignore
    !!activeSize && alert(`Notify Me  - ${activeSize.code}`);
  };

  const handleAddWishlist = () => {
    // @ts-ignore
    !!activeSize && alert(`Add Wishlist  - ${activeSize.code}`);
  };

  const handleColorChange = async (index: number) => {
    await setActiveColor(product.colors[index]);
  };

  const handleSizechange = async (e: any) => {
    const code = e.target.value;

    const size = activeColor.sizes.find(
      (size: IProductSize) => size.code === code
    );

    // @ts-ignore
    await setActiveSize(size);
  };

  const ProductBreadcrumbs = () =>
    product.breadcrumbs && (
      <Breadcrumbs breadcrumbs={product.breadcrumbs} name={product.name} />
    );

  return (
    <Layout>
      <Head>
        <title key={HEAD.pageTitle}>
          {product.name} | {cms && cms.summary} | Rapha
        </title>
      </Head>

      <EcommerceBar
        isVisible={ecomBarVisible}
        product={product}
        activeColor={activeColor}
        activeSize={activeSize}
        handleAddToBasket={handleAddToBasket}
        handleNotifyMe={handleNotifyMe}
      />

      <Page>
        {cms && cms.showcaseCollection.items.length > 0 && (
          <Showcase cms={cms} />
        )}

        <ProductBreadcrumbs />

        <PDPWrapperGrid>
          <PDPWrapperColumn>
            <ProductIntro
              header={product.name}
              main={cms.summary}
              rating={product.reviews.Results[0].Rating}
              anchor=""
              href=""
            />

            <MannequinCarousel
              activeColorCms={activeColorCms}
              product={product}
              activeColor={activeColor}
            />

            <VizSensor onChange={(isVisible) => setecomBarVisible(!isVisible)}>
              <Details
                forwardRef={detailsRef}
                product={product}
                activeColor={activeColor}
                handleColorChange={handleColorChange}
                activeSize={activeSize}
                handleSizeChange={handleSizechange}
                handleNotifyMe={handleNotifyMe}
                handleAddToBasket={handleAddToBasket}
                handleAddWishlist={handleAddWishlist}
              />
            </VizSensor>
          </PDPWrapperColumn>
        </PDPWrapperGrid>

        {!!cms && <CMSEntries cms={cms} />}

        <RatingAndQA
          scoreTitle={product.reviews.Results[0].Rating}
          rNumbers={product.reviews.TotalResults}
          rButton=""
          qaTitle=""
          qaNumbers={product.reviews.TotalResults}
          qaButton=""
          rating={product.reviews.Results[0].Rating}
        />

        <ProductGuarantees />

        <ProductBreadcrumbs />
      </Page>
    </Layout>
  );
};

// @ts-ignore
export default enhance(Product);
