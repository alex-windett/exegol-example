import { NextPageContext } from "next";
import Axios from "axios";
import gql from "fake-tag";
import Router from "next/router";
import ErrorComponent from "@Components/PageError";

interface Error {
  error: boolean;
  statusCode: number;
}

const error: Error = {
  error: true,
  statusCode: 404
};

// Error component will only render if server side request doesnt return a product
const Product = (props: Error | {}) => <ErrorComponent {...props} />;

Product.getInitialProps = async ({
  res,
  query
}: NextPageContext): Promise<Error | {}> => {
  try {
    const { data } = await Axios({
      method: "post",
      url: process.env.GRAPHQL_URL,
      data: {
        query: gql`
          query getProduct($sku: String) {
            product(sku: $sku) {
              name
              code
              sanitizedName
            }
          }
        `,
        variables: {
          sku: query.sku
        }
      },
      headers: {
        "Content-Type": "application/json"
      }
    });

    const { product } = data.data;

    if (!product.code) {
      return error;
    }

    if (!!res) {
      const url: string = `/product/${product.code}/${product.sanitizedName}`;
      res.writeHead(301, { Location: url });
      res.end();
    } else {
      Router.push(
        "/product/[sku]/[name]",
        `/product/${product.code}/${product.sanitizedName}`
      );
    }

    return {};
  } catch (e) {
    return e;
  }
};

export default Product;
