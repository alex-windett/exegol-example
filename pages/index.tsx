import { compose } from "recompose";
import Layout from "@Containers/PageLayout";
import { withTranslation } from "@Server/withI18n";
import Head from "next/head";
import { HEAD } from "@Utils/constants";

const Homepage = ({ t }: any) => {
  return (
    <Layout>
      <Head>
        <title key={HEAD.pageTitle}>
          The World’s Finest Cycling Clothing and Accessories. | Rapha
        </title>
      </Head>
      <h1>{t("hello_world")}</h1>
      <p>Rapha - Angrilu</p>
    </Layout>
  );
};

const enhance = compose(withTranslation("common"));

export default enhance(Homepage);
