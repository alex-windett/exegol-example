import React, { FunctionComponent, Fragment } from "react";
import styled from 'styled-components';
import tokens, { dim, mq } from "@rapharacing/tokens";

import UI from "@rapharacing/rapha-ui";
import Layout from "@Containers/PageLayout";
import Page from "@Components/Page";

const { Block, Button, Image, Module, Spacer, Typography } = UI;

const { Body, H1, H2 } = Typography;

const Heading = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  grid-column: span 6;
  align-items: center;

  ${mq.md`
    grid-column: 3 / span 8;
  `}

  ${mq.lg`
    grid-column: 4 / span 6;
  `}
`;

const ButtonContainer = styled.div`
    width: ${dim(13)};
`;

const List = styled.ul`
  padding: ${dim(7)} 0 ${dim(11)};
  margin: 0;
`;

const ListItem = styled.li`
  border-bottom: 1px solid ${tokens.color("grey15")};
  font-family: ${tokens.get("type.fontFamily.body")};
  font-size: ${tokens.get("type.fontSize.body")};
  list-style: none;
  padding: ${dim(3)} 0;

  &:first-child {
    border-top: 1px solid ${tokens.color("grey15")};
  }
`;

const FAQ = styled.div`
  grid-column: span 12;
  padding: ${dim(6)} 0;
  text-align: left;

	${mq.md`
    grid-column: 4 / 10;
  `}
`;

const Question = styled(Body)`
  margin: 0;
  font-weight: ${tokens.get("type.fontWeights.bold")};
`;

const Answer = styled(Body)`
  margin-top: 0;
`;

const Elemnt = styled.div`
  width: 300px;
`;

const data = {
  header: {
    title: "Ride With Us",
    subtitle: "Thanks to our partnership with Canyon Bicycles, members can rent a top of the range bike whether they’re home or away. Members can also borrow a Wahoo ELEMNT, so you’re free to ride like a local wherever you are.",
    cta: {
      book: "Book Now",
      member: "Become A Member"
    }
  },
  canyon: {
    hero: {
      src: "https://images.ctfassets.net/1stkoghspd4h/1Yz1igTPMKpVcsgi0BHVG5/2089bec72fbdd1aad58ce78931b1050d/rcc-bike-hire.jpg",
      alt: "RCC Bike Hire"
    },
    requirements: {
      title: "Requirements",
      list: [
        "Hire is charged by the day, in local currency.",
        "Maximum hire is 3 days at a time.",
        "If you became a member or have renewed your membership after the 19th of November 2019, cost of bicycle hire is £35 / $50 / €40/ A$60//¥5500/ 1,350TWD/ 360 HKD/ 55,000 KRW/ 320DKK per day.",
        "If you became a member before the 19th of November 2019 and have not yet renewed, the price of hire will be £20/ $30/ €25/ A$35/ ¥3,500/ 770 TWD/ 200 HKD/ 35,000 KRW/ 200 DKK per day.",
        "A Damage and Theft Liability Reduction Waiver is included as standard in the cost of hire.",
        "Bikes cannot be used for racing or other competitive events.",
        "You cannot travel with an RCC bike.",
        "Payment will be made in store on collection of the hire.",
        "You must provide your own helmet, clipless pedals and shoes, lights, bidons and anything else you may require. We recommend a saddle bag with a tube and levers, like this.",
        "Your bike will be set up to suit you and your pedals fitted in the Clubhouse by a member of staff. We do not recommend tinkering – you will be liable for any damage that occurs while the bike is in your possession due to over-torquing etc.",
        "Pick-up and drop-off must be done in person during Clubhouse hours.",
        "Photo ID must be presented at collection."
      ]
    },
    faq: [
      {
        title: "How do I book?",
        body: "Bookings are made online, where you can see what bikes are available in which locations. When you collect, you’ll be asked to read and sign the waiver and terms and conditions."
      },
      {
        title: "How do I know my size?",
        body: "If you’re unsure of your size, please use Canyon’s Perfect Positioning System to calculate which frame size you should hire."
      },
      {
        title: "Can I race this bike?",
        body: "No. All warranty and damage liability waivers will become null and void. You would be liable for the full price of the bicycle in the case of any damage."
      },
      {
        title: "Can I hire a bike for friends and family?",
        body: "No, you may only hire a bike for yourself to ride. Hiring a bike for another person would violate the Terms and Conditions of hire."
      },
    ],
  },
  wahoo: {
    hero: {
      src: "https://images.ctfassets.net/1stkoghspd4h/2XLA430XC32QfDw196pyyR/fe898a60194e1e92facdbf13c8cbc3c8/rcc-wahoo-hire.jpeg",
      alt: "RCC Wahoo Hire"
    },
    elemnt: {
      src: "https://images.ctfassets.net/1stkoghspd4h/32AGh4dmrKKdpYh2KT7OaF/2f2bf50692f9b0ce6a281981fd4190af/elemnt.jpg",
      alt: "Wahoo Elemnt"
    },
    title: "Wahoo Element: Navigation Made Simple",
    subtitle: "Simple to setup, simple to experience and simple to navigate. Wahoo have reinvented the bike computer experience, making the ELEMNT extremely straightforward to setup, use and navigate. The device comes pre-loaded with a selection of our favorite routes from the chapter you hire from.",
    faq: [
      {
        title: "How do I book a Wahoo GPS Bike computer?",
        body: "You can book an RCC Wahoo ELEMNT from selected Clubhouses via our online system. At the time of collection, you'll be asked to read and sign the waiver and terms and conditions. Click the link below to make a booking."
      },
      {
        title: "Does it cost to hire the device?",
        body: "No, there is no charge to hire the device."
      },
      {
        title: "Do I need to have the Wahoo Companion App on my phone?",
        body: "To get the best experience, we recommend using the Wahoo ELEMNT with the companion app on your smartphone."
      },
      {
        title: "Can I read the waiver and Terms & Conditions that I will sign on collection?",
        body: "Please click to read the Wahoo hire Terms & Conditions and Waiver that you will need to sign at the time of collection."
      },
    ],
    cta: {
      book: "Book Now",
      member: "Become A Member"
    }
  }
};

const RCCBikeHire: FunctionComponent<any> = () => {
  const { header, canyon, wahoo } = data;

  return (
    <Layout>
      <Page>
        {header &&
          <Block>
            <Heading>
              <H1>{header.title}</H1>
              <Body variant="sml">{header.subtitle}</Body>
              <ButtonContainer>
                <Button>{header.cta.book}</Button>
              </ButtonContainer>
            </Heading>
          </Block>
        }
        <Spacer />
        {canyon && (
          <Fragment>
            <Image src={canyon.hero.src} alt={canyon.hero.alt} aspect="3:2" />
            <Module border={false} title={canyon.requirements.title} subGridColumnStart={7} subGridColumnEnd={12}>
              <List>
                {canyon.requirements.list.map((li, i) => (
                  <ListItem key={i}>
                    {li}
                  </ListItem>
                ))}
              </List>
            </Module>
            <Block>
              <FAQ>
                {canyon.faq.map((faq, i) => (
                  <span key={i}>
                    <Question variant="xSml">{faq.title}</Question>
                    <Answer variant="xSml">{faq.body}</Answer>
                  </span>
                ))}
              </FAQ>
            </Block>
            <Spacer />
          </Fragment>
        )}
        {wahoo && (
          <Fragment>
            <Image src={wahoo.hero.src} alt={wahoo.hero.alt} aspect="3:2" />
            <Block>
              <Heading>
                <H2>{wahoo.title}</H2>
                <Body variant="sml">{wahoo.subtitle}</Body>
                <Elemnt>
                  <Image src={wahoo.elemnt.src} alt={wahoo.elemnt.alt} aspect="2:3" />
                </Elemnt>
                <FAQ>
                  {canyon.faq.map((faq, i) => (
                    <span key={i}>
                      <Question variant="xSml">{faq.title}</Question>
                      <Answer variant="xSml">{faq.body}</Answer>
                    </span>
                  ))}
                </FAQ>
                <ButtonContainer>
                  <Button>{wahoo.cta.book}</Button>
                </ButtonContainer>
              </Heading>
            </Block>
          </Fragment>
        )}
      </Page>
    </Layout>
  );
}

export default RCCBikeHire;
