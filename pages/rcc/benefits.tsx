import React, { FunctionComponent } from "react";
import styled from "styled-components";
import tokens, { dim, mq } from "@rapharacing/tokens";
import withApollo from "@Hocs/withApollo";
import { compose } from "recompose";
import gql from "fake-tag";

import UI from "@rapharacing/rapha-ui";
import Layout from "@Containers/PageLayout";
import Page from "@Components/Page";

const { Button, Image, HeadlineSubHeading, Spacer, Typography } = UI;

const { Anchor, Body, H4 } = Typography;

const Container = styled.div`
  padding: ${dim(2)} 0 ${dim(6)};
  position: relative;

  img {
    width: 100%;
  }

  &:after {
    background-color: ${tokens.color("grey15")};
    content: "";
    display: block;
    height: 1px;
    position: absolute;
    top: 0;
    width: 100%;
  }

  &:first-child {
    &:after {
      display: none;
    }
  }

  ${mq.md`
    border-left: 1px solid ${tokens.color("grey15")};
    padding: ${dim(10)} ${dim(2)} ${dim(5)};

    &:after {
      width: calc(100% - ${dim(4)});
    }

    &:nth-child(-n+2) {
      &:after {
        display: none;
      }
    }

    &:nth-child(2n+1) {
      border-left: 0;
    }
  `}

  ${mq.lg`
    width: unset;

    &:nth-child(-n+4) {
      &:after {
        display: none;
      }
    }

    &:nth-child(2n+1) {
      border-left: 1px solid ${tokens.color("grey15")}
    }

    &:nth-child(4n+1) {
      border-left: 0;
    }

    &:last-child {
      border-right: 1px solid ${tokens.color("grey15")};
    }
  `}
`;

const CtaContainer = styled.div`
  align-items: center;
  display: flex;
  flex-flow: column nowrap;
  padding: ${dim(8)} 0;
`;

const ButtonsContainer = styled.div`
  display: flex;
  flex-flow: row nowrap;
  padding: ${dim(2)} 0 ${dim(4)};
  width: 100%;

  ${mq.md`
    width: unset;
  `}
`;

const ButtonWrap = styled.div`
  display: inline-block;

  &:last-child {
    margin-left: ${dim(1)};
  }

  ${mq.md`
    &:last-child {
      margin-left: ${dim(4)};
    }
  `}
`;

const StyledH4 = styled(H4)`
  color: ${tokens.color("rcc.pink")};
  margin-bottom: 0;
`;

const StyledBody = styled(Body)`
  font-style: italic;
`;

const StyledGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(1, minmax(0, 1fr));

  ${mq.md`
    margin: 0 -${dim(2)};
    grid-template-columns: repeat(2, minmax(0, 1fr));
  `}

  ${mq.lg`
    grid-template-columns: repeat(4, minmax(0, 1fr));
  `}
`;

const enhance = compose(
  withApollo(() => {
    return {
      query: gql`
        {
          rccBenefits(id: "${process.env.RCC_BENEFITS_ID}") {
            title
            subtitle
            benefitsCollection {
              items {
                ... on Benefit {
                  title
                  description
                  image {
                    title
                    url
                  }
                  linksCollection {
                    items {
                      ... on Link {
                        text
                        url
                      }
                    }
                  }
                }
              }
            }
            ctaTitle
            ctaSubtitle
            ctaButtonsCollection {
              items {
                ... on Button {
                  text
                  url
                  variant
                }
              }
            }
            ctaGiftLink {
              ... on Link {
                text
                url
                
              }
            }
          }
        }
      `,
    };
  })
);

const RCCBenefits: FunctionComponent<any> = ({ data }: any) => {
  const {
    benefitsCollection,
    ctaButtonsCollection,
    ctaGiftLink,
    ctaSubtitle,
    ctaTitle,
    subtitle,
    title,
  } = data.rccBenefits;

  const { items } = benefitsCollection;

  return (
    <Layout>
      {data && (
        <Page>
          {title && (
            <HeadlineSubHeading heading={title} subHeading={subtitle} />
          )}
          <Spacer />
          {items.length > 0 && (
            <StyledGrid>
              {items.map((benefit: any, index: any) => {
                return (
                  <Container key={index}>
                    {benefit.image.url && (
                      <Image
                        src={benefit.image.url}
                        alt={benefit.image.title}
                        aspect="3:2"
                        width="400"
                      />
                    )}
                    {benefit.title && <H4>{benefit.title}</H4>}
                    {benefit.description && (
                      <Body variant="xSml">{benefit.description}</Body>
                    )}
                    {benefit.linksCollection.items.length > 0 && (
                      <div>
                        {benefit.linksCollection.items.map(
                          (link: any, index: any) => (
                            <Anchor href={link.url} key={index}>
                              {link.text}
                            </Anchor>
                          )
                        )}
                      </div>
                    )}
                  </Container>
                );
              })}
            </StyledGrid>
          )}
          {ctaButtonsCollection.items.length > 0 && (
            <CtaContainer>
              {ctaTitle && <StyledH4>{ctaTitle}</StyledH4>}
              {ctaSubtitle && (
                <StyledBody variant="xSml">{ctaSubtitle}</StyledBody>
              )}
              <ButtonsContainer>
                {ctaButtonsCollection.items.map((cta: any, index: any) => {
                  return (
                    <ButtonWrap key={index}>
                      <Button variant={cta.variant}>{cta.text}</Button>
                    </ButtonWrap>
                  );
                })}
              </ButtonsContainer>
              {ctaGiftLink.url && <Anchor>{ctaGiftLink.text}</Anchor>}
            </CtaContainer>
          )}
        </Page>
      )}
    </Layout>
  );
};

export default enhance(RCCBenefits);
