import gql from "fake-tag";
import { compose } from "recompose";
import Head from "next/head";
import styled from "styled-components";
import UI from "@rapharacing/rapha-ui";
import tokens, { dim, mq } from "@rapharacing/tokens";

import { Link } from "@Server/withI18n";

import withApollo from "@Hocs/withApollo";
import Layout from "@Containers/PageLayout";
import { HEAD } from "@Utils/constants";

import LocationsMap from "@Containers/Map/index.tsx";
import RCCRegions from "@Components/RCC/regions";

import {
  IClubhouse,
  ICityPartner,
  IClubhouseCity,
  IClubhouseRegion,
  IMapLocation,
} from "@Types/clubhouses";

const { Typography } = UI;

const PARTNER = "partner";
const CENTER: [number, number] = [-0.1011399, 51.529314];
const ZOOM = 2;

const Map = styled.section`
  width: 100%;
  height: 500px;
  position: relative;
`;

const Header = styled.header`
  text-align: center;
  border-bottom: 1px solid ${tokens.color("grey15")};
  margin: ${dim(4)} ${dim(2)};
  padding-bottom: ${dim(2)};

  ${mq.md`
    margin: 0 ${dim(2)};
    display: grid;
    grid-template-columns: repeat(12, 1fr);
  `}

  > * {
    ${mq.md`
      grid-column: 4 / span 6;
    `}
  }
`;
interface IProps {
  data: {
    clubhouseRegionCollection: {
      items: IClubhouseRegion[];
    };
  };
}

type IFlatLocation = ICityPartner | IClubhouse;

const RCCLocatins: React.FunctionComponent<IProps> = ({ data }) => {
  const allLocations = data.clubhouseRegionCollection.items.map(
    (region: IClubhouseRegion) =>
      region.citiesCollection.items.map((city: IClubhouseCity) => [
        city.clubhouse,
        ...city.partnersCollection.items.map((partner: ICityPartner) => ({
          ...partner,
          type: PARTNER,
        })),
      ])
  );

  const flatLocations = allLocations.flat(2).filter((x) => x);

  const mapLocations: IMapLocation[] = flatLocations.map((x: IFlatLocation) => {
    const { lat, lon } = x.location;

    return {
      coords: [lon, lat],
      popup: {
        html: () => {
          if ("type" in x && x.type === PARTNER) {
            return (
              <>
                <Typography.Body variant="sml">
                  <Typography.Anchor
                    href={x.url}
                    target="_blank"
                    title={x.name}
                  >
                    {x.name}
                  </Typography.Anchor>
                </Typography.Body>
                <Typography.Body variant="sml">{x.address}</Typography.Body>
              </>
            );
          }

          if ("slug" in x) {
            return (
              <>
                <Typography.Body variant="sml">
                  <Link
                    href={{
                      pathname: "/clubhouses/[slug]",
                      query: {
                        slug: x.slug,
                      },
                    }}
                    as={`/clubhouses/${x.slug}`}
                    passHref
                  >
                    <Typography.Anchor title={x.name}>
                      {x.name}
                    </Typography.Anchor>
                  </Link>
                </Typography.Body>
                <Typography.Body variant="sml">{x.address}</Typography.Body>
              </>
            );
          }
        },
      },
      marker: {
        url:
          "type" in x && x.type === PARTNER
            ? "https://mt.googleapis.com/vt/icon/name=icons/onion/SHARED-mymaps-container-bg_4x.png,icons/onion/SHARED-mymaps-container_4x.png,icons/onion/1534-cafe-cup_4x.png&highlight=ff000000,424242&scale=1.0"
            : "https://www.rapha.cc/_ui/build/images/clubhouse-pin.svg",
      },
    };
  });

  return (
    <Layout>
      <Head>
        <title key={HEAD.pageTitle}>Locations | Rapha Cycling Club</title>
      </Head>

      <Map>
        <LocationsMap
          center={CENTER}
          zoom={ZOOM}
          showControls={true}
          locations={mapLocations}
        />
      </Map>

      <Header>
        <div>
          <Typography.H1>Locations</Typography.H1>
          <Typography.Body>
            The Rapha Cycling Club has members all over the world, all connected
            by one app and many based near one of our Clubhouses or partner
            cafés. See the list below to find your local group.
          </Typography.Body>
        </div>
      </Header>

      {data.clubhouseRegionCollection.items ? (
        <RCCRegions regions={data.clubhouseRegionCollection.items} />
      ) : (
        <Typography.H2>No Locations found</Typography.H2>
      )}
    </Layout>
  );
};

const enhance = compose<IProps, {}>(
  withApollo(() => {
    return {
      query: gql`
        {
          clubhouseRegionCollection(limit: 5) {
            items {
              name
              citiesCollection {
                items {
                  name
                  membersGroup
                  rideLeaders
                  bikeHire
                  clubhouse {
                    name
                    slug
                    address
                    location {
                      lat
                      lon
                    }
                  }
                  partnersCollection(limit: 10) {
                    items {
                      name
                      url
                      location {
                        lat
                        lon
                      }
                    }
                  }
                }
              }
            }
          }
        }
      `,
    };
  })
);

export default enhance(RCCLocatins);
