import styled from "styled-components";
import tokens, { dim, mq } from "@rapharacing/tokens";
import UI from "@rapharacing/rapha-ui";
import Layout from "@Containers/PageLayout";

const { Button, Rcc, Typography, VideoPlayer } = UI;

const { H4 } = Typography;

const {
  RccBenefitList,
  RccFeatures,
  RccJoin,
  RccOneFigure,
  RccOneFigureSupporting,
  RccTwoFigure,
  RccUsps,
} = Rcc;

const data = {
  header: {
    title: "Rapha Cycling Club",
    ctaText: "Join the club",
    ctaScrollTarget: "join-rcc",
    buttonVariant: "quaternaryRCC",
  },
  video: {
    url:
      "https://s1.adis.ws/rapha/_vid/rcc_relaunch_2020_16-9_lowrez_web/5f0e9cfd-7115-4645-9373-1e01e036e23e/video/4750485d-d24b-472b-9ada-e9ae768f71ac.mp4?protocol=https",
    poster: "https://i8.amplience.net/i/rapha/RCC_Posterframe?w=2000",
  },
  whatIsRcc: {
    anchor: "what-is-rcc",
    title: "What is the RCC",
    description:
      "The Rapha Cycling Club is a global community of passionate, active cyclists with exclusive access to our most sought after products, experiences and offers.",
    usps: [
      {
        title: "Priority at Rapha",
        icon: "RccPriority",
        scrollTo: "priority-at-rapha",
      },
      {
        title: "Member-only Products",
        icon: "RccProducts",
        scrollTo: "member-only-products",
      },
      {
        title: "Active Social Network",
        icon: "RccSocial",
        scrollTo: "active-social-network",
      },
      {
        title: "Rides and experiences",
        icon: "RccRides",
        scrollTo: "rides-and-experiences",
      },
      {
        title: "Meeting places",
        icon: "RccMeeting",
        scrollTo: "meeting-places",
      },
    ],
    ctaText: "Join the club",
    ctaUrl: "/",
  },
  priority: {
    anchor: "priority-at-rapha",
    title: "Membership Benefits",
    highlightTitle: "Priority at Rapha",
    highlightDescription:
      "Priority access to the best of Rapha, with exclusive early access to all of our most anticipated releases, limited editions, collaborations and seasonal sales.",
    highlightCaption1: "Explore Hooded GORE-TEX Pullover",
    highlightCaption2: "RCC x Paul Smith Collection",
    buttonText: "View Coming Soon",
    buttonLink: "/stories/coming-soon",
    buttonVariant: "quinaryRCC",
  },
  memberProducts: {
    anchor: "member-only-products",
    figureOne: "https://i8.amplience.net/i/rapha/RCC_Member_Only_001?w=1200",
    figureOneCaption: "RCC Souplesse Aero Jersey",
    figureTwo: "https://i8.amplience.net/i/rapha/RCC_Member_Only_002?w=1200",
    figureTwoCaption: "RCC Souplesse Aero Jersey",
    title: "Member Only Products",
    description:
      "Exclusive member-only products created by Rapha and in collaboration with selected brand partners, including Canyon, Giro, Scicon and Silca, as well as the world’s best and most distinctive club kit.",
    buttonOneText: "Shop Mens",
    buttonOneLink:
      "/collections/rapha-cycling-club-kit/category/Mixed-rcc?q=gender:Mens",
    buttonTwoText: "Shop Womens",
    buttonTwoLink:
      "/collections/rapha-cycling-club-kit/category/Mixed-rcc?q=gender:Womens",
  },
  social: {
    anchor: "active-social-network",
    figure: "https://i8.amplience.net/i/rapha/DW_B3676?w=2000",
    figureCaption: "RCC Members climbing Sa Calobra",
    supportImage:
      "https://i8.amplience.net/i/rapha/App_Comp_Social_Network?w=1200",
    title: "Active Social Network",
    description:
      "A global network of more than 13,000 like-minded, passionate cyclists and ride leaders connected by an easy-to-use app, with hundreds of rides, routes and group chats every week.",
    buttonText: "Download the app",
    buttonLink: "/stories/rapha-cycling-club-app",
  },
  rides: {
    anchor: "rides-and-experiences",
    figure: "https://i8.amplience.net/i/rapha/DW_B5010?w:=2000",
    figureCaption: "RCC Members-only area at L’Étape",
    title: "Meeting Places",
    description:
      "Bike hire and 50% off coffee at Rapha Clubhouses and at a network of approved partner cycling cafés around the world.",
    buttonText: "See summits and escapes",
    buttonLink: "/rcc/summits-escapes",
  },
  meeting: {
    anchor: "meeting-places",
    figure: "https://i8.amplience.net/i/rapha/DSC04217?w=2000",
    figureCaption: "Clubhouses & partner cafes around the world",
    supportImage:
      "https://i8.amplience.net/i/rapha/RCC_Meeting_Places_002?w=1200",
    title: "Meeting Places",
    description:
      "Bike hire and 50% off coffee at Rapha Clubhouses and at a network of approved partner cycling cafés around the world.",
    buttonText: "See our Locations",
    buttonLink: "/rcc/locations",
  },
  keyBenefits: {
    anchor: "key-benefits",
    title: "Key Benefits",
    benefits: [
      {
        text:
          "Exclusive early access to all of Rapha's limited edition and major new releases.",
      },
      { text: "Early access to all seasonal sales." },
      {
        text:
          "An extensive range of members-only RCC products and special editions with selected brands.",
      },
      { text: "Signature club kit, recognisable around the world." },
      { text: "Exclusive RCC icons within Rapha Custom." },
      { text: "Half-price express shipping on all orders." },
      { text: "Offers and discounts from selected industry partners." },
      { text: "Basic personal injury insurance." },
      {
        text:
          "Members only app to instantly connect to a global network of ride leaders, group rides and members just like you.",
      },
      { text: "Members only forum." },
      {
        text:
          "Support at the world’s biggest sportives, including L’Etape du Tour and Mallorca 312.",
      },
      {
        text:
          "Exclusive riding trips and access to fully-supported RCC Summits along with priority access to key Rapha events.",
      },
      {
        text:
          "Half-price coffee at 23 Rapha Clubhouses and a network of global partner cafes.",
      },
      {
        text:
          "Bike hire of high-spec Canyon bikes from all Clubhouse locations.",
      },
    ],
    ctaText: "Learn more about benefits",
    ctaUrl: "/",
  },
  join: {
    anchor: "join-rcc",
    title: "Join the RCC",
    ctas: [
      {
        title: "Rcc Membership",
        subtitle: "£70 for 12 months",
        image: {
          url:
            "https://i8.amplience.net/i/rapha/RCC2020-RCCMembership2020?w=500",
          alt: "Rcc Membership",
        },
        ctaText: "Buy Membership",
        ctaUrl: "/",
      },
      {
        title: "RCC Membership Bundles",
        subtitle: "Save 10%",
        image: {
          url: "https://i8.amplience.net/i/rapha/Bundle?w=500",
          alt: "RCC Membership Bundles",
        },
        ctaText: "Shop Bundles",
        ctaUrl: "/",
      },
      {
        title: "Gift Membership",
        subtitle: "£70 for 12 months",
        image: {
          url:
            "https://i8.amplience.net/i/rapha/RCC2020-RCCMembership2020Gift?w=500",
          alt: "Gift Membership",
        },
        ctaText: "Gift Membership",
        ctaUrl: "/",
      },
    ],
  },
};

const IdWrap = styled.div`
  display: block;
`;

const Header = styled.div`
  align-items: center;
  background-color: ${tokens.color("black")};
  display: flex;
  height: 70px;
  justify-content: space-between;
  padding: 0 ${dim(2)};
  position: sticky;
  top: -1px;
  z-index: 2;

  ${mq.md`
    padding: 0 ${dim(4)};
  `}
`;

const Title = styled(H4)`
  color: ${tokens.color("white")};
  font-size: 18px;

  ${mq.md`
    font-size: 24px;
  `}
`;

const VideoContainer = styled.div`
  margin: 0 ${dim(2)} -${dim(20)};

  ${mq.md`
    margin: 0 ${dim(4)} -${dim(13)};
  `}
`;

export default () => {
  const {
    priority,
    header,
    whatIsRcc,
    memberProducts,
    rides,
    social,
    meeting,
    keyBenefits,
    join,
    video,
  } = data;

  const handleLabelClick = (event: any) => {
    const data = event.currentTarget.getAttribute("data");
    const scrollTarget = document.getElementById(data);

    if (scrollTarget) {
      scrollTarget.scrollIntoView({
        behavior: "smooth",
      });
    }
  };

  return (
    <Layout>
      <Header>
        <Title>{header.title}</Title>
        <Button
          data={header.ctaScrollTarget}
          variant={header.buttonVariant}
          onClick={handleLabelClick}
        >
          {header.ctaText}
        </Button>
      </Header>
      <VideoContainer>
        <VideoPlayer fullwidth mediaSource={video.url} poster={video.poster} />
      </VideoContainer>
      <RccUsps {...whatIsRcc} handleLabelClick={handleLabelClick} />
      <IdWrap id={priority.anchor}>
        <RccFeatures {...priority} />
      </IdWrap>
      <IdWrap id={memberProducts.anchor}>
        <RccTwoFigure {...memberProducts} />
      </IdWrap>
      <IdWrap id={social.anchor}>
        <RccOneFigureSupporting {...social} />
      </IdWrap>
      <IdWrap id={rides.anchor}>
        <RccOneFigure {...rides} />
      </IdWrap>
      <IdWrap id={meeting.anchor}>
        <RccOneFigureSupporting {...meeting} />
      </IdWrap>
      <IdWrap id={keyBenefits.anchor}>
        <RccBenefitList {...keyBenefits} />
      </IdWrap>
      <IdWrap id={join.anchor}>
        <RccJoin {...join} />
      </IdWrap>
    </Layout>
  );
};
