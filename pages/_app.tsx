import "cross-fetch/polyfill";

import React from "react";

// @ts-ignore
import { GraphQLProvider } from "graphql-react";

// @ts-ignore
import { withGraphQLApp } from "next-graphql-react";
import App, { NextWebVitalsMetric } from "next/app";
import compose from "recompose/compose";

import GlobalErrorBoundary from "@Containers/GlobalErrorBoundary";
import RouteProgress from "@Components/RouteProgress";

import { appWithTranslation } from "@Server/withI18n";

import MasterProvider from "../context";

import report from "@Utils/webVitals";

import "mapbox-gl/dist/mapbox-gl.css";

export const reportWebVitals = (metrics: NextWebVitalsMetric) => {
  report(metrics);
};

class CustomApp extends App {
  render() {
    // @ts-ignore
    const { Component, pageProps, graphql } = this.props;

    return (
      <GlobalErrorBoundary>
        <GraphQLProvider graphql={graphql}>
          <MasterProvider>
            <RouteProgress />
            <Component {...pageProps} />
          </MasterProvider>
        </GraphQLProvider>
      </GlobalErrorBoundary>
    );
  }
}

const enhance = compose(appWithTranslation, withGraphQLApp);

// @ts-ignore
export default enhance(CustomApp);
