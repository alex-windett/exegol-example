import React from "react";
import PropTypes from "prop-types";
import { NextPageContext } from "next";
import styled from "styled-components";

import { withTranslation } from "@Server/withI18n";

import Layout from "@Containers/PageLayout";

import UI from "@rapharacing/rapha-ui";

const { Typography, Image } = UI;

const { H3, Body } = Typography;

const Wrapper = styled.section`
  text-align: center;
  margin: 60px;
`;

const Error = ({ statusCode, t }: any) => (
  <Layout>
    <Wrapper>
      <H3>{t("common_went_wrong")}</H3>

      <Body>
        {statusCode
          ? t("error_with_status", { statusCode })
          : t("error_without_status")}
      </Body>

      <Image
        src="https://i1.adis.ws/i/rapha/404"
        alt={t("common_went_wrong")}
      />
    </Wrapper>
  </Layout>
);

Error.getInitialProps = async ({ res, err }: NextPageContext) => {
  let statusCode = null;
  if (res) {
    ({ statusCode } = res);
  } else if (err) {
    ({ statusCode } = err);
  }
  return {
    namespacesRequired: ["common"],
    statusCode,
  };
};

Error.defaultProps = {
  statusCode: null,
};

Error.propTypes = {
  statusCode: PropTypes.number,
  t: PropTypes.func.isRequired,
};

export default withTranslation("common")(Error);
