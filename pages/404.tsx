import React from "react";
import PropTypes from "prop-types";

import { withTranslation } from "@Server/withI18n";

// @ts-ignore
import Layout from "@Containers/PageLayout";

import ErrorComponent from "@Components/PageError";

const Error = (props: any) => (
  <Layout>
    <ErrorComponent {...props} />
  </Layout>
);

Error.defaultProps = {
  statusCode: null,
};

Error.propTypes = {
  statusCode: PropTypes.number,
  t: PropTypes.func.isRequired,
};

export default withTranslation("common")(Error);
