import React, { FunctionComponent } from "react";
import Head from "next/head";

import AddressForm from "@Containers/Forms/Checkout/address";

// UI
import Layout from "@Containers/PageLayout";
import { Wrapper } from "@Components/Checkout/Address/ui";

const Address: FunctionComponent<any> = ({}) => {
  return (
    <Layout>
      <Head>
        <title key="title">Add new Address | Rapha</title>
      </Head>
      <Wrapper>
        <AddressForm />
      </Wrapper>
    </Layout>
  );
};

export default Address;
