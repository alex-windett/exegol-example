import React from "react";
import Head from "next/head";
import dynamic from "next/dynamic";
import { compose } from "recompose";
import { withRouter, SingletonRouter } from "next/router";
import moment from "moment";
import styled from "styled-components";

// import { useScrollPosition } from "@n8tb1t/use-scroll-position";

import { HEAD } from "@Utils/constants";
import { Link, Router, withTranslation } from "@Server/withI18n";

// @ts-ignore
import Layout from "@Containers/PageLayout";

import UI from "@rapharacing/rapha-ui";

import withGraphqlData from "@Hocs/withApollo";
import query from "@Queries/story";

import mapModelToComponent from "@Utils/mapModelToComponent";

const {
  HeadlineSubHeading,
  Image,
  StoryCreditShare,
  Breadcrumb,
  Typography,
} = UI;

const StoryModuleSection = styled.section`
  & + & {
    padding: 45px 0;
  }
`;

const BreadcrumbItem = styled.a`
  ${Typography.Label} {
    margin: 0;
    text-decoration: underline;
  }
`;

const INITIAL_OPACITY = 0;
const Story = ({ router, data }: any) => {
  const [story] = data.storyCollection.items;

  if (!story) return Router.push("/404");

  const [initialTop, setInitialTop] = React.useState();
  const [opacity] = React.useState<number>(INITIAL_OPACITY);
  // const [opacity, setOpacity] = React.useState<number>(INITIAL_OPACITY);
  const storyRef = React.useRef(null);

  React.useEffect(() => {
    if (storyRef && storyRef.current) {
      // @ts-ignore
      setInitialTop(storyRef.current.getBoundingClientRect().top);
    }
  }, [initialTop]);

  // useScrollPosition(
  //   ({ currPos }) => {
  //     if (currPos.y > INITIAL_OPACITY) {
  //       // @ts-ignore
  //       const opactiy = currPos.y / initialTop;
  //       setOpacity(1 - opactiy);
  //     } else {
  //       setOpacity(1);
  //     }
  //   },
  //   [initialTop],

  //   // @ts-ignore
  //   storyRef
  // );

  const [subCategory] = story.subCategoryCollection.items;

  const background: string =
    story.backgroundColor && `rgba(${story.backgroundColor}, ${opacity})`;

  // debugger
  return (
    // @ts-ignore
    <Layout background={background}>
      <Head>
        <title key={HEAD.pageTitle}>{story.heading}</title>
        <meta key={HEAD.ogTitle} property="og:title" content={story.heading} />
        <meta
          key={HEAD.ogImage}
          property="og:image"
          content={story.heroMedia.url}
        />
      </Head>

      <div ref={storyRef}>
        <Image src={story.heroMedia.url} alt={story.heading} aspect="16:9" />

        <Breadcrumb>
          <Link
            href={{
              pathname: "/stories/[category]",
              query: {
                category: subCategory.parentCategory.slug,
              },
            }}
            as={`/stories/${subCategory.parentCategory.slug}`}
            passHref
          >
            <BreadcrumbItem>
              <Typography.Label variant="sml">
                {subCategory.parentCategory.name}
              </Typography.Label>
            </BreadcrumbItem>
          </Link>

          <Link
            href={{
              pathname: "/stories/[category]/[subCategory]",
              query: {
                category: subCategory.parentCategory.slug,
                subCategory: subCategory.slug,
              },
            }}
            as={`/stories/${subCategory.parentCategory.slug}/${subCategory.slug}`}
            passHref
          >
            <BreadcrumbItem>
              <Typography.Label variant="sml">
                {subCategory.name}
              </Typography.Label>
            </BreadcrumbItem>
          </Link>
        </Breadcrumb>

        <HeadlineSubHeading
          heading={story.heading}
          subHeading={story.subHeading}
          date={moment(story.publsihedDate).format("Do MMMM YYYY")}
        />
        <StoryCreditShare
          credit1={story.credit1}
          credit2={story.credit2}
          credit3={story.credit2}
          url={router.href}
        />

        {story.storyModulesCollection &&
          story.storyModulesCollection.items.map((module: any, i: number) => {
            if (!module || module.__typename) return null;

            const ComponentName = mapModelToComponent({
              moduleName: module.__typename,
            });

            if (!ComponentName) {
              console.log(`No __typename provided`);
              return null;
            }

            const Comp = dynamic(() =>
              import("@Components/Story/" + ComponentName)
            );

            if (!Comp) {
              console.log(
                `Cannot find component for ${module.__typename} on Story Page`
              );
              return null;
            }

            return (
              <StoryModuleSection
                data-id={`Section-${module.__typename}`}
                key={i}
              >
                <Comp {...module} router={router} />
              </StoryModuleSection>
            );
          })}
      </div>
    </Layout>
  );
};

const enhance = compose(
  withTranslation("common"),
  withRouter,
  withGraphqlData(({ router }: { router: SingletonRouter }) => ({
    query,
    variables: {
      slug: router.query.slug,
      limit: 25,
      skip: 0,
      preview: router.query.category.includes("preview_entry_category"),
    },
  }))
);

// @ts-ignore
export default enhance(Story);
