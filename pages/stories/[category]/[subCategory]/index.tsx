import { NextComponentType } from "next";

import React from "react";
import { compose } from "recompose";
import { withRouter, SingletonRouter } from "next/router";
import { StoriesPage } from "../../";

import withGraphqlData from "@Hocs/withApollo";

import { subCategoryQuery } from "@Queries/stories";

const Stories: NextComponentType = (props: any) => {
  return <StoriesPage {...props} />;
};

const enhance = compose(
  withRouter,
  withGraphqlData(({ router }: { router: SingletonRouter }) => ({
    query: subCategoryQuery,
    variables: {
      subCategorySlug: router.query.subCategory,
      categorySlug: router.query.category
    }
  }))
);

const Enhanced: NextComponentType = enhance(Stories);

export default Enhanced;
