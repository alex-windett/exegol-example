import { NextComponentType } from "next";

import React from "react";
import { compose } from "recompose";
import { withRouter, SingletonRouter } from "next/router";
import { StoriesPage } from "../";

import withGraphqlData from "@Hocs/withApollo";

// @ts-ignore
import { categoryQuery } from "@Queries/stories";

const Page: NextComponentType = (props: any) => {
  return <StoriesPage {...props} />;
};

const enhance = compose(
  withRouter,
  withGraphqlData(({ router }: { router: SingletonRouter }) => ({
    query: categoryQuery,
    variables: { categorySlug: router.query.category }
  }))
);

const Enhanced: NextComponentType = enhance(Page);

export default Enhanced;
