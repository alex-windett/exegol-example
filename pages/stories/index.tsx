import { NextComponentType } from "next";

// @ts-ignore
import React, { ReactNode } from "react";
import { compose } from "recompose";
import { withRouter } from "next/router";
import Head from "next/head";
import styled from "styled-components";

import withGraphqlData from "@Hocs/withApollo";
import Layout from "@Containers/PageLayout";
import UI from "@rapharacing/rapha-ui";
import { HEAD, RAPHA_DEFAULT } from "@Utils/constants";
import { Link } from "@Server/withI18n";

import { allStoriesQuery } from "@Queries/stories";

const { Block, Card, Typography, Breadcrumb } = UI;

const BreadcrumbItem = styled.a`
  ${Typography.Label} {
    margin: 0;
    text-decoration: underline;
  }
`;

const StoryRoute: React.FC<{
  slug: string;
  subCategoryCollection?: any;
}> = ({ children, slug, subCategoryCollection }) => {
  const [subCategory] = subCategoryCollection.items;
  return (
    <Link
      href={{
        pathname: "/stories/[category]/[subCategory]/[slug]",
        query: {
          category: subCategory.parentCategory.slug,
          subCategory: subCategory.slug,
          slug,
        },
      }}
      as={`/stories/${subCategory.parentCategory.slug}/${subCategory.slug}/${slug}`}
      passHref
    >
      <a>{children}</a>
    </Link>
  );
};

export const StoriesPage: NextComponentType = ({ router, data }: any) => {
  const storiesList = data.storyCollection
    ? data.storyCollection.items.reduce((a: any, c: any) => {
        return c ? [...a, ...c.linkedFrom.storyCollection.items] : a;
      }, [])
    : [];

  if (storiesList.length < 1) {
    return (
      <Layout>
        <Typography.H3>No stories</Typography.H3>
      </Layout>
    );
  }

  const StoryHead = () => {
    const pageTitleArray: string[] = [
      !!router.query.category && data.category.items[0].name,
      !!router.query.subCategory && data.subCategory.items[0].name,
    ].filter((x) => x);

    const pageTitle: string =
      pageTitleArray.length > 0
        ? pageTitleArray.join(" | ") + " | Rapha"
        : "Rapha";

    return (
      <Head>
        <title key={HEAD.pageTitle}>{pageTitle}</title>
        <meta key={HEAD.ogTitle} property="og:title" content={pageTitle} />
        <meta
          key={HEAD.ogImage}
          property="og:image"
          content={
            storiesList.length < 1
              ? RAPHA_DEFAULT
              : storiesList[0].heroMedia.url
          }
        />
      </Head>
    );
  };

  return (
    <Layout>
      <StoryHead />
      {router.query.category && (
        <Breadcrumb>
          <Link
            href={{
              pathname: "/stories/[category]",
              query: {
                category: router.query.category,
              },
            }}
            as={`/stories/${router.query.category}`}
            passHref
          >
            <a>
              <BreadcrumbItem>
                <Typography.Label variant="sml">
                  {data.category.items[0].name}
                </Typography.Label>
              </BreadcrumbItem>
            </a>
          </Link>

          {router.query.subCategory && (
            <Link
              href={{
                pathname: "/stories/[category]/[subCategory]",
                query: {
                  category: router.query.category,
                  subCategory: router.query.subCategory,
                },
              }}
              as={`/stories/${router.query.category}/${router.query.subCategory}`}
              passHref
            >
              <a>
                <BreadcrumbItem>
                  <Typography.Label variant="sml">
                    {data.subCategory.items[0].name}
                  </Typography.Label>
                </BreadcrumbItem>
              </a>
            </Link>
          )}
        </Breadcrumb>
      )}

      <Block>
        {storiesList.map((story: any) => (
          <Card
            key={story.sys.id}
            type="feature"
            body={story.subHeading}
            title={story.heading}
            image={{ src: story.heroMedia.url, alt: story.title }}
            Router={({ children }: { children: ReactNode }) => (
              <StoryRoute {...story}>{children}</StoryRoute>
            )}
          />
        ))}
      </Block>
    </Layout>
  );
};

const enhance = compose(
  withRouter,
  withGraphqlData({
    query: allStoriesQuery,
  })
);

const Enhanced: NextComponentType = enhance(StoriesPage);

export default Enhanced;
