import { useState } from "react";
import Head from "next/head";
import Layout from "@Containers/PageLayout";
import compose from "recompose/compose";

// HOCS
import { withSSRContext } from "aws-amplify";
import { withFormik } from "formik";
import withContext from "@Hocs/withContext";
import Router, { NextRouter, withRouter } from "next/router";

// Validation
import * as Yup from "yup";

// UI
import ui from "@rapharacing/rapha-ui";
const { Button, Input } = ui;
import {
  Title,
  Body,
  Submit,
  Section,
  Confirm,
  Resend,
  Border,
} from "@Components/Auth/ui";

// Makes Login SSR aware
const { Auth } = withSSRContext();

const formik = {
  displayName: "ConfirmRegister",
  validationSchema: Yup.object({
    code: Yup.number().required("Code is required"),
  }),
  mapPropsToValues: ({ router }: { router: NextRouter }) => ({
    code: "",
    email: router.query.email,
  }),
  handleSubmit: async ({ code, email }: any, { props, setSubmitting }: any) => {
    setSubmitting(true);

    try {
      await Auth.confirmSignUp(email, code);

      Router.push("/login");
    } catch (error) {
      const { appContext } = props;

      console.error(error);
      await appContext.setGlobalErrors(error.message);
    } finally {
      setSubmitting(false);
    }
  },
};

const ConfirmRegister = ({
  errors,
  handleBlur,
  handleChange,
  handleSubmit,
  // isSubmitting,
  // isValid,
  touched,
  values,
  appContext,
}: any) => {
  const [reconfirmLoading, setreconfirmLoading] = useState(false);
  const handleReSend = async () => {
    setreconfirmLoading(true);

    try {
      await Auth.resendSignUp(values.email);
      await appContext.setGlobalErrors("Confirmation code resent");
    } catch (error) {
      console.error("error resending code: ", error);
      await appContext.setGlobalErrors(error.message);
    } finally {
      setreconfirmLoading(false);
    }
  };

  return (
    <Layout>
      <Head>
        <title key="title">Rapha | Confirm Registration</title>
      </Head>

      <Title>Confrim your account</Title>

      <Body variant="sml">
        We have sent an email to {values.email} to confirm your account
      </Body>

      <Section>
        <Confirm>
          <form onSubmit={handleSubmit}>
            <Input
              disabled
              aria-label="Email"
              aria-required="true"
              autoComplete="email"
              id="email"
              label="Email"
              name="email"
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder="Email"
              type="email"
              value={values.email}
            />
            {touched.email && errors.email && <div>{errors.email}</div>}

            <Input
              aria-required="true"
              errors={errors.code}
              id="code"
              label="Confirmation Code"
              name="code"
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder="Confirmation Code"
              type="text"
              value={values.code}
            />
            {touched.code && errors.code && <div>{errors.code}</div>}

            <Submit>
              <Button
                variant="primary"
                type="submit"
                // disabled={!isValid || isSubmitting}
              >
                Confirm
              </Button>
            </Submit>
          </form>
        </Confirm>

        <Border />

        <Resend>
          <Button disabled={reconfirmLoading} onClick={handleReSend}>
            Re-send confirmation code
          </Button>
        </Resend>
      </Section>
    </Layout>
  );
};

const enhance = compose(
  withRouter,
  withContext,
  // @ts-ignore
  withFormik<Router, FormTypes.FormValues>(formik)
);

export default enhance(ConfirmRegister);
