import compose from "recompose/compose";
import React from "react";
import Head from "next/head";
import { withRouter } from "next/router";
import styled from "styled-components";
import { Link } from "@Server/withI18n";
import tokens, { dim } from "@rapharacing/token";

import { RegisterForm } from "@Containers/Forms";
import Layout from "@Containers/PageLayout";
import UI from "@rapharacing/rapha-ui";

const { Typography, Button } = UI;

const Title = styled(Typography.H3)`
  text-align: center;
`;

const Section = styled.section`
  display: grid;
  grid-template-columns: repeat(12, 1fr);
`;

const LoginIn = styled.div`
  grid-column: 2 / span 4;
`;

const LoginForm = styled.div`
  grid-column: 8 / span 4;
  display: flex;
  flex-direction: column;
`;

const Border = styled.div`
  grid-column: 7 / span 1;
  width: 1px;
  background: ${tokens.color("grey15")};
  height: 100%;
`;

const ColumnTitle = styled(Typography.Label)`
  text-align: center;
  margin-bottom: ${dim(4)};
`;

const LoginButton = styled(Button)`
  width: 100%;
`;

const enhance = compose(withRouter);

const Register = () => {
  return (
    <Layout>
      <Head>
        <title key="title">Rapha | Register</title>
      </Head>

      <Title>Sign Up</Title>

      <Section>
        <LoginIn>
          <ColumnTitle>New Customer</ColumnTitle>
          <RegisterForm />
        </LoginIn>

        <Border />

        <LoginForm>
          <ColumnTitle>Returning Customers</ColumnTitle>

          <Link href="/login">
            <a>
              <LoginButton variant="secondary">Sign In</LoginButton>
            </a>
          </Link>
        </LoginForm>
      </Section>
    </Layout>
  );
};

const Enhanced = enhance(Register);

export default Enhanced;
