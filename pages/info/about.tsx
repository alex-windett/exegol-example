import React, { FunctionComponent } from "react";
import Head from "next/head";

// UI
import Layout from "@Containers/PageLayout";

const About: FunctionComponent<any> = ({}) => {
  return (
    <Layout>
      <Head>
        <title key="title">About | Rapha</title>
      </Head>

      <div>
        <p>
          This page actually lives under `pages/info` but rewrites allows us to
          map an incoming request path to a different destination path.
        </p>
      </div>
    </Layout>
  );
};

export default About;
