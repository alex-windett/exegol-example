import Document, {
  DocumentContext,
  DocumentInitialProps,
  Head,
  Html,
  Main,
  NextScript,
} from "next/document";

import { ServerStyleSheet } from "styled-components";
import UI from "@rapharacing/rapha-ui";
import i18n, { RTL_LANGS } from "@Server/withI18n";

const { GlobalStyles } = UI;

import { createGlobalStyle } from "styled-components";
import { bool } from "aws-sdk/clients/signer";

const RaphaGlobals = createGlobalStyle`
  body {
    margin: 0;
    -webkit-font-smoothing: antialiased;
    text-rendering: optimizeLegibility;
  }
  a:focus, a:active {
    outline: none;
    -moz-outline-style: none;
  }
`;

// const Auth = new Authentication();

interface IProps extends DocumentInitialProps {
  isRTL?: bool;
  currentLanguage: string;
}

export default class MyDocument extends Document<IProps> {
  static async getInitialProps(ctx: DocumentContext) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App: any) => (props: any) =>
            sheet.collectStyles(<App {...props} />),
        });

      const initialProps = await Document.getInitialProps(ctx);

      //@ts-ignore
      const currentLanguage = ctx.req ? ctx.req.language : i18n.language;
      const additionalProps = {
        isRTL: RTL_LANGS.some((lng) => lng === currentLanguage),
        currentLanguage,
      };

      return {
        ...initialProps,
        ...additionalProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      };
    } finally {
      sheet.seal();
    }
  }

  render() {
    const { isRTL, currentLanguage } = this.props;

    return (
      <Html lang={currentLanguage} dir={isRTL ? "rtl" : "ltr"}>
        <Head />

        <style jsx global>{`
          body {
            margin: 0px;
            padding: 0px;
          }
          img {
            max-width: 100%;
          }
        `}</style>

        <GlobalStyles />
        <RaphaGlobals />

        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
