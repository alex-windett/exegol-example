// /products/:category

import { withRouter, SingletonRouter } from "next/router";
import Head from "next/head";
import withApollo from "@Hocs/withApollo";
import { compose } from "recompose";
import { withTranslation } from "@WithTranslations";
import query from "@Queries/productCategory";
import Layout from "@Containers/PageLayout";
import UI from "@rapharacing/rapha-ui";
import { formatProductData } from "@Utils/index";
import * as Types from "@Types/productCategory";
import { ContentCard } from "@Components/Products";
import { HEAD, RAPHA_DEFAULT } from "@Utils/constants";
import { ProductsHeader, EmptyCategory } from "@Components/Products";
import Page from "@Components/Page";

const { PLPGrid } = UI;

import { ProductCell } from "@Components/Product";

const enhance = compose(
  withTranslation("common"),
  withRouter,
  withApollo(({ router }: { router: SingletonRouter }) => ({
    query,
    variables: {
      category: router.query.category,
    },
  }))
);

const ProductItem = ({ product }: any) => {
  return <ProductCell {...formatProductData(product)} />;
};

interface IProps {
  data: Types.ICategoryPage;
  router: SingletonRouter;
  t: any;
}

const Category = ({ data, router, t }: IProps) => {
  const { items } = data.productCategoryCollection;

  const category: Types.ICategoryCMSEntry = items[0] || {};

  const contentBlocksCollection = category.contentBlocksCollection || {};

  const contentSlots: string = category.contentSlots || "";

  const products: Types.IProductCell[] = data.hrybisProductCategory
    ? data.hrybisProductCategory.items
    : [];

  const Products = () => (
    <>
      {products.map((product: Types.IProductCell, ind: number) => (
        <ProductItem key={ind} product={product} />
      ))}
    </>
  );

  const ContentCards = contentBlocksCollection.items
    ? contentBlocksCollection.items.map(
        (content: Types.IProductCategoryContentSlot, i: number) => (
          <ContentCard key={i} content={content} t={t} />
        )
      )
    : [];

  const ContentGridItems = ({ contentSlots }: { contentSlots: number[] }) => {
    const Items = () => (
      <>
        {products.flatMap((product: Types.IProductCell, ind: number) => {
          const Cell = () => (
            <ProductItem product={product} key={product.code} />
          );

          const contentIndex = contentSlots.indexOf(ind);
          if (ContentCards.length > 0 && contentIndex > -1) {
            const Content = () => ContentCards[contentIndex];

            return [<Content />, <Cell />];
          }

          return <Cell />;
        })}
      </>
    );

    return <Items />;
  };

  // Convert commar sepearted array to integer
  const contentEntries: number[] = contentSlots
    .split(",")
    .map((x: string) => parseInt(x.trim()));

  return (
    <Layout>
      <Head>
        <title key={HEAD.pageTitle}>
          {`${category.metaTitle || category.title} | Rapha`}
        </title>
        <meta
          key={HEAD.ogTitle}
          property="og:title"
          content={`${category.metaTitle || category.title} | Rapha`}
        />

        <meta
          key={HEAD.ogImage}
          property="og:image"
          content={category.metaImage ? category.metaImage.url : RAPHA_DEFAULT}
        />
      </Head>

      <Page>
        <ProductsHeader category={category} />

        <div>
          {products.length < 1 ? (
            <EmptyCategory router={router} />
          ) : (
            <PLPGrid hasHero={category.hasHero}>
              {contentBlocksCollection.items &&
              contentBlocksCollection.items.length > 0 ? (
                <ContentGridItems contentSlots={contentEntries} />
              ) : (
                <Products />
              )}
            </PLPGrid>
          )}
        </div>
      </Page>
    </Layout>
  );
};

// @ts-ignore
export default enhance(Category);
