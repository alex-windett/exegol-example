import React from "react";

import { withTranslation } from "@Server/withI18n";
import Layout from "@Containers/PageLayout";

import UI from "@rapharacing/rapha-ui";

const { Typography } = UI;

const Products = () => (
  <Layout>
    <Typography.H1>Welcome to the shop</Typography.H1>
  </Layout>
);

export default withTranslation("common")(Products);
