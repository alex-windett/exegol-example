import Head from "next/head";

import { Link } from "@Server/withI18n";
import styled from "styled-components";

import { ResetPasswordConfrim } from "@Containers/Forms";
import Layout from "@Containers/PageLayout";
import UI from "@rapharacing/rapha-ui";

const { Typography } = UI;

const Title = styled(Typography.H3)`
  text-align: center;
`;

const Grid = styled.section`
  display: grid;
  grid-template-columns: repeat(12, minmax(0, 1fr));
`;

const Wrapper = styled.div`
  grid-column: 4 / span 6;
`;

const Login = () => {
  return (
    <Layout>
      <Head>
        <title key="title">Rapha | Reset Password</title>
      </Head>

      <Title>Update your Password</Title>

      <Grid>
        <Wrapper>
          <ResetPasswordConfrim />

          <Link href="/password/reset" passHref>
            <Typography.Anchor>Get your code again</Typography.Anchor>
          </Link>
        </Wrapper>
      </Grid>
    </Layout>
  );
};

export default Login;
