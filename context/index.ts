import { combineProviders } from "react-combine-providers";

import ContextProvier, { Context } from "./app";
import UserProvider, { UserContext } from "./user";

const providers = combineProviders();

providers.push(ContextProvier);
providers.push(UserProvider);

const MasterProvider = providers.master();

export default MasterProvider;

export { Context, UserContext };
