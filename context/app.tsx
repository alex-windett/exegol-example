import React, { useState, createContext } from "react";
import graphqlClient from "@Utils/graphqlClient";
import gql from "graphql-tag";
import { GlobalError, AppContext } from "@Types/index.d.ts";
import { IBasketWishlist } from "@Types/basketWishlist";

export const Context = createContext<any>(null);

const ContextProvier = ({ children }: any) => {
  const [globalErrors, setGlobalErrors] = useState<GlobalError[]>([]);
  const [miniBasket, setMiniBasket] = useState<IBasketWishlist>({});
  const [miniBasketOpen, setMiniBasketOpen] = useState(false);
  const [basketID, setBasketID] = useState("");

  const handleMiniBasketOpen = async ({
    forceRefresh = true,
    basketID = "",
  }: {
    forceRefresh?: boolean;
    basketID?: string;
  }) => {
    if (miniBasket && !forceRefresh) return;

    await setMiniBasket({ loading: true });
    try {
      /**
       * TODO: Move query somewhere else,
       */
      const { data } = await graphqlClient.query({
        query: gql`
          {
            basket(id: ${basketID}) {
              count
              guid
              totalPrice {
                value
                formattedValue
              }
              subTotalPrice {
                value
                formattedValue
              }
              deliveryMode {
                price {
                  formattedValue
                }
              }
              items {
                code
                name
                count
                size {
                  code
                  sanitizedName
                }
                color {
                  sanitizedName
                  code
                  cmsComponents {
                    items {
                      manequinImagesCollection {
                        items {
                          url
                          title
                        }
                      }
                    }
                  }
                }
                price {
                  value
                  formattedValue
                }
                wasPrice {
                  value
                  formattedValue
                }
                totalPrice {
                  value
                  formattedValue
                }
              }
            }

            wishlist(id: "asdasd") {
              guid
              count
              subTotalPrice {
                value
                formattedValue
              }
              items {
                code
                name
                count
              }
              items {
                code
                name
                count
                size {
                  sanitizedName
                }
                color {
                  sanitizedName
                }
                price {
                  value
                  formattedValue
                }
                wasPrice {
                  value
                  formattedValue
                }
                totalPrice {
                  value
                  formattedValue
                }
              }
            }
          }
        `,
      });

      await setMiniBasket(data);
      return data;
    } catch (error) {
      await setMiniBasket({ error });
    } finally {
      await setMiniBasket((prev) => ({ ...prev, loading: false }));
    }
  };

  const value: AppContext = {
    miniBasket,
    setMiniBasket: handleMiniBasketOpen,
    miniBasketOpen,
    basketID,
    setBasketID,
    setMiniBasketOpen: async (isOpen) => {
      setMiniBasketOpen(isOpen);
      await handleMiniBasketOpen({});
    },
    globalErrors,
    setGlobalErrors: (error: string) => {
      const triggeredAt = Date.now();
      const newError = { triggeredAt, error };

      setGlobalErrors([...globalErrors, newError]);

      setTimeout(() => {
        setGlobalErrors(
          globalErrors.filter(
            (error: GlobalError) => error.triggeredAt !== triggeredAt
          )
        );
      }, 3000);
    },
  };

  return <Context.Provider value={value}>{children}</Context.Provider>;
};

export default ContextProvier;
