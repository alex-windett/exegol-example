import React, { useMemo, useEffect, useState, createContext } from "react";
import Amplify, { Auth } from "aws-amplify";

import config from "../config";

Amplify.configure({ ...config.Auth, ssr: true });

export const UserContext = createContext<any>(null);

const UserProvider = ({ children }: any) => {
  const [user, setUser] = useState(null);

  useEffect(() => {
    Auth.currentAuthenticatedUser()
      .then((user) => setUser(user))
      .catch(() => setUser(null));
  }, []);

  const values = useMemo(() => ({ user }), [user]);

  return <UserContext.Provider value={values}>{children}</UserContext.Provider>;
};

export default UserProvider;
