export default {
  apiGateway: {
    REGION: "eu-west-1",
    URL: "https://sek121t6l6.execute-api.eu-west-1.amazonaws.com/dev/customer",
  },
  Auth: {
    mandatorySignIn: true,
    region: "eu-west-1",
    userPoolId: "eu-west-1_sFCu5Ph0l",
    userPoolWebClientId: "1qvamp9cau41m2g06jlnqp9l3p",
    authenticationFlowType: "USER_PASSWORD_AUTH",
  },
  social: {
    FB: "325529841810070",
  },
  oauth: {
    domain: "https://raphausers.auth.eu-west-1.amazoncognito.com",
    scope: ["email", "openid"],
    redirectSignIn: "http://localhost:3000/",
    redirectSignOut: "http://localhost:3000/",
    responseType: "code",
  },
};
