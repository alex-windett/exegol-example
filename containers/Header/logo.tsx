import React from "react";
import styled from "styled-components";
import { mq } from "@rapharacing/tokens";
import UI from "@rapharacing/rapha-ui";
import { Link } from "@WithTranslations";

const { SVG } = UI;

const { Rapha } = SVG;

const Container = styled.div`
  grid-column: 3 / span 2;
  display: flex;
  align-items: center;
  justify-content: center;

  svg {
    height: 38px;
    max-width: 100%;
    width: 82px;
  }

  ${mq.md`
    grid-column: 6 / span 2;
  `}
`;

const Logo = () => (
  <Container>
    <Link href="/" passHref>
      <a>
        <Rapha />
      </a>
    </Link>
  </Container>
);

export default Logo;
