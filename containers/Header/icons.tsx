import React from "react";
import styled, { css } from "styled-components";
import tokens, { dim, mq } from "@rapharacing/tokens";
import UI from "@rapharacing/rapha-ui";

const { SVG, Typography } = UI;

const { AccountIcon, BasketIcon, SearchIcon } = SVG;
const { Label } = Typography;

export const Container = styled.div`
  grid-column: 5 / span 2;
  display: flex;
  align-items: center;
  justify-content: flex-end;

  ${mq.md`
    grid-column: 11 / span 2;
  `}
`;

const Icon = styled.button`
  appearance: none;
  background: transparent;
  border: none;
  height: ${dim(2)};
  margin-right: ${dim(4)};
  width: ${dim(2)};

  &:hover {
    cursor: pointer;
    svg {
      .editStroke {
        stroke: ${tokens.color("pink")};
      }

      .editFill {
        fill: ${tokens.color("pink")};
      }
    }
  }

  &:last-of-type {
    margin-right: 0;
  }
`;

export const Search = styled.button`
  appearance: none;
  background: transparent;
  border: 0;
  cursor: pointer;
  height: ${dim(2)};
  margin-right: ${dim(4)};
  outline: none;
  padding: 0;
  width: ${dim(2)};

  &:hover {
    cursor: pointer;
    svg .editStroke {
      stroke: ${tokens.color("pink")};
    }
  }

  &:only-child {
    margin-right: 0;
  }
`;

const Basket = styled(Icon)<{ count: number | undefined }>`
  display: flex;
  justify-content: space-between;

  ${({ count = 0 }) =>
    count > 0 &&
    css`
      svg {
        .editStroke {
          stroke: ${tokens.color("pink")};
        }

        .editFill {
          fill: ${tokens.color("pink")};
        }
      }
    `}
`;

const BasketCount = styled(Label)`
  color: ${tokens.color("pink")};
  margin: 0;
  min-width: ${dim(3)};
`;

interface IProps {
  isDesktop: boolean;
  inView: boolean;
  onSearchClick?: () => void;
  onAccountClick?: () => void;
  onBasketClick?: () => void;
  basketCount?: number;
}

const Icons = ({
  isDesktop,
  inView,
  onSearchClick = () => null,
  onAccountClick = () => null,
  onBasketClick = () => null,
  basketCount = 0,
}: IProps) => {
  return (
    <Container>
      {isDesktop && (
        <Search onClick={onSearchClick} title="Search">
          <SearchIcon />
        </Search>
      )}
      {!inView && (
        <Icon onClick={onAccountClick} title="Account">
          <AccountIcon />
        </Icon>
      )}
      {!inView && (
        <Basket onClick={onBasketClick} title="Basket" count={basketCount}>
          <div>
            <BasketIcon />
          </div>
          {basketCount > 0 && <BasketCount>{basketCount}</BasketCount>}
        </Basket>
      )}
    </Container>
  );
};

export default Icons;
