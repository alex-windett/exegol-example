import React from "react";
import WindowSizeListener from "react-window-size-listener";
import { Transition } from "react-transition-group";
import styled from "styled-components";

import tokens from "@rapharacing/tokens";

import Content from "./content";
import Overlay from "@Components/Overlay";

import { HeaderContext } from "./";

const Div = styled.div`
  position: relative;
  z-index: 0;
`;

const Header = (props: any) => {
  // @ts-ignore
  const { navOpen, desktopNav, setdesktopNav, setNavOpen } = React.useContext(
    HeaderContext
  );

  const mouseOverOverlay = () => {
    setTimeout(() => {
      setNavOpen(false);
    }, 300);
  };

  return (
    <Div>
      <WindowSizeListener
        onResize={(windowSize) =>
          setdesktopNav(windowSize.windowWidth > tokens.get("breakpoints.md"))
        }
      >
        <Content desktopNav={desktopNav} navOpen={navOpen} {...props} />
        <Transition timeout={900} in={navOpen}>
          {(state) => (
            <Overlay
              className={`overlay-${state}`}
              onMouseOver={mouseOverOverlay}
            />
          )}
        </Transition>
      </WindowSizeListener>
    </Div>
  );
};

export default Header;
