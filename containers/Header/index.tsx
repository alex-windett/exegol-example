import React, { createContext, useState } from "react";
import { INavigationContext } from "@Types/layout";

import Head from "./head";

export const HeaderContext = createContext<INavigationContext | {}>({});

interface ITertiary {
  name: string;
  urlPath: string;
}

interface ISecondary {
  name: string;
  urlPath: string;
  menuItemsCollection: {
    items: ITertiary[];
  };
}

interface IPrimary {
  name: string;
  secondaryNavigationCollection: {
    items: ISecondary[];
  };
}

interface INavigation {
   menuCollection: {
        items: IPrimary[];
      };
}
interface IProps {
  onBasketClick: () => void;
  basketCount: number | undefined;
  data: {
    navigation: INavigation;
    mobileNavigation: INavigation;
  };
}

const Header = ({ data, ...props }: IProps) => {
  const [navOpen, setNavOpen] = useState(false);
  const [desktopNav, setdesktopNav] = useState(false);

  const navigationData = desktopNav
    ? data.navigation
    : data.mobileNavigation || data.navigation;

  const navigation = navigationData
    ? navigationData.menuCollection.items.map((x: IPrimary) => ({
        title: x.name,
        items:
          x.secondaryNavigationCollection &&
          x.secondaryNavigationCollection.items.map((y: ISecondary) => ({
            title: y.name,
            url: y.urlPath,
            items:
              y.menuItemsCollection &&
              y.menuItemsCollection.items.map((z: ITertiary) => ({
                title: z.name,
                url: z.urlPath,
              })),
          })),
      }))
    : [];

  return (
    <HeaderContext.Provider
      value={{
        navOpen: navOpen,
        desktopNav: desktopNav,
        navigation,
        setNavOpen: (bool: boolean) => setNavOpen(bool),
        setdesktopNav: (bool: boolean) => setdesktopNav(bool),
      }}
    >
      <Head {...props} />
    </HeaderContext.Provider>
  );
};

export default Header;
