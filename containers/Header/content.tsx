import styled from "styled-components";
import tokens, { dim, mq } from "@rapharacing/tokens";

import Icons, { Search } from "./icons";
import Logo from "./logo";
import Navigation from "@Components/Header/Navigation";

import UI from "@rapharacing/rapha-ui";

const { SVG } = UI;

const { SearchIcon } = SVG;

const Grid = styled.div`
  align-items: center;
  background-color: ${tokens.color("white")};
  display: grid;
  grid-column-gap: ${dim(2)};
  grid-template-columns: repeat(6, 1fr);
  height: ${dim(8)};
  padding: 0 ${dim(2)};
  position: relative;
  z-index: 2;

  ${mq.md`
    grid-column-gap: ${dim(4)};
    grid-template-columns: repeat(12, 1fr);
    padding: 0 ${dim(4)};
  `}
`;

const SearchOuter = styled(Search)`
  grid-column: 2 / span 1;

  &:last-child {
    margin-right: 0;
  }
`;

interface IProps {
  desktopNav: boolean;
  navOpen: boolean;
}

const Content = ({ desktopNav, navOpen, ...props }: IProps) => (
  <Grid>
    <Navigation />
    {!desktopNav && !navOpen && (
      <SearchOuter tabIndex={0}>
        <SearchIcon />
      </SearchOuter>
    )}
    <Logo />
    <Icons isDesktop={desktopNav} inView={navOpen} {...props} />
  </Grid>
);

export default Content;
