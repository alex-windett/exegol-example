import React, { useEffect, useRef, useState } from "react";
import { renderToString } from "react-dom/server";
import mapboxgl from "mapbox-gl";
import { IMapLocation, IMapMarker } from "@Types/clubhouses";
import { CSSProperties } from "styled-components";

const defaultMaker = {
  url: "https://www.rapha.cc/_ui/build/images/clubhouse-pin.svg",
  width: "30px",
  height: "35px",
};

const style: CSSProperties = {
  width: "100%",
  height: "100%",
  position: "absolute",
};

interface IProps {
  center?: [number, number];
  zoom?: number;
  mapStyle?: string;
  locations: IMapLocation[];
  showControls?: boolean;
}

const MapboxGLMap = ({
  center = [0, 0],
  zoom = 5,
  mapStyle = "mapbox://styles/rapha/cjrhp0dqp3nah2spcz7qsw64s",
  locations = [],
  showControls = true,
}: IProps) => {
  const [map, setMap] = useState(null);
  const mapContainer = useRef<HTMLElement | null>(null);

  useEffect(() => {
    if (!process.env.MAPBOX_TOKEN) {
      console.log("Locations map missing access token");
      return;
    }

    mapboxgl.accessToken = process.env.MAPBOX_TOKEN;

    const initializeMap = ({ setMap, mapContainer }: any) => {
      const map = new mapboxgl.Map({
        center,
        container: mapContainer.current,
        style: mapStyle,
        zoom,
      });

      map.on("load", () => {
        setMap(map);

        map.resize();

        if (showControls) {
          map.addControl(new mapboxgl.NavigationControl());
        }

        locations.forEach((location: IMapLocation) => {
          let el = document.createElement("div");

          el.className = "marker";

          const marker: IMapMarker = { ...defaultMaker, ...location.marker };

          el.style.background = `url(${marker.url}) no-repeat`;

          if (marker.width) el.style.width = marker.width;
          if (marker.height) el.style.height = marker.height;
          if (location.popup) el.style.cursor = "pointer";

          el.addEventListener("click", () => {
            if (location.popup) {
              new mapboxgl.Popup({
                closeOnClick: false,
                closeOnMove: true,
              })
                .setLngLat(
                  location.popup.coords || [
                    location.coords[0],
                    location.coords[1],
                  ]
                )
                .setHTML(renderToString(location.popup.html()))
                .addTo(map);
            }
          });

          new mapboxgl.Marker(el).setLngLat(location.coords).addTo(map);
        });
      });
    };

    if (!map) initializeMap({ setMap, mapContainer });
  }, [map]);

  return <div ref={(el: any) => (mapContainer.current = el)} style={style} />;
};

export default MapboxGLMap;
