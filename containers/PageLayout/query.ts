import gql from "fake-tag";

const navigation = gql`
  entryTitle
  menuCollection(limit: 5) {
    items {
      __typename
      ... on PrimaryNavigation {
        entryTitle
        name
        secondaryNavigationCollection(limit: 10) {
          items { 
            name
            urlPath
            menuItemsCollection(limit: 10) {
              items {
                name
                urlPath
                styles
              }
            }
          }
        }
      }
    }
  }
`;

export default gql`
  query getLayout($preview: Boolean, $locale: String) {
    # Footer Nodes
    footer {
      legalNotice
      # navigation {
      #   name
      #   text
      #   links {
      #     isExternal
      #     linkTarget
      #     title
      #     url
      #   }
      # }
    }

    # Header Details
    header {
      benefitsBar {
        backgroundColor
        textColor
      }
      logo {
        text
        url
      }
    }

    # Basket
    basket(id: "asdsa") {
      guid
      count
    }

    # Navigation Nodes
    navigation(id: "${process.env.NAVIGATION_ID}", locale: $locale, preview: $preview) {
      ${navigation}
    }

    mobileNavigation: navigation(id: "${process.env.MOBILE_NAVIGATION_ID}", locale: $locale, preview: $preview) {
     ${navigation}
    } 
  }
`;
