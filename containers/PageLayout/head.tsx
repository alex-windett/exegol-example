import Head from "next/head";
import { getCurrentURL } from "@Utils/index";
import { SingletonRouter } from "next/router";
import { HEAD } from "@Utils/constants";

const RaphaHead = ({ router }: { router: SingletonRouter }) => (
  <Head>
    <link key="preconnect" rel="preconnect" href={process.env.HOST} />
    <link rel="canonical" href={process.env.HOST} />
    <meta key="themeColor" name="theme-color" content="#f67599" />
    <link
      rel="stylesheet/css"
      href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css"
    ></link>

    <meta
      key="appleStatusBar"
      name="apple-mobile-web-app-status-bar-style"
      content="black-translucent"
    />
    <link key="manifest" rel="manifest" href={"/manifest.json"} />
    <link
      key="appleIcon"
      rel="apple-touch-startup-image"
      href={"/images/logo-32.png"}
    />

    <link
      key="favicon"
      rel="shortcut icon"
      type="image/x-icon"
      media="all"
      href="//dyzmn8020x6cd.cloudfront.net/static/images/favicon.ico"
    />
    <link
      key="favicon-180"
      rel="apple-touch-icon"
      sizes="180x180"
      href="https://i1.adis.ws/i/rapha/apple-touch-icon-180x180-precomposed"
    />
    <link
      key="favicon-120"
      rel="apple-touch-icon"
      sizes="120x120"
      href="https://i1.adis.ws/i/rapha/apple-touch-icon-120x120-precomposed"
    />
    <link
      key="favicon-167"
      rel="apple-touch-icon"
      sizes="167x167"
      href="https://i1.adis.ws/i/rapha/apple-touch-icon-167x167-precomposed"
    />
    <link
      key="favicon-152"
      rel="apple-touch-icon"
      sizes="152x152"
      href="https://i1.adis.ws/i/rapha/apple-touch-icon-152x152-precomposed"
    />
    <title key={HEAD.pageTitle}>
      The World’s Finest Cycling Clothing and Accessories. | Rapha
    </title>
    <meta
      key={HEAD.ogTitle}
      property="og:title"
      content="The World’s Finest Cycling Clothing and Accessories. | Rapha"
    />
    <meta
      key={HEAD.ogURL}
      property="og:url"
      content={getCurrentURL(router.asPath)}
    />
    <meta
      key={HEAD.ogImage}
      property="og:image"
      content="/images/brand-logo.png"
    />
  </Head>
);

export default RaphaHead;
