import React from "react";
import { withRouter } from "next/router";
import { compose } from "recompose";
import UI from "@rapharacing/rapha-ui";
import styled from "styled-components";
import { dim } from "@rapharacing/token";
import { Transition } from "react-transition-group";
import { ReactNode } from "react";
import { SingletonRouter } from "next/router";

import query from "./query";

import { AppContext } from "@Types/index";
import { i18n, languages } from "@Server/withI18n";
import Head from "./head";
import MiniBasket from "@Containers/MiniBasket";
import withGraphqlData from "@Hocs/withApollo";
import Header from "@Containers/Header";
import withContext from "@Hocs/withContext";

interface ILayout {
  background?: false | string;
  pageTitle?: any;
  data: any;
  router: SingletonRouter;
  appContext: AppContext;
  children?: ReactNode;
  t: any;
}

const { Footer, BenefitsBar, Typography, Dropdown } = UI;

const Main = styled.main<{ background: false | string }>`
  position: relative;
  background-color: ${({ background = "transparent" }) => background};
`;

const Errors = styled.section`
  position: fixed;
  top: 0;
  left: 0;
  background-color: black;
  width: 100vw;
  padding: ${dim(2)} 0;
  z-index: 10;
`;

const Error = styled(Typography.Body)`
  color: white;
  text-align: center;
`;

const Layout: React.FC<ILayout> = ({
  children,
  background = "",
  data,
  router,
  appContext,
}) => {
  const handleLanguageChange = (
    event: React.ChangeEvent<HTMLSelectElement>
  ) => {
    i18n.changeLanguage(event.target.value);
  };

  const onBasketClick = async () => {
    appContext.setMiniBasketOpen(true);

    await appContext.setMiniBasket({
      forceRefresh: !appContext.miniBasket,
    });
  };

  return (
    <>
      <Head router={router} />

      <Main background={background}>
        {data.header && <BenefitsBar {...data.header.benefitsBar} />}
        {appContext.globalErrors && (
          <Errors>
            {appContext.globalErrors.map((e: any, i: number) => (
              <Error variant="sml" key={i}>
                {e.error}
              </Error>
            ))}
          </Errors>
        )}
        <Transition in={!appContext.miniBasketOpen} timeout={600}>
          {(state) => (
            <MiniBasket
              transition={state}
              setminiBasketOpen={appContext.setMiniBasketOpen}
              data={appContext.miniBasket}
              miniBasketOpen={appContext.miniBasketOpen}
            />
          )}
        </Transition>

        <Header
          // handleLogout={handleLogout}
          data={data}
          onBasketClick={onBasketClick}
          basketCount={data.basket && data.basket.count}
        />
        <>
          {children}

          <p>change language to: </p>
          <Dropdown onChange={handleLanguageChange} defaultValue={"DEFAULT"}>
            <option disabled value="DEFAULT">
              Select a language
            </option>
            {Object.keys(languages).map((language: string) => (
              <option key={language} value={language}>
                {language}
              </option>
            ))}
          </Dropdown>
        </>
        <Footer {...data.footer} />
      </Main>
    </>
  );
};

const enhance = compose(
  withRouter,
  withContext,
  withGraphqlData({
    query,
  })
);

//@ts-ignore
export default enhance(Layout);
