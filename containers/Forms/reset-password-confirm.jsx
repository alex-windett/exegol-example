import compose from "recompose/compose";

// HOCS
import { withFormik } from "formik";
import { withSSRContext } from "aws-amplify";
import { withRouter } from "next/router";
import withContext from "@Hocs/withContext";

// Router
import { Router } from "@Server/withI18n";

// Validation
import * as Yup from "yup";
import { FORM_VALIDATION } from "@Utils/constants";

// UI
import ui from "@rapharacing/rapha-ui";
const { Button, Input } = ui;
import { Submit } from "./ui";

const formik = {
  displayName: "ResetPasswordConfirmForm",
  validationSchema: Yup.object({
    email: FORM_VALIDATION.EMAIL({ required: true }),
    code: Yup.number().required(),
    password: FORM_VALIDATION.PASSWORD({ required: true }),
    confirmPassword: Yup.string()
      .required("Please confirm your password")
      .oneOf([Yup.ref("password"), null], "Your passwords do not match"),
  }),
  mapPropsToValues: ({ router }) => ({
    email: router.query.email,
    password: "",
    confirmPassword: "",
    code: "",
  }),
  handleSubmit: async (values, { props, setSubmitting }) => {
    setSubmitting(true);

    const { appContext, router } = props;
    const { confirm_password, ...value } = values;

    try {
      await Auth.confirmResetPassword(value);

      await appContext.setGlobalErrors("Password reset, returing to login");

      setTimeout(() => {
        if (router.query.email) {
          Router.push(`/login?email=${router.query.email}`);
        } else {
          Router.push("/login");
        }
      }, 2000);
    } catch (error) {
      console.error(error);
      await appContext.setGlobalErrors(error.message);
    } finally {
      setSubmitting(false);
    }
  },
};

const ResetPasswordConfirmForm = ({
  errors,
  handleBlur,
  handleChange,
  handleSubmit,
  isSubmitting,
  isValid,
  touched,
  values,
}) => (
  <form onSubmit={handleSubmit}>
    <Input
      aria-label="Email"
      aria-required="true"
      autoComplete="email"
      id="email"
      label="Email"
      name="email"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="Email"
      type="text"
      value={values.email}
    />
    {touched.email && errors.email && <div>{errors.email}</div>}

    <Input
      aria-label="Code"
      aria-required="true"
      id="code"
      label="Code"
      name="code"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="Code"
      type="text"
      value={values.code}
    />
    {touched.code && errors.code && <div>{errors.code}</div>}

    <Input
      aria-label="Password"
      aria-required="true"
      autoComplete="new-password"
      id="password"
      label="Password"
      name="password"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="Password"
      type="password"
      value={values.password}
    />
    {touched.password && errors.password && <div>{errors.password}</div>}

    <Input
      aria-label="Confirm Password"
      aria-required="true"
      autoComplete="new-password"
      id="confirmPassword"
      label="Confirm Password"
      name="confirmPassword"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="Confirm Password"
      type="confirmPassword"
      value={values.confirmPassword}
    />
    {touched.confirmPassword && errors.confirmPassword && (
      <div>{errors.confirmPassword}</div>
    )}

    <Submit>
      <Button
        variant="primary"
        type="submit"
        // disabled={!isValid || isSubmitting}
        // data-test-id="login-submit-button"
      >
        Reset Password
      </Button>
    </Submit>
  </form>
);

const enhance = compose(withRouter, withContext, withFormik(formik));

export default enhance(ResetPasswordConfirmForm);
