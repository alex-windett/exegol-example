import compose from "recompose/compose";

// HOCS
import { withFormik } from "formik";
import { withSSRContext } from "aws-amplify";
import withContext from "@Hocs/withContext";

// Router
import { Router } from "@Server/withI18n";
import { withRouter } from "next/router";

// Validation
import * as Yup from "yup";
import { FORM_VALIDATION } from "@Utils/constants";

// UI
import ui from "@rapharacing/rapha-ui";
const { Button, Input } = ui;
import { Submit } from "./ui";

// Makes Login SSR aware
const { Auth } = withSSRContext();

const formik = {
  displayName: "ResetPasswordForm",
  validationSchema: Yup.object({
    email: FORM_VALIDATION.EMAIL({ required: true }),
  }),
  mapPropsToValues: ({ router }) => ({
    email: router.query.email,
  }),
  handleSubmit: async (values, { props, setSubmitting }) => {
    setSubmitting(true);

    try {
      const { email } = values;

      await Auth.resetPassword(email);

      Router.push(`/password/reset/confirm?email=${email}`);
    } catch (error) {
      const { appContext } = props;

      console.error(error);
      await props.appContext.setGlobalErrors(error.message);
    } finally {
      setSubmitting(false);
    }
  },
};

const ResetPasswordForm = ({
  errors,
  handleBlur,
  handleChange,
  handleSubmit,
  isSubmitting,
  isValid,
  touched,
  values,
}) => (
  <form onSubmit={handleSubmit}>
    <Input
      aria-label="Email"
      aria-required="true"
      autoComplete="email"
      id="email"
      label="Email"
      name="email"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="Email"
      type="text"
      value={values.email}
    />
    {touched.email && errors.email && <div>{errors.email}</div>}

    <Submit>
      <Button
        variant="primary"
        type="submit"
        // disabled={!isValid || isSubmitting}
        // data-test-id="login-submit-button"
      >
        Reset Password
      </Button>
    </Submit>
  </form>
);

const enhance = compose(withRouter, withContext, withFormik(formik));

export default enhance(ResetPasswordForm);
