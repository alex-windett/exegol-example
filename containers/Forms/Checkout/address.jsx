import { compose } from "recompose";
import axios from "axios";
import qs from "query-string";

// HOCS
import { withFormik } from "formik";
import withContext from "@Hocs/withContext";

// Validation
import * as Yup from "yup";

// UI
import UI from "@rapharacing/rapha-ui";
const { Typography, Button } = UI;

import Checkbox from "@Components/Checkout/FormElements/Checkbox";
import Dropdown from "@Components/Checkout/FormElements/Dropdown";
import Input from "@Components/Checkout/FormElements/Input";

import { AddressContainer, FieldGroup } from "@Components/Checkout/Address/ui";

const options = [
  { value: "mr", label: "Mr" },
  { value: "mrs", label: "Mrs" },
  { value: "miss", label: "Miss" },
  { value: "dr", label: "Dr" },
];

const countryOptions = [
  { isocode: "uk", label: "United Kingdom" },
  { isocode: "us", label: "United States" },
];

const checkOptions = [
  {
    title: "Keep me up to date with news and exclusive offers",
    value: false,
  },
];

const formik = {
  displayName: "AddressForm",
  validationSchema: Yup.object({
    titleCode: Yup.string().required("Title is required"),
    firstName: Yup.string()
      .required("First Name is required")
      .max(80, "Maximum 80 characters")
      .matches(/^[a-z]*$/i, "Only latin characters"),
    lastName: Yup.string()
      .required("Last Name is required")
      .max(80, "Maximum 80 characters")
      .matches(/^[a-z]*$/i, "Only latin characters"),
    email: Yup.string()
      .required("Email address is required")
      .email("Invalid email"),
    cellphone: Yup.string()
      .required("Phone number is required")
      .matches(/^[0-9\s]*$/, "Only digits and spaces allowed")
      .min(7, "Phone number too short")
      .max(20, "Phone number too long"),
    postcode_lookup: Yup.string().max(8, "Maximum 8 characters"),
    companyName: Yup.string()
      .max(80, "Maximum 80 characters")
      .matches(/^[a-z]*$/i, "Only latin characters"),
    line1: Yup.string()
      .required("This field is required")
      .matches(/^[\w\-\s]+$/, "Only latin characters")
      .max(120, "Maximum 120 characters"),
    line2: Yup.string()
      .max(100, "Maximum 100 characters")
      .matches(/^[\w\-\s]+$/, "Only latin characters"),
    town: Yup.string()
      .required("Town/City is required")
      .max(100, "Maximum 100 characters")
      .matches(/^[a-z]*$/i, "Only latin characters"),
    postalCode: Yup.string()
      .required("Postcode is required")
      .max(8, "Maximum 8 characters")
      .matches(/^[\w\-\s]+$/, "Alphanumeric only"),
    country: Yup.string().required("Country is required"),
  }),
  mapPropsToValues: () => ({
    titleCode: "",
    firstName: "",
    lastName: "",
    email: "",
    cellphone: "",
    postcode_lookup: "",
    companyName: "",
    line1: "",
    line2: "",
    town: "",
    postalCode: "",
    country: "",
    marketing: "",
  }),
  handleSubmit: async (values, { props, setSubmitting }) => {
    const { appContext } = props;

    setSubmitting(true);

    try {
      const {
        country,
        marketing,
        postcode_lookup,
        titleCode,
        ...rest
      } = values;

      const formattedData = Object.assign(
        {
          titleCode: values.titleCode.value,
          country: values.country.isocode,
          region: "US-NY",
          shippingAddress: true,
          visibleInAddressBook: true,
          defaultAddress: true,
          district: values.town,
          formattedAddress: `${values.line1} ${values.line2} ${values.postalCode} ${values.town}`,
          phone: values.cellphone,
        },
        rest
      );

      const queryString = qs.stringify(formattedData);

      await axios({
        method: "get",
        url: `/api/delivery-address?address=${queryString}`,
      });

      await appContext.setGlobalErrors(
        "Your address has been successfully submit"
      );
    } catch (error) {
      console.error("error", error, values);
      await appContext.setGlobalErrors(error);
    } finally {
      setSubmitting(false);
    }
  },
};

const Address = ({
  errors,
  handleBlur,
  handleChange,
  handleSubmit,
  isSubmitting,
  isValid,
  touched,
  values,
}) => (
  <AddressContainer>
    <Typography.H4>Shipping Details</Typography.H4>
    <form onSubmit={handleSubmit} noValidate>
      <FieldGroup fields={2}>
        <Input
          label="Email"
          id="email"
          name="email"
          type="email"
          required={true}
        />
        <Input
          label="Phone Number"
          id="cellphone"
          name="cellphone"
          type="text"
          required={true}
        />
        <Checkbox
          id="marketing"
          name="marketing"
          options={checkOptions}
          text="Keep me up to date with news and exclusive offers"
        />
      </FieldGroup>
      <FieldGroup fields={3}>
        <Dropdown
          label="Title"
          id="titleCode"
          name="titleCode"
          type="text"
          options={options}
          required={true}
        />
        <Input
          label="First Name"
          id="firstName"
          name="firstName"
          type="text"
          required={true}
        />
        <Input
          label="Last Name"
          id="lastName"
          name="lastName"
          type="text"
          required={true}
        />
      </FieldGroup>
      <FieldGroup fields={1}>
        <Input
          label="Postcode Lookup"
          id="postcode_lookup"
          name="postcode_lookup"
          type="text"
        />
      </FieldGroup>
      <FieldGroup fields={2}>
        <Input
          label="Company Name"
          id="companyName"
          name="companyName"
          type="text"
        />
        <Input
          label="Address Line 1"
          id="line1"
          name="line1"
          type="text"
          required={true}
        />
      </FieldGroup>
      <FieldGroup fields={2}>
        <Input label="Address Line 2" id="line2" name="line2" type="text" />
        <Input
          label="Town/City"
          id="town"
          name="town"
          type="text"
          required={true}
        />
      </FieldGroup>
      <FieldGroup fields={2}>
        <Input
          label="Postcode"
          id="postalCode"
          name="postalCode"
          type="text"
          required={true}
        />
        <Dropdown
          label="Country"
          id="country"
          name="country"
          type="text"
          options={countryOptions}
          required={true}
        />
      </FieldGroup>
      <Button disabled={!isValid} type="submit">
        Submit
      </Button>
    </form>
  </AddressContainer>
);

const enhance = compose(withContext, withFormik(formik));

export default enhance(Address);
