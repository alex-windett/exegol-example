import React, { Fragment } from "react";
import compose from "recompose/compose";

// HOCS
import { withFormik } from "formik";
import { withSSRContext } from "aws-amplify";
import { withRouter } from "next/router";
import withContext from "@Hocs/withContext";

// Router
import { Router } from "@Server/withI18n";
import { useRouter } from "next/router";

// Validation
import * as Yup from "yup";
import { FORM_VALIDATION } from "@Utils/constants";

// UI
import ui from "@rapharacing/rapha-ui";
const { Button, Input, Typography } = ui;
import { Submit } from "./ui";

// Makes Login SSR aware
const { Auth } = withSSRContext();

const formik = {
  displayName: "LoginForm",
  validationSchema: Yup.object({
    email: FORM_VALIDATION.EMAIL({ required: true }),
    password: FORM_VALIDATION.PASSWORD({ required: true }),
  }),
  mapPropsToValues: ({ router }) => ({
    email: "",
    password: "",
  }),
  handleSubmit: async (values, { props, setSubmitting }) => {
    setSubmitting(true);

    try {
      const { email, password } = values;

      await Auth.signIn(email, password);

      Router.push("/account");
    } catch (error) {
      const { appContext } = props;

      console.error(error);
      await appContext.setGlobalErrors(error.message);
    } finally {
      setSubmitting(false);
    }
  },
};

const LoginForm = ({
  errors,
  handleBlur,
  handleChange,
  handleSubmit,
  isSubmitting,
  isValid,
  touched,
  values,
}) => (
  <Fragment>
    <form onSubmit={handleSubmit}>
      <Input
        aria-label="Email"
        aria-required="true"
        autoComplete="email"
        id="email"
        label="Email"
        name="email"
        onBlur={handleBlur}
        onChange={handleChange}
        placeholder="Email"
        type="email"
        value={values.email}
      />
      {touched.email && errors.email && <div>{errors.email}</div>}

      <Input
        aria-label="Password"
        aria-required="true"
        autoComplete="current-password"
        id="password"
        label="Password"
        name="password"
        onBlur={handleBlur}
        onChange={handleChange}
        placeholder="Password"
        type="password"
        value={values.password}
      />
      {touched.password && errors.password && <div>{errors.password}</div>}

      <Submit>
        <Button
          variant="primary"
          type="submit"
          // disabled={!isValid | isSubmitting}
          // data-test-id="login-submit-button"
        >
          Login
        </Button>
      </Submit>
    </form>

    {/* <div>
      <Link href="/password/reset" passHref>
        <Typography.Anchor>Forgot Password?</Typography.Anchor>
      </Link>
    </div> */}
  </Fragment>
);

// Combine multiple HOC
const enhance = compose(withRouter, withContext, withFormik(formik));

export default enhance(LoginForm);
