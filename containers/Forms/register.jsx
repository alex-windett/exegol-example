import styled from "styled-components";
import compose from "recompose/compose";

// HOCS
import Router, { withRouter } from "next/router";
import { withFormik } from "formik";
import { withSSRContext } from "aws-amplify";
import withContext from "@Hocs/withContext";

// Validation
import * as Yup from "yup";
import { FORM_VALIDATION } from "@Utils/constants";

// UI
import ui from "@rapharacing/rapha-ui";
const { Button, Input } = ui;
import { Submit } from "./ui";

// Makes Login SSR aware
const { Auth } = withSSRContext();

const formik = {
  displayName: "RegisterForm",
  validationSchema: Yup.object().shape({
    email: FORM_VALIDATION.EMAIL({ required: true }),
    firstName: Yup.string().required("First Name is required"),
    lastName: Yup.string().required("Last Name is required"),
    password: FORM_VALIDATION.PASSWORD({ required: true }),
    confirmPassword: Yup.string()
      .required("Please confirm your password")
      .oneOf([Yup.ref("password"), null], "Your passwords do not match"),
  }),
  mapPropsToValues: () => ({
    email: "",
    firstName: "",
    lastName: "",
    password: "",
    confirmPassowrd: "",
  }),
  handleSubmit: async (values, { props, setSubmitting }) => {
    setSubmitting(true);

    try {
      const { email, password, firstName, lastName } = values;

      const user = await Auth.signUp({
        username: email,
        password,
        attributes: {
          given_name: firstName,
          family_name: lastName,
        },
      });

      Router.push(`/register/confirm?email=${email}`);
    } catch (error) {
      const { appContext } = props;

      console.error(error);
      await appContext.setGlobalErrors(error.message);
    } finally {
      setSubmitting(false);
    }
  },
};

const RegisterForm = ({
  errors,
  handleBlur,
  handleChange,
  handleSubmit,
  isSubmitting,
  isValid,
  touched,
  values,
}) => (
  <form onSubmit={handleSubmit}>
    <Input
      aria-label="Email"
      aria-required="true"
      autoComplete="email"
      id="email"
      label="Email"
      name="email"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="Email"
      type="email"
      value={values.email}
    />
    {touched.email && errors.email && <div>{errors.email}</div>}

    <Input
      aria-label="First Name"
      aria-required="true"
      autoComplete="given-name"
      id="firstName"
      label="First Name"
      name="firstName"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="First Name"
      type="text"
      value={values.firstName}
    />
    {touched.firstName && errors.firstName && <div>{errors.firstName}</div>}

    <Input
      aria-label="Last Name"
      aria-required="true"
      autoComplete="family-name"
      id="lastName"
      label="Last Name"
      name="lastName"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="Last Name"
      type="text"
      value={values.lastName}
    />
    {touched.lastName && errors.lastName && <div>{errors.lastName}</div>}

    <Input
      aria-label="Password"
      aria-required="true"
      autoComplete="new-password"
      id="password"
      label="Password"
      name="password"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="Password"
      type="password"
      value={values.password}
    />
    {touched.password && errors.password && <div>{errors.password}</div>}

    <Input
      aria-label="Confirm Password"
      aria-required="true"
      autoComplete="new-password"
      id="confirmPassword"
      label="Confirm Password"
      name="confirmPassword"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="Confirm Password"
      type="password"
      value={values.confirmPassword}
    />
    {touched.confirmPassword && errors.confirmPassword && (
      <div>{errors.confirmPassword}</div>
    )}

    <Submit>
      <Button
        variant="primary"
        type="submit"
        // disabled={!isValid || isSubmitting}
        // data-test-id="register-submit-button"
      >
        Register
      </Button>
    </Submit>
  </form>
);

// Combine multiple HOC
const enhance = compose(withRouter, withContext, withFormik(formik));

export default enhance(RegisterForm);
