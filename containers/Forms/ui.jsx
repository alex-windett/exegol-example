import styled from "styled-components";

export const Submit = styled.div`
  margin-top: 32px;
  width: 100%;

  button {
    width: 100%;
  }
`;
