import { Fragment } from "react";
import { compose } from "recompose";
import axios from "axios";

// HOCS
import { withFormik } from "formik";
import withContext from "@Hocs/withContext";

// Validation
import * as Yup from "yup";
import { FORM_VALIDATION } from "@Utils/constants";

// UI
import UI from "@rapharacing/rapha-ui";
const { Typography, Input } = UI;
import { Submit } from "../ui";

const formik = {
  displayName: "UpdateEmailForm",
  validationSchema: Yup.object({
    new_email: FORM_VALIDATION.EMAIL({ required: true }),
    confirm_email: Yup.string().oneOf(
      [Yup.ref("new_email"), null],
      "Email address' do not match"
    ),
  }),
  mapPropsToValues: () => ({
    new_email: "",
    confirm_email: "",
  }),
  handleSubmit: async (values, { props, setSubmitting }) => {
    setSubmitting(true);

    const { appContext, isCodeSent, isNewEmailSet } = props;

    try {
      const { new_email } = values;

      await axios({
        method: "get",
        url: `/api/update-email?new_email=${new_email}`,
      });

      isCodeSent(true);
      isNewEmailSet(new_email);
    } catch (error) {
      const { appContext } = props;

      console.error(error);
      await appContext.setGlobalErrors(error);
    } finally {
      setSubmitting(false);
    }
  },
};

const UpdateEmail = ({
  errors,
  handleBlur,
  handleChange,
  handleSubmit,
  isSubmitting,
  isValid,
  touched,
  user,
  values,
}) => (
  <Fragment>
    <Typography.Label style={{ marginTop: 0 }} variant="md">
      Email
    </Typography.Label>
    <Typography.Body variant="sml">{user.email}</Typography.Body>

    <form onSubmit={handleSubmit}>
      <Input
        aria-label="New Email Address"
        aria-required="true"
        id="new_email"
        label="New Email Address"
        name="new_email"
        onBlur={handleBlur}
        onChange={handleChange}
        type="email"
        value={values.new_email}
      />
      {touched.new_email && errors.new_email && <div>{errors.new_email}</div>}

      <Input
        aria-label="Confirm New Email Address"
        aria-required="true"
        id="confirm_email"
        label="Confirm New Email Address"
        name="confirm_email"
        onBlur={handleBlur}
        onChange={handleChange}
        type="email"
        value={values.confirm_email}
      />
      {touched.confirm_email && errors.confirm_email && (
        <div>{errors.confirm_email}</div>
      )}

      <Submit disabled={!isValid} type="submit">
        Update
      </Submit>
    </form>
  </Fragment>
);

const enhance = compose(withContext, withFormik(formik));

export default enhance(UpdateEmail);
