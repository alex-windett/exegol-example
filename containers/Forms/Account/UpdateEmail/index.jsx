import { Fragment, useState } from "react";
import { compose } from "recompose";

// HOCS
import withContext from "@Hocs/withContext";

import UpdateForm from "./update-email";
import ConfirmationForm from "./update-email-confirmation";

const UpdateEmail = ({ user }) => {
  const [codeSent, setCodeSent] = useState(false);
  const [newEmail, setNewEmail] = useState(null);

  return (
    <Fragment>
      {!codeSent ? (
        <UpdateForm
          isCodeSent={(sent) => setCodeSent(sent)}
          isNewEmailSet={(email) => setNewEmail(email)}
          user={user}
        />
      ) : (
        <ConfirmationForm email={newEmail} />
      )}
    </Fragment>
  );
};

const enhance = compose(withContext);

export default enhance(UpdateEmail);
