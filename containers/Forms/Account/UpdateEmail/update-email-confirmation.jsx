import { Fragment, useState } from "react";
import { compose } from "recompose";
import axios from "axios";

// HOCS
import { withSSRContext } from "aws-amplify";
import { withFormik } from "formik";
import withContext from "@Hocs/withContext";

// Router
import { Router } from "@Server/withI18n";

// Validation
import * as Yup from "yup";
import { FORM_VALIDATION } from "@Utils/constants";

// UI
import UI from "@rapharacing/rapha-ui";
const { Typography, Input } = UI;
import { Submit } from "../ui";

const { Auth } = withSSRContext();

const formik = {
  displayName: "UpdateEmailConfirmationForm",
  validationSchema: Yup.object({
    code: Yup.number().required(),
  }),
  mapPropsToValues: () => ({
    code: "",
  }),
  handleSubmit: async (values, { props, setSubmitting }) => {
    setSubmitting(true);

    const { appContext } = props;

    try {
      const { code } = values;

      await axios({
        method: "get",
        url: `/api/update-email-confirm?code=${code}`,
      });

      await Auth.signOut();

      Router.push("/login");
    } catch (error) {
      const { appContext } = props;

      console.error(error);
      await appContext.setGlobalErrors(error.message);
    } finally {
      setSubmitting(false);
    }
  },
};

const UpdateEmail = ({
  email,
  errors,
  handleBlur,
  handleChange,
  handleSubmit,
  isSubmitting,
  isValid,
  touched,
  values,
}) => (
  <Fragment>
    <Typography.Label style={{ marginTop: 0 }} variant="md">
      Verification code
    </Typography.Label>
    <Typography.Body variant="sml">
      Please check your email {email} for the confirmation code.
    </Typography.Body>

    <form onSubmit={handleSubmit}>
      <Input
        aria-label="Verification Code"
        aria-required="true"
        id="code"
        label="Verification Code"
        name="code"
        onBlur={handleBlur}
        onChange={handleChange}
        type="tel"
        value={values.code}
      />
      {touched.code && errors.code && <div>{errors.code}</div>}
      <Submit disabled={!isValid} type="submit">
        Submit Code
      </Submit>
    </form>
  </Fragment>
);

const enhance = compose(withContext, withFormik(formik));

export default enhance(UpdateEmail);
