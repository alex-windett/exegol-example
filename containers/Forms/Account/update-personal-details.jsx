import { compose } from "recompose";

// HOCS
import { withSSRContext } from "aws-amplify";
import { withFormik } from "formik";
import withContext from "@Hocs/withContext";

// Validation
import * as Yup from "yup";
import { FORM_VALIDATION } from "@Utils/constants";

// UI
import UI from "@rapharacing/rapha-ui";
const { Typography, Input } = UI;
import { Submit } from "./ui";

const formik = {
  displayName: "UpdatePersonalDetailsForm",
  validationSchema: Yup.object({
    given_name: Yup.string().required(),
    family_name: Yup.string().required(),
    phone_number: FORM_VALIDATION.TELEPHONE,
  }),
  mapPropsToValues: (props) => ({
    given_name: props.user.given_name,
    family_name: props.user.family_name,
    phone_number: props.user.phone_number,
  }),
  handleSubmit: async (values, { props, setSubmitting }) => {
    setSubmitting(true);

    const { appContext } = props;

    try {
      const user = await Auth.currentAuthenticatedUser();
      await Auth.updateUserAttributes(user, values);

      await appContext.setGlobalErrors("Your details have been updated");
    } catch (error) {
      console.error(error);
      await appContext.setGlobalErrors(error.message);
    } finally {
      setSubmitting(false);
    }
  },
};

const UpdatePassword = ({
  errors,
  handleBlur,
  handleChange,
  handleSubmit,
  isSubmitting,
  isValid,
  touched,
  user,
  values,
}) => (
  <form onSubmit={handleSubmit}>
    <Input
      aria-label="First Name"
      aria-required="true"
      autoComplete="given-name"
      id="firstName"
      label="First Name"
      name="given_name"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="First Name"
      type="text"
      value={values.given_name}
    />
    {touched.given_name && errors.given_name && <div>{errors.given_name}</div>}

    <Input
      aria-label="Last Name"
      aria-required="true"
      autoComplete="family-name"
      id="family_name"
      label="Last Name"
      name="family_name"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="Last Name"
      type="text"
      value={values.family_name}
    />
    {touched.family_name && errors.family_name && (
      <div>{errors.family_name}</div>
    )}

    <Input
      aria-label="Telephone"
      autoComplete="tel"
      id="phone_number"
      label="Telephone"
      name="phone_number"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="Telephone"
      type="telephone"
      value={values.phone_number}
    />
    {touched.phone_number && errors.phone_number && (
      <div>{errors.phone_number}</div>
    )}

    <Typography.Label variant="sml">
      Please include the country code prefix (i.e +44)
    </Typography.Label>

    <Submit disabled={!isValid} type="submit">
      Update
    </Submit>
  </form>
);

const enhance = compose(withContext, withFormik(formik));

export default enhance(UpdatePassword);
