import { compose } from "recompose";

// HOCS
import { withSSRContext } from "aws-amplify";
import { withFormik } from "formik";
import withContext from "@Hocs/withContext";

// Validation
import * as Yup from "yup";
import { FORM_VALIDATION } from "@Utils/constants";

// UI
import UI from "@rapharacing/rapha-ui";
const { Typography, Input } = UI;
import { Submit } from "./ui";

const { Auth } = withSSRContext();

const formik = {
  displayName: "UpdatePasswordForm",
  validationSchema: Yup.object({
    password: FORM_VALIDATION.PASSWORD({ required: true }),
    password_confirm: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Passwords do not match"
    ),
  }),
  mapPropsToValues: () => ({
    password: "",
    password_current: "",
    password_confirm: "",
  }),
  handleSubmit: async (values, { props, setSubmitting }) => {
    setSubmitting(true);

    const { appContext } = props;

    try {
      const { password, password_current } = values;

      const currentUser = await Auth.currentAuthenticatedUser();
      await Auth.changePassword(currentUser, password_current, password);

      await appContext.setGlobalErrors(
        "Your password has been updated, you will now be logged out"
      );
    } catch (err) {
      console.error(err);
      await appContext.setGlobalErrors(err.error.message);
    } finally {
      setSubmitting(false);
    }
  },
};

const UpdatePassword = ({
  errors,
  handleBlur,
  handleChange,
  handleSubmit,
  isSubmitting,
  isValid,
  touched,
  values,
}) => (
  <form onSubmit={handleSubmit}>
    <Input
      aria-label="Current Password"
      aria-required="true"
      autoComplete="password"
      id="password_current"
      label="Current Password"
      name="password_current"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="Current Password"
      type="password"
      value={values.password_current}
    />
    {touched.password_current && errors.password_current && (
      <div>{errors.password_current}</div>
    )}

    <Input
      aria-label="New Password"
      aria-required="true"
      id="password"
      label="New Password"
      name="password"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="New Password"
      type="password"
      value={values.password}
    />
    {touched.password && errors.password && <div>{errors.password}</div>}

    <Input
      aria-label="Confirm Password"
      aria-required="true"
      id="password_confirm"
      label="Confirm Password"
      name="password_confirm"
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder="Confirm Password"
      type="password_confirm"
      value={values.password_confirm}
    />
    {touched.password_confirm && errors.password_confirm && (
      <div>{errors.password_confirm}</div>
    )}

    <Submit disabled={!isValid} type="submit">
      Update
    </Submit>
  </form>
);

const enhance = compose(withContext, withFormik(formik));

export default enhance(UpdatePassword);
