import styled from "styled-components";

import ui from "@rapharacing/rapha-ui";
import { dim } from "@rapharacing/tokens";

const { Button } = ui;

export const Submit = styled(Button)`
  width: 100%;
  margin-top: ${dim(2)};
`;
