import { useEffect, useRef } from "react";
import styled from "styled-components";
import UI from "@rapharacing/rapha-ui";
import { useState } from "react";
import tokens, { dim } from "@rapharacing/tokens";
import { useOutsideClick } from "@Utils/hooks";
import { IBasketItem, IBasketWishlist } from "@Types/basketWishlist";
import BasketContents from "@Components/Basket";
import Summary from "@Components/Header/MiniBasket/Summary";

const { SVG } = UI;

const ASIDE_WIDTH = 470;

const Aside = styled.aside`
  position: fixed;
  z-index: 10;
  top: 0;
  right: 0;
  transition: transform 0.6s cubic-bezier(0.23, 1, 0.32, 1);
  transform: translateX(
    ${({ transition }: { transition: string }) =>
      transition === "entering" || transition === "entered"
        ? ASIDE_WIDTH + 64
        : 0}px
  );

  background-color: ${tokens.color("brand.white")};
  width: ${ASIDE_WIDTH}px;
  height: 100%;
`;

const Overlay = styled.div`
  background-color: rgba(0, 0, 0, 0.3);
  width: 100%;
  height: 100%;
  position: fixed;
  top: 0;
  opacity: ${({ transition }: { transition: string }) =>
    transition === "entering" || transition === "entered" ? 0 : 1};
  transition: opacity 0.6s cubic-bezier(0.23, 1, 0.32, 1);
  z-index: ${({ transition }: { transition: string }) =>
    transition === "entering" || transition === "entered" ? -1 : 1};
`;

const Close = styled.button`
  appearance: none;
  border: none;
  background: none;
  position: absolute;
  top: ${dim(4)};
  right: ${dim(4)};
  z-index: 1;

  &:hover {
    cursor: pointer;
  }
`;

const ContentWrapper = styled.div`
  padding: ${dim(9)} ${dim(4)} ${dim(4)};
`;

interface IProps {
  setminiBasketOpen: (status: boolean) => void;
  transition: string;
  data: IBasketWishlist;
  miniBasketOpen: boolean;
}

const MiniBasket = ({
  transition,
  setminiBasketOpen,
  miniBasketOpen,
  data,
}: IProps) => {
  const basket = data.basket || {};
  const wishlist = data.wishlist || {};
  const [wishlistActive, setwishlistActive] = useState(false);
  const ref = useRef(null);

  useOutsideClick(ref, () => setminiBasketOpen(false));

  const [products, setProducts] = useState<IBasketItem[] | undefined>(
    basket.items
  );

  const hanldeWishlistActive = async () => {
    await setProducts(wishlist && wishlist.items);
    setwishlistActive(true);
  };

  const handleBasketActive = async () => {
    await setProducts(basket && basket.items);
    setwishlistActive(false);
  };

  useEffect(() => setProducts(basket.items), [data]);

  return (
    <>
      <Overlay transition={transition} />

      <Aside
        ref={ref}
        transition={transition}
        aria-hidden={!miniBasketOpen}
        id="mini-basket"
      >
        <Close
          onClick={() => {
            setminiBasketOpen(false);
          }}
          title="Close Mini Basket"
          aria-controls="mini-basket"
          aria-hidden={miniBasketOpen}
        >
          <SVG.CloseIcon />
        </Close>

        <ContentWrapper>
          <BasketContents
            loading={data.loading}
            products={products}
            wishlistActive={wishlistActive}
            basketCount={basket.count}
            wishlistCount={wishlist.count}
            hanldeWishlistActive={hanldeWishlistActive}
            handleBasketActive={handleBasketActive}
          />

          {data.basket && (
            <Summary data={data} wishlistActive={wishlistActive} />
          )}
        </ContentWrapper>
      </Aside>
    </>
  );
};

export default MiniBasket;
