import React from "react";
import { createIssue } from "@Utils/gitlab";

interface IState {
  hasError: boolean;
}

class GlobalErrorBoundary extends React.Component<any, IState> {
  constructor(props: any) {
    super(props);

    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  async componentDidCatch(error: any, info: any) {
    if (process.env.NODE_ENV === "production") {
      await createIssue({ error, info });
    }
  }

  render() {
    if (this.state.hasError && process.env.NODE_ENV === "production") {
      return (
        <div>
          <h1>An error was encountered, it has been logged to GitLab.</h1>
        </div>
      );
    }

    return this.props.children;
  }
}

export default GlobalErrorBoundary;
