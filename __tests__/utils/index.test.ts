import { getCurrentURL } from "../../utils/index";
import mapModelToComponent from "../../utils/mapModelToComponent";

describe("Test utility functions", () => {
  describe("getCurrentURL function", () => {
    test("function to be truthy", () => {
      const url = getCurrentURL("/foo");

      expect(url).toBeTruthy();
    });

    test("function with string `/foo` to return domain + /foo", () => {
      const url = getCurrentURL("/foo");

      expect(url).toBe(`${process.env.PUBLIC_URL}/foo`);
    });

    test("function with string `foo` to return domain + /foo", () => {
      const url = getCurrentURL("foo");

      expect(url).toBe(`${process.env.PUBLIC_URL}/foo`);
    });
  });

  describe("mapModelToComponent function", () => {
    test("returns true", () => {
      const moduleName: string = "body";
      const Comp: string = mapModelToComponent({ moduleName });

      expect(Comp).toBeTruthy();
    });
  });

  describe("formatt product data", () => {
    test("returns correct format", () => {
      const input = {
        code: "RPR01XX",
        name: "RCC Pro Team Lightweight Rain Gilet",
        sanitizedName: "rcc-pro-team-lightweight-rain-gilet-RPR01XX",
        colors: [
          {
            code: "RPR01XXGRY",
            colorName: "Grey",
            price: {
              formattedValue: "£140.00"
            },
            wasPrice: null,
            cmsComponents: {
              items: []
            }
          }
        ]
      };

      const output = {
        code: "RPR01XX",
        name: "RCC Pro Team Lightweight Rain Gilet",
        sanitizedName: "rcc-pro-team-lightweight-rain-gilet-RPR01XX",
        colors: [
          {
            code: "RPR01XXGRY",
            colorName: "Grey",
            price: {
              formattedValue: "£140.00"
            },
            wasPrice: null,
            cmsComponents: {
              items: []
            },
            mannequins: []
          }
        ]
      };

      expect(input).toMatchObject(output);
    });

    test("returns correct format with contentful items", () => {
      const input = {
        code: "FLY06XX",
        name: "Classic Flyweight Jersey",
        sanitizedName: "classic-flyweight-jersey-FLY06XX",
        colors: [
          {
            code: "FLY06XXBLK",
            colorName: "Black",
            price: {
              formattedValue: "£100.00"
            },
            wasPrice: null,
            cmsComponents: {
              items: [
                {
                  swatch: "black",
                  sku: "FLY06XXBLK",
                  marketingMessage: "New Season",
                  manequinImagesCollection: {
                    items: [
                      {
                        url:
                          "https://images.ctfassets.net/1stkoghspd4h/Nlc6eVQvIyqEoaCNdEIRm/d22ea5377c4a3204ea8eb078f3dea573/FLY01XX_BLK_10808Mannequin",
                        title: "FLY01XX BLK 10808Mannequin"
                      },
                      {
                        url:
                          "https://images.ctfassets.net/1stkoghspd4h/75N1IKcBQzLivKts8dJU00/c6219238e4d91a78a1535161f493315f/FLY01XX_BLK_10809Mannequin",
                        title: "FLY01XX BLK 10809Mannequin"
                      }
                    ]
                  }
                }
              ]
            }
          },
          {
            code: "FLY06XXOLB",
            colorName: "Dark Olive/Black",
            price: {
              formattedValue: "£90.00"
            },
            wasPrice: null,
            cmsComponents: {
              items: [
                {
                  swatch: "green",
                  sku: "FLY06XXOLB",
                  marketingMessage: null,
                  manequinImagesCollection: {
                    items: [
                      {
                        url:
                          "https://images.ctfassets.net/1stkoghspd4h/5bEVELzuI5rJYynxUaQPY2/30bdf12eb9324062369f24280aea086c/FLY01XX_DNY_10841Mannequin",
                        title: "FLY01XX DNY 10841Mannequin"
                      }
                    ]
                  }
                }
              ]
            }
          },
          {
            code: "FLY06XXRDN",
            colorName: "Rose/Dark Green",
            price: {
              formattedValue: "£90.00"
            },
            wasPrice: null,
            cmsComponents: {
              items: []
            }
          },
          {
            code: "FLY06XXWSB",
            colorName: "White/Royal Blue",
            price: {
              formattedValue: "£90.00"
            },
            wasPrice: null,
            cmsComponents: {
              items: []
            }
          }
        ]
      };

      const output = {
        code: "FLY06XX",
        name: "Classic Flyweight Jersey",
        sanitizedName: "classic-flyweight-jersey-FLY06XX",
        colors: [
          {
            code: "FLY06XXBLK",
            colorName: "Black",
            price: {
              formattedValue: "£100.00"
            },
            wasPrice: null,
            cmsComponents: {
              items: [
                {
                  swatch: "black",
                  sku: "FLY06XXBLK",
                  marketingMessage: "New Season",
                  manequinImagesCollection: {
                    items: [
                      {
                        url:
                          "https://images.ctfassets.net/1stkoghspd4h/Nlc6eVQvIyqEoaCNdEIRm/d22ea5377c4a3204ea8eb078f3dea573/FLY01XX_BLK_10808Mannequin",
                        title: "FLY01XX BLK 10808Mannequin"
                      },
                      {
                        url:
                          "https://images.ctfassets.net/1stkoghspd4h/75N1IKcBQzLivKts8dJU00/c6219238e4d91a78a1535161f493315f/FLY01XX_BLK_10809Mannequin",
                        title: "FLY01XX BLK 10809Mannequin"
                      }
                    ]
                  }
                }
              ]
            },
            swatch: "black",
            sku: "FLY06XXBLK",
            marketingMessage: "New Season",
            manequinImagesCollection: {
              items: [
                {
                  url:
                    "https://images.ctfassets.net/1stkoghspd4h/Nlc6eVQvIyqEoaCNdEIRm/d22ea5377c4a3204ea8eb078f3dea573/FLY01XX_BLK_10808Mannequin",
                  title: "FLY01XX BLK 10808Mannequin"
                },
                {
                  url:
                    "https://images.ctfassets.net/1stkoghspd4h/75N1IKcBQzLivKts8dJU00/c6219238e4d91a78a1535161f493315f/FLY01XX_BLK_10809Mannequin",
                  title: "FLY01XX BLK 10809Mannequin"
                }
              ]
            },
            mannequins: [
              {
                url:
                  "https://images.ctfassets.net/1stkoghspd4h/Nlc6eVQvIyqEoaCNdEIRm/d22ea5377c4a3204ea8eb078f3dea573/FLY01XX_BLK_10808Mannequin",
                title: "FLY01XX BLK 10808Mannequin"
              },
              {
                url:
                  "https://images.ctfassets.net/1stkoghspd4h/75N1IKcBQzLivKts8dJU00/c6219238e4d91a78a1535161f493315f/FLY01XX_BLK_10809Mannequin",
                title: "FLY01XX BLK 10809Mannequin"
              }
            ]
          },
          {
            code: "FLY06XXOLB",
            colorName: "Dark Olive/Black",
            price: {
              formattedValue: "£90.00"
            },
            wasPrice: null,
            cmsComponents: {
              items: [
                {
                  swatch: "green",
                  sku: "FLY06XXOLB",
                  marketingMessage: null,
                  manequinImagesCollection: {
                    items: [
                      {
                        url:
                          "https://images.ctfassets.net/1stkoghspd4h/5bEVELzuI5rJYynxUaQPY2/30bdf12eb9324062369f24280aea086c/FLY01XX_DNY_10841Mannequin",
                        title: "FLY01XX DNY 10841Mannequin"
                      }
                    ]
                  }
                }
              ]
            },
            swatch: "green",
            sku: "FLY06XXOLB",
            marketingMessage: null,
            manequinImagesCollection: {
              items: [
                {
                  url:
                    "https://images.ctfassets.net/1stkoghspd4h/5bEVELzuI5rJYynxUaQPY2/30bdf12eb9324062369f24280aea086c/FLY01XX_DNY_10841Mannequin",
                  title: "FLY01XX DNY 10841Mannequin"
                }
              ]
            },
            mannequins: [
              {
                url:
                  "https://images.ctfassets.net/1stkoghspd4h/5bEVELzuI5rJYynxUaQPY2/30bdf12eb9324062369f24280aea086c/FLY01XX_DNY_10841Mannequin",
                title: "FLY01XX DNY 10841Mannequin"
              }
            ]
          },
          {
            code: "FLY06XXRDN",
            colorName: "Rose/Dark Green",
            price: {
              formattedValue: "£90.00"
            },
            wasPrice: null,
            cmsComponents: {
              items: []
            },
            mannequins: []
          },
          {
            code: "FLY06XXWSB",
            colorName: "White/Royal Blue",
            price: {
              formattedValue: "£90.00"
            },
            wasPrice: null,
            cmsComponents: {
              items: []
            },
            mannequins: []
          }
        ]
      };

      expect(input).toMatchObject(output);
    });
  });
});
