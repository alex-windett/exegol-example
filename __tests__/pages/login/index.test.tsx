import puppeteer from "puppeteer";

const url = "https://localhost:3001/gb/en/login";

const usernameInput = "input[data-test-id='login-username-input']";
const passwordInput = "input[data-test-id='login-password-input']";
const submitButton = "[data-test-id='login-password-input']";
const validUsername = "alex@rapha.cc";
const validPassword = "rapha2020";
const invalidUsername = "alex";
const invalidPassword = "rapha";

const options = {
  headless: !!process.env.PUPPETTEER_HEADLESS,
  devtools: !!process.env.PUPPETTEER_DEVTOOLS,
  args: ["--no-sandbox", "--disable-setuid-sandbox", "--disable-dev-shm-usage"]
};

describe("The Login Page", () => {
  test("The submit button is enabled when the form is valid", async () => {
    const browser = await puppeteer.launch(options);
    const page = await browser.newPage();

    await page.goto(url);

    try {
      await page.focus(usernameInput);
      await page.keyboard.type(validUsername);

      await page.focus(passwordInput);
      await page.keyboard.type(validPassword);

      const isDisabled = await page.$eval(
        submitButton,
        button => button.disabled
      );

      browser.close();

      expect(!isDisabled).toBeTruthy();
    } catch (e) {
      browser.close();
      throw new Error(e);
    }
  });

  // test("The submit button is disabled when the username is inval valid", async () => {
  //   const browser = await puppeteer.launch(options);
  //   const page = await browser.newPage();

  //   await page.goto(url);

  //   try {
  //     await page.focus(usernameInput);
  //     await page.keyboard.type(invalidUsername);

  //     await page.focus(passwordInput);
  //     await page.keyboard.type(validPassword);

  //     const isDisabled = await page.$eval(submitButton, button => {
  //       console.log(button);
  //       return button.disabled;
  //     });

  //     browser.close();

  //     expect(isDisabled).toBeTruthy();
  //   } catch (e) {
  //     browser.close();
  //     throw new Error(e);
  //   }
  // });

  // test("The submit button is disabled when the password is inval valid", async () => {
  //   const browser = await puppeteer.launch(options);
  //   const page = await browser.newPage();

  //   await page.goto(url);

  //   try {
  //     await page.focus(usernameInput);
  //     await page.keyboard.type(validUsername);

  //     await page.focus(passwordInput);
  //     await page.keyboard.type(invalidPassword);

  //     const isDisabled = await page.$eval(
  //       submitButton,
  //       button => button.disabled
  //     );

  //     browser.close();

  //     expect(isDisabled).toBeTruthy();
  //   } catch (e) {
  //     browser.close();
  //     throw new Error(e);
  //   }
  // });
});
