import fs from "fs";
import puppeteer from "puppeteer";
import devices from "puppeteer/DeviceDescriptors";
import path from "path";

devices["MacBook Pro 12"] = {
  name: "MacBook Pro 12",
  userAgent:
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.1.2 Safari/603.3.8",
  viewport: {
    width: 1152,
    height: 720,
    deviceScaleFactor: 2,
    isMobile: false,
    hasTouch: false,
    isLandscape: true
  }
};

devices["MacBook Pro 15"] = {
  name: "MacBook Pro 15",
  userAgent:
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.1.2 Safari/603.3.8",
  viewport: {
    width: 2880,
    height: 1800,
    deviceScaleFactor: 2,
    isMobile: false,
    hasTouch: false,
    isLandscape: true
  }
};

const BASE_URL: string = process.env.PUPPETTEER_URL || "https://localhost:3001";
let browser: any = null;
let page: any = null;
const dir: string = "/images";

// https://github.com/puppeteer/puppeteer/blob/master/lib/DeviceDescriptors.js
const deviceRenders: string[] = [
  "Pixel 2",
  "iPhone X",
  "iPad",
  "iPad landscape",
  "MacBook Pro 12"
  // "MacBook Pro 15"
];

const screenshot = async ({
  title,
  deviceName
}: {
  title: string;
  deviceName: string;
}) => {
  const pagePath = path.join(__dirname, dir, title);

  if (!fs.existsSync(pagePath)) {
    await fs.mkdirSync(pagePath, {
      recursive: true
    });
  }

  const file = `${deviceName}.jpeg`;
  const dirPath = path.join(pagePath, file);
  await page.screenshot({
    path: dirPath,
    type: "jpeg",
    fullPage: true,
    quality: 25,
    omitBackground: true
  });

  console.log(`Saved screenshot ${file} to ${dirPath}`);
};

const createSnapshot = async ({
  url,
  title
}: {
  url: string;
  title: string;
}) => {
  deviceRenders.forEach(async deviceName => {
    test(`${title} on ${deviceName}`, async () => {
      browser = await puppeteer.launch({
        headless: !!process.env.PUPPETTEER_HEADLESS,
        devtools: !!process.env.PUPPETTEER_DEVTOOLS,
        args: [
          "--no-sandbox",
          "--disable-setuid-sandbox",
          "--disable-dev-shm-usage"
        ]
      });
      page = await browser.newPage();
      await page.emulate(devices[deviceName]);

      try {
        await page.goto(url);
        await screenshot({ title, deviceName });
        browser.close();
      } catch (e) {
        browser.close();
        throw new Error(e);
      }
    });
  });
};

describe("Running screenhots on Hompage", () => {
  const test = async () => {
    const url = `${BASE_URL}/`;
    const title = "homepage";

    await createSnapshot({ url, title });
  };

  test();
});

describe("Running screenhots on Stories Listing", () => {
  const test = async () => {
    const url = `${BASE_URL}/stories`;
    const title = "stories";

    await createSnapshot({ url, title });
  };

  test();
});

describe("Running screenhots on Story Rapha + ByBore", () => {
  const test = async () => {
    const url = `${BASE_URL}/stories/events/womens-100/rapha-bybore`;
    const title = "story";

    await createSnapshot({ url, title });
  };

  test();
});
