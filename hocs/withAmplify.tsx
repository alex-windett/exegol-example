import React, { createContext, useContext } from "react";
import { NextPage } from "next";

import { Amplify, withSSRContext } from "aws-amplify";

const AmplifyContext = createContext<any>(null);

export const useAmplify = () => useContext(AmplifyContext);

export const withAmplify = (ComposedComponent: NextPage) => {
  const WithAmplify = (props: any) => {
    const { cognitoConfig, ...rest } = props;
    const { Auth } = withSSRContext();

    React.useMemo(() => {
      Amplify.configure({ cognitoConfig, ssr: true });
      Auth.configure({ cognitoConfig, ssr: true });
    }, []);

    return (
      <AmplifyContext.Provider value={{ cognitoConfig }}>
        <ComposedComponent {...rest} />
      </AmplifyContext.Provider>
    );
  };

  WithAmplify.getInitialProps = async (appContext: any) => {
    const { Auth } = withSSRContext(appContext.ctx);

    const cognitoConfig = {
      Auth: {
        mandatorySignIn: true,
        region: "eu-west-1",
        userPoolId: "us-east-1_XpMZuE03g",
        userPoolWebClientId: "1ri6uqvrl60uk5to2takucmojd",
        authenticationFlowType: "USER_PASSWORD_AUTH",
      },
    };

    if (typeof window === "undefined") {
      Amplify.configure({ cognitoConfig, ssr: true });
      Auth.configure({ cognitoConfig, ssr: true });
    }

    return { cognitoConfig };
  };

  return WithAmplify;
};
