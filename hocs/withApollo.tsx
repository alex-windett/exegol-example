import React, { Fragment } from "react";
import { NextComponentType, NextPage } from "next";
import { useGraphQL } from "graphql-react";
import { CopyBlock, googlecode } from "react-code-blocks";

import { i18n } from "@Server/withI18n";

import UI from "@rapharacing/rapha-ui";

const { Loader } = UI;

interface State {
  query: string;
  variables?: any;
}

const withGraphql = (initialState: Function | State) => <P extends {}>(
  ComposedComponent: NextPage<P>
): NextComponentType<P> => {
  const WithGraphql: NextComponentType<any> = (props?: any) => {
    const state: State =
      typeof initialState === "function" ? initialState(props) : initialState;

    const { query, variables } = state;

    const { cacheValue, loading } = useGraphQL<any, any>({
      fetchOptionsOverride(options) {
        options.url =
          process.env.GRAPHQL_URL || "http://localhost:4000/graphql";
      },
      operation: {
        query,
        variables: {
          locale: i18n.language,
          preview: process.env.NODE_ENV !== "production",
          ...variables,
        },
      },
    });

    if (!cacheValue) return <Fragment />;

    const { data } = cacheValue;

    if (loading) return <Loader dark />;

    if (!data) {
      return (
        <>
          <p>A GraphQL error occured:</p>

          <CopyBlock
            text={JSON.stringify(cacheValue)}
            theme={googlecode}
            language="json"
            showLineNumbers={false}
            wrapLines
          />
        </>
      );
    }

    return <ComposedComponent {...props} data={data} />;
  };

  WithGraphql.getInitialProps = async (ctx: any) => {
    const currentLanguage = !!ctx.req ? ctx.req.language : i18n.language;

    return ComposedComponent.getInitialProps
      ? { ...ComposedComponent.getInitialProps(ctx), currentLanguage }
      : { currentLanguage };
  };

  return WithGraphql;
};

export default withGraphql;
