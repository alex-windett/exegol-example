import React, { useContext } from "react";
import { Context, UserContext } from "../context";

const withAppContext = (WrappedComponent: any) => {
  const WithAppContext = (props: any) => {
    const context = useContext(Context);
    const userContext = useContext(UserContext);

    // @ts-ignore
    const appContext = { ...context, ...userContext };

    return <WrappedComponent {...props} appContext={appContext} />;
  };

  return WithAppContext;
};

export default withAppContext;
