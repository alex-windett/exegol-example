# Exegol

A Next.js app using GraphqQL as a data source.

# Install

- `yarn && yarn tasks`

## Environemnt Files

Copy example enviroment file to .env

`cp .env.example .env`

## SSL Certificates

Install mkcert https://github.com/FiloSottile/mkcert

- `brew install mkcert`

Create certs

```bash
mkdir certs; cd certs; mkcert -install; mkcert localhost;
```

## Development

From root of project

- `yarn dev`

## Production

From root of project

- `yarn build && yarn start`
